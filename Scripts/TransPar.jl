## =======================================================
##
## File: TransPar.jl
##
## Parameter transformation from one- and two-sided
## domains to the real axis. The transformation can be
## collected in a 1d array with different transformations
## for different components.
##
## creation:          April   16, 2021 -- Peter Reichert
## last modification: January 05, 2022 -- Peter Reichert
## 
## contact:           peter.reichert@eawag.ch
##
## =======================================================

abstract type TransPar end

struct TransParUnbounded <: TransPar
end

struct TransParLowerBound <: TransPar
    par_min::Float64
    TransParLowerBound(par_min) = new(par_min)
    TransParLowerBound() = TransParLowerBound(0.0)
end

struct TransParUpperBound <: TransPar
    par_max::Float64
    TransParUpperBound(par_max) = new(par_max)
    TransParUpperBound() = TransParUpperBound(0.0)
end

struct TransParInterval <: TransPar
    par_min::Float64
    par_max::Float64
    TransParInterval(par_min,par_max) = par_min >= par_max ? error("par_min must be smaller than par_max") : new(par_min,par_max)
    TransParInterval() = TransParInterval(0.0,1.0)
end

function TransformForward(::TransParUnbounded,x)
    return x
end

function TransformBackward(::TransParUnbounded,x)
    return x
end

function CorrectLogPdf(::TransParUnbounded,x)
    return 0.0
end

function TransParGetBounds(::TransParUnbounded)
    return (-Inf,Inf)
end


function TransformForward(t::TransParLowerBound,x)
    if x < t.par_min; error("TransParForward: x = ",x," is smaller than par_min = ",t.par_min); end
    return log(x-t.par_min)
end

function TransformBackward(t::TransParLowerBound,x)
    return t.par_min + exp(x)
end

function CorrectLogPdf(::TransParLowerBound,x)
    return x
end

function TransParGetBounds(t::TransParLowerBound)
    return (t.par_min,Inf)
end


function TransformForward(t::TransParUpperBound,x)
    if x > t.par_max; error("TransParForward: x = ",x," is larger than par_max = ",t.par_max); end
    return - log(t.par_max-x)
end

function TransformBackward(t::TransParUpperBound,x)
    return t.par_max - exp(-x)
end

function CorrectLogPdf(::TransParUpperBound,x)
    return - x
end

function TransParGetBounds(t::TransParUpperBound)
    return (-Inf,t.par_max)
end


function TransformForward(t::TransParInterval,x)
    if (x < t.par_min) | (x > t.par_max)
        error("TransParForward: x = ",x," is smaller than par_min",t.par_min,
              "or it is larger than par_max = ",t.par_max)
    end
    return log(x-t.par_min) - log(t.par_max-x)
end

function TransformBackward(t::TransParInterval,x)
    return (t.par_min+t.par_max*exp(x)) / (1+exp(x))
end

function CorrectLogPdf(t::TransParInterval,x)
    return log(t.par_max-t.par_min) - log(2+exp(x)+exp(-x))
end

function TransParGetBounds(t::TransParInterval)
    return (t.par_min,t.par_max)
end



function TransformForward!(t::Array{TransPar,1},x::AbstractArray)
    if length(t) != length(x); error("TransformForward: length of t and of x must be the same"); end
    x = TransformForward.(t,x)
end

function TransformForward(t::Array{TransPar,1},x::AbstractArray)
    if length(t) != length(x); error("TransformForward: length of t and of x must be the same"); end
    y = deepcopy(x)  # deepcopy needed for the case when x is ForwardDiff.Dual rather than Float64
    y = TransformForward.(t,x)
    return(y)
end

function TransformBackward!(t::Array{TransPar,1},x::AbstractArray)
    if length(t) != length(x); error("TransformBackward: length of t and of x must be the same"); end
    x = TransformBackward.(t,x)
end

function TransformBackward(t::Array{TransPar,1},x::AbstractArray)
    if length(t) != length(x); error("TransformBackward: length of t and of x must be the same"); end
    y = deepcopy(x)  # deepcopy needed for the case when x is ForwardDiff.Dual rather than Float64
    y = TransformBackward.(t,x)
    return y
end

function CorrectLogPdf(t::Array{TransPar,1},x::AbstractArray)
    if length(t) != length(x); error("CorrectLogPdf: length of t and of x must be the same"); end
    corrlp = 0.0
    for i in 1:length(t)
        corrlp = corrlp + CorrectLogPdf(t[i],x[i])
    end
    return corrlp
end


#=

# for testing:

using  DistributionsAD
using  ForwardDiff

Trans = Array{TransPar}(undef,4)


Trans[1] = TransParUnbounded()
Trans[2] = TransParLowerBound(1.0)
Trans[3] = TransParUpperBound(-1.0)
Trans[4] = TransParInterval(0.0,1.0)

x = [1.0,2.0,-5.0,0.5]

TransformForward!(Trans,x)
x

TransformBackward!(Trans,x)
x

CorrectLogPdf(Trans,x)

function logpdf(x)
    return sum(DistributionsAD.logpdf.(DistributionsAD.Normal.(0,1),x))
end

logpdf(x)

function logpdf_trans1(x)
    y = deepcopy(x)  # deepcopy needed for the case when x is ForwardDiff.Dual rather than Float64
    TransformBackward!(Trans,y)
    return logpdf(y) + CorrectLogPdf(Trans,y)
end

function logpdf_trans2(x)
    TransformBackward!(Trans,x)
    lp = logpdf(x) + CorrectLogPdf(Trans,x)
    TransformForward!(Trans,x)
    return lp
end

function logpdf_trans3(x)
    y = TransformBackward(Trans,x)
    return logpdf(y) + CorrectLogPdf(Trans,y)
end

TransformForward!(Trans,x)
logpdf_trans1(x)
logpdf_trans2(x)
logpdf_trans3(x)

ForwardDiff.gradient(logpdf,x)
ForwardDiff.gradient(logpdf_trans1,x)
ForwardDiff.gradient(logpdf_trans2,x)
ForwardDiff.gradient(logpdf_trans3,x)

=#
