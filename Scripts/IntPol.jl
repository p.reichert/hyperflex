## =======================================================
##
## File: IntPol.jl
##
## Interpolation functions.
## Own implementation due to incompatibilities with Zygote
##
## creation:          December 26, 2021 -- Peter Reichert
## last modification: December 26, 2021 -- Peter Reichert
## 
## contact:           peter.reichert@eawag.ch
##
## =======================================================


function IntPolFun(x::AbstractArray,y::AbstractArray;method="linear",extrapolation="constant")

    n = length(x)
    if length(y) != n
        error(string("IntPolFun: x and y do not have the same length (",n,",",length(y),")"))                                  
    end
    if n == 0; error("IntPolFun: lengths of arrays x and y are zero"); end
    if n > 1
        for i in 2:n
            if ! (x[i]>x[i-1]); error("IntPolFun: values in array x are not strictly monotonically increasing"); end
        end
    end

    function IntPol(x0)

        if extrapolation == "constant"
            if x0 <= x[1]; return y[1]; end
            if x0 >= x[n]; return y[n]; end
        elseif (extrapolation == "no") | (extrapolation == "none")
            if x0 <= x[1]; return NaN; end
            if x0 >= x[n]; return NaN; end
        else
            error(string("IntPolFun: Unknown extrapolation method ",extrapolation))
        end

        i0 = min(max(1,trunc(Int,(x0-x[1])/(x[n]-x[1])*Float64(n))),n-1)

        if method == "linear"
            if x0 < x[i0]
                while x0 < x[i0]; i0 -= 1; end
            else
                while x0 > x[i0+1]; i0 += 1; end
            end
            return ( (x0-x[i0])*y[i0+1] + (x[i0+1]-x0)*y[i0] ) / (x[i0+1]-x[i0])
        else
            error(string("IntPolFun: Unknown interpolation method ",method))
        end

    end

    return IntPol

end

#=

# Test:

IntPol = IntPolFun([1.0,2.0,3.0,4.0,5.0],[5.0,6.0,7.0,6.0,5.0])
x = (0:60)/10.0
y = IntPol.(x)
println(y)

=#