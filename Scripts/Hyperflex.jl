## =======================================================
##
## File: Hyperflex.jl
##
## Hierarchical Hydrological Model
##
## creation:          January  01, 2021 -- Peter Reichert
## last modification: April    17, 2022 -- Peter Reichert
## version:           0.50
## 
## contact:           peter.reichert@eawag.ch
##
## =======================================================


# Packages:
# =========

# import Interpolations        # problems due to incompatibility with Zygote
# import BasicInterpolators    # problems due to incompatibility with Zygote

import Dates

import DifferentialEquations
import DiffEqSensitivity
import DistributionsAD
import ForwardDiff
import Zygote
import LogDensityProblems
# import TransformVariables
import MCMCChains
import Optim
import Flux
import GalacticOptim
import AdvancedHMC
import DynamicHMC
import AdvancedVI

include("IntPol.jl")
include("TransPar.jl")
include("Auxiliary.jl")

include("Seq1dODESolver.jl")


# Auxiliary functions and structures:
# ===================================

# Auxiliary function for getting (first) indices from string vectors:
# -------------------------------------------------------------------

function Hyperflex_GetIndices(names,names_requested)

    ind    = Int[]
    errmsg = String[]
    for i in 1:length(names_requested)
        j = findfirst(isequal(names_requested[i]),names)
        if isnothing(j)
            errmsg = vcat(errmsg,string("name ",names_requested[i]," not found"))
            ind = vcat(ind,[0])
        else
            ind = vcat(ind,[j])
        end
    end

    return ( ind = (;zip(Symbol.(names_requested),ind)...) , errmsg = errmsg )

end

# Auxiliary function to print current states and derivatives:
# -----------------------------------------------------------

function Hyperflex_Print_States(t,u)

    sigdigits = 6
    println(string("time = ",t))
    println("states")
    println(round.(u,sigdigits=sigdigits))
    flush(stdout)

end

function Hyperflex_Print_States_Rhs_Par(t,u,du,par,ind_hyd)

    sigdigits = 6
    println(string("time = ",round(t,sigdigits=sigdigits)))
    println("states")
    println(round.(u,sigdigits=sigdigits))
    println("derivatives")
    println(round.(du,sigdigits=sigdigits))
    println(join(string(collect(String.(keys(ind_hyd)))[i],"=",round(par[collect(ind_hyd)][i],sigdigits=sigdigits)," ") for i in 1:length(ind_hyd)))
    flush(stdout)

end

# Structure for DynamicHMC:
# -------------------------

struct DynamicHMC_InferenceProblem
    logposttrans::Function
end

function(p::DynamicHMC_InferenceProblem)(par_trans)
    p.logposttrans(par_trans)
end

# Structure for elevation bands of the hydrological model:
# --------------------------------------------------------

struct Fband
    fA::Float64
    Tmin::Array{Float64,2}
    Tmax::Array{Float64,2}
    Prec::Array{Float64,2}
end


# Run the hydrological model:
# ===========================

# run the model with specifying model, parameters, input and
# numerical algorithm and its settings:
# ----------------------------------------------------------

function Hyperflex_RunModel(
            model::String,                             # name of model
            par::AbstractVector,                       # parameters
            parnames::Vector{String},                  # parameter names
            Prec::Matrix{Float64},                     # 2 col matrix time, precip
            Epot::Union{Missing,Matrix{Float64}},      # 2 col matrix time, pot. evap
            Tmin::Union{Missing,Matrix{Float64}},      # 2 col matrix time, Tmin
            Tmax::Union{Missing,Matrix{Float64}},      # 2 col matrix time, Tmax
            Lday::Union{Missing,Matrix{Float64}},      # 2 col matrix time, daylight length Lday
            Felevband::Union{Missing,Array{Fband,1}},  # elevation band forcing (A) 
            times::Vector{Float64};                    # requested output time points
            algorithm = DifferentialEquations.Euler(), # integration algorithm
            dt = 0.1*(maximum(P[:,1])-minimum(P[:,1]))/size(P)[1],
                                                       # time step for fixed time step integrators
            dtmax = (maximum(P[:,1])-minimum(P[:,1]))/size(P)[1], 
                                                       # maximum time step for variable time step int.
                                                       # (adapt to not miss input peaks!)
            reltol = 1e-4,                             # relative tolerance of integrator
            abstol = 1e-5,                             # absolute tolerance of integrator
            verbose = false)                           # verbose mode of some ODE solvers

    # define 1box model (names of required parameters and rhs function of odes):
    # --------------------------------------------------------------------------

    # Right-hand side of simple 1 box model as used in:

    # Reichert, P., Ammann, L. and Fenicia, F.
    # Potential and Challenges of Investigating Intrinsic Uncertainty 
    # of Hydrological Models with Stochastic, Time‐Dependent Parameters
    # Water Resources Research, in press, 2021.
    # https://doi.org/10.1029/2020WR028400

    parnames_required_1box = ["Styp","S0","k","alpha","m","ce"]

    function rhs_1box!(du,u,p,t)

        # state variable: S

        # check input:

        if ! all(isfinite.(u))
            println("*** infinite states on entry to rhs_1box!")
            Hyperflex_Print_States(t,u)
        end

        # evaluate input at time t:

        Pmult  = 1.0; if ind_sens.Pmult>0;  Pmult  = p[ind_sens.Pmult];  end
        Tshift = 0.0; if ind_sens.Tshift>0; Tshift = p[ind_sens.Tshift]; end

        P = Prec_fun(t)
        if ismissing(Epot_fun)
            E = Epot_Hamon(0.5*(Tmin_fun(t)+Tmax_fun(t))+Tshift,Lday_fun(t))
        else
            E = Epot_fun(t)
        end
        P = P * Pmult
        E = E * p[ind_hyd.ce]

        # evaluate right hand side of ODE (parameters are known from enclosing function):

        local S = max(u[1],0.0)
        local Q = p[ind_hyd.k]/p[ind_hyd.Styp]^(p[ind_hyd.alpha]-1.0)*S^p[ind_hyd.alpha]
    
        du[1] = P - Q - E*(1.0-exp(-S/p[ind_hyd.m]))
    
        # check output:
        
        if ! all(isfinite.(du))
            println("*** infinite derivatives on exit from rhs_1box!")
            Hyperflex_Print_States_Rhs_Par(t,u,du,p,ind_hyd)
        end

    end

    # define 2box model (names of required parameters and rhs function of odes):
    # --------------------------------------------------------------------------
    
    # Right-hand side of simple 2 box model as used in:

    # Reichert, P., Ammann, L. and Fenicia, F.
    # Potential and Challenges of Investigating Intrinsic Uncertainty 
    # of Hydrological Models with Stochastic, Time‐Dependent Parameters
    # Water Resources Research, in press, 2021.
    # https://doi.org/10.1029/2020WR028400

    parnames_required_2box = ["Styp","S10","S20","k1","k2","k12",
                              "alpha1","alpha2","alpha12","m1","m2","ce"]

    function rhs_2box!(du,u,p,t)

        # state variables: S1, S2

        # check input:
        
        if ! all(isfinite.(u))
            println("*** infinite states on entry to rhs_2box!")
            Hyperflex_Print_States(t,u)
        end

        # evaluate input at time t:

        Pmult  = 1.0; if ind_sens.Pmult>0;  Pmult  = p[ind_sens.Pmult];  end
        Tshift = 0.0; if ind_sens.Tshift>0; Tshift = p[ind_sens.Tshift]; end

        P = Prec_fun(t)
        if ismissing(Epot_fun)
            E = Epot_Hamon(0.5*(Tmin_fun(t)+Tmax_fun(t))+Tshift,Lday_fun(t))
        else
            E = Epot_fun(t)
        end
        P = P * Pmult
        E = E * p[ind_hyd.ce]

        # evaluate right hand side of ODE system (parameters are known from enclosing function):

        local S1    = max(u[1],0.0)
        local S2    = max(u[2],0.0)
        local Q1    = p[ind_hyd.k1]/p[ind_hyd.Styp]^(p[ind_hyd.alpha1]-1.0)*S1^p[ind_hyd.alpha1]
        local Q2    = p[ind_hyd.k2]/p[ind_hyd.Styp]^(p[ind_hyd.alpha2]-1.0)*S2^p[ind_hyd.alpha2]
        local Q12   = p[ind_hyd.k12]/p[ind_hyd.Styp]^(p[ind_hyd.alpha12]-1.0)*S1^p[ind_hyd.alpha12]
    
        du[1] = P - Q12 - Q1 - E*(1.0-exp(-S1/p[ind_hyd.m1]))
        du[2] =     Q12 - Q2 - E*exp(-S1/p[ind_hyd.m1])*(1.0-exp(-S2/p[ind_hyd.m2]))

        # check output:
        
        if ! all(isfinite.(du))
            println("*** infinite derivatives on exit from rhs_2box!")
            Hyperflex_Print_States_Rhs_Par(t,u,du,p,ind_hyd)
        end

    end

    # define GR4 model (names of required parameters and rhs function of odes):
    # -------------------------------------------------------------------------
    
    # Right-hand side of the GR4 model:

    # Discrete-time version GR4J with daily time step:

    # Perrin, C., Michel, C. and Andreassian, V.
    # Improvement of a parsimonious model for streamflow simulation
    # Journal of Hydrology 279, 275-289, 2003
    # https://doi.org/10.1016/S0022-1694(03)00225-7

    # Description of the continuous-time version that is implemented below:

    # Santos, L., Thirel, G. and Perrin, C.
    # Continuous state-space representation of a bucket-type rainfall-runoff model:
    # a case study with the GR4 model using state-space GR4 (version 1.0)
    # Geoscientific Model Development 11, 1591-1605, 2018.
    # https://doi.org/10.5194/gmd-11-1591-2018

    parnames_required_GR4 = ["x1","x2","x3","x4",
                             "alpha","beta","gamma","omega","Phi","nu",
                             "Ut","nres",
                             "ce"]
       
    function rhs_GR4!(du,u,p,t)

        # state variables: Ss, Sh_1, ... , Sh_nres, Sr

        # check input:
        
        if ! all(isfinite.(u))
            println("*** infinite states on entry to rhs_GR4!")
            Hyperflex_Print_States(t,u)
        end

        # evaluate input at time t:

        Pmult  = 1.0; if ind_sens.Pmult>0;  Pmult  = p[ind_sens.Pmult];  end
        Tshift = 0.0; if ind_sens.Tshift>0; Tshift = p[ind_sens.Tshift]; end

        P = Prec_fun(t)
        if ismissing(Epot_fun)
            E = Epot_Hamon(0.5*(Tmin_fun(t)+Tmax_fun(t))+Tshift,Lday_fun(t))
        else
            E = Epot_fun(t)
        end
        P = P * Pmult
        E = E * p[ind_hyd.ce]

        if P > E
            Pn = P - E
            En = 0.0
        else
            Pn = 0.0
            En = E - P
        end

        # evaluate right hand side of ODE system (parameters are known from enclosing function):

        local SS    = max(u[1],0.0)
        local Ps    = Pn * ( 1.0 - (SS/p[ind_hyd.x1])^p[ind_hyd.alpha] )
        # local Es    = En * ( 2.0*SS/p[ind_hyd.x1] - (SS/p[ind_hyd.x1])^p[ind_hyd.alpha] )
        local Es    = En * ( 1.0 - (1.0-SS/p[ind_hyd.x1])^p[ind_hyd.alpha] )  # modified to remain in [0,1] for alpha != 2
        local Qperc = p[ind_hyd.x1]^(1.0-p[ind_hyd.beta]) / ( (p[ind_hyd.beta]-1.0)*p[ind_hyd.Ut] ) * p[ind_hyd.nu]^(p[ind_hyd.beta]-1.0) * SS^p[ind_hyd.beta]
    
        du[1] = Ps - Es - Qperc
 
        local Qin = Qperc + (Pn-Ps)
        for i in 1:nres
            Sr_i    = max(u[1+i],0.0)
            Qout    = p[ind_hyd.nres]/p[ind_hyd.x4] * Sr_i
            du[1+i] = Qin - Qout
            Qin     = Qout
        end

        local SR = max(u[nres+2],0.0)
        local Q9 = p[ind_hyd.Phi]*Qin     # inflow from last Nash cascade reservoir into routing store
        local QF = p[ind_hyd.x2]/p[ind_hyd.x3]^p[ind_hyd.omega] * SR^p[ind_hyd.omega]
        local QR = p[ind_hyd.x3]^(1.0-p[ind_hyd.gamma]) / ( (p[ind_hyd.gamma]-1.0)*p[ind_hyd.Ut] ) * SR^p[ind_hyd.gamma]

        du[nres+2] = Q9 + QF - QR

        # check output:
        
        if ! all(isfinite.(du))
            println("*** infinite derivatives on exit from rhs_GR4!")
            Hyperflex_Print_States_Rhs_Par(t,u,du,p,ind_hyd)
        end

    end

    # define GR4neige model (names of required parameters and rhs function of odes):
    # ------------------------------------------------------------------------------
    
    # Right-hand side of the combined GR4 and Cemaneige model:

    # Discrete-time version GR4J with daily time step:

    # Perrin, C., Michel, C. and Andreassian, V.
    # Improvement of a parsimonious model for streamflow simulation
    # Journal of Hydrology 279, 275-289, 2003
    # https://doi.org/10.1016/S0022-1694(03)00225-7

    # Description of the continuous-time version that is implemented below:

    # Santos, L., Thirel, G. and Perrin, C.
    # Continuous state-space representation of a bucket-type rainfall-runoff model:
    # a case study with the GR4 model using state-space GR4 (version 1.0)
    # Geoscientific Model Development 11, 1591-1605, 2018.
    # https://doi.org/10.5194/gmd-11-1591-2018

    # Description of the Cemaneige model (in discrete time; the model was slightly
    # modified for continuous time and non-equal areas of altitude bands):

    # Audrey, V., Andreassian, V. and Perrin, C.
    # `As simple as possible but not simpler': What is useful in a temperature-based
    # snow-accounting routine? Part 2 - Sensitivity analysis of the Cemaneige snow
    # accounting routine on 380 catchments
    # Journal of Hydrology 517, 1176-1187, 2014.
    # https://doi.org/10.1016/j.jhydrol.2014.04.058
    
    parnames_required_GR4neige = ["x1","x2","x3","x4",
                                  "alpha","beta","gamma","omega","Phi","nu",
                                  "Ut","nres",
                                  "thetaG1","thetaG2","Tsfth","Tsmth","DeltaTsm","Ssnth",
                                  "ce"]
                                     
    function rhs_GR4neige!(du,u,p,t)

        # state variables: Tsn_1, Ssn_1, ... , Tsn_nbands, Ssn_nbands, SS, Sr_1, ... , Sr_nres, SR
        # (temperature first in snow model to allow for sequential solution of implicit equations)

        # check input:
        
        if eltype(u) == Float64   # avoid output during taking of derivatives
            if ! all(isfinite.(u))
                println("*** infinite states on entry to rhs_GR4neige!")
                Hyperflex_Print_States(t,u)
            end
        end

        # evaluate input at time t:

        Pmult  = 1.0; if ind_sens.Pmult>0;  Pmult  = p[ind_sens.Pmult];  end
        Tshift = 0.0; if ind_sens.Tshift>0; Tshift = p[ind_sens.Tshift]; end

        local P = 0.0
        for i in 1:n_bands
            if n_bands == 1
                Tmin = Tmin_fun(t) + Tshift
                Tmax = Tmax_fun(t) + Tshift
                Ptot = Prec_fun(t) * Pmult
            else
                Tmin  = Tmin_fun_band[i](t) + Tshift
                Tmax  = Tmax_fun_band[i](t) + Tshift
                Ptot  = Prec_fun_band[i](t) * Pmult
            end
            Tmean = 0.5*(Tmin+Tmax)
            if Tmin >= p[ind_hyd.Tsfth]
                fsnow = 0.0
            elseif Tmax <= p[ind_hyd.Tsfth]
                fsnow = 1.0
            else
                fsnow = (p[ind_hyd.Tsfth]-Tmin)/(Tmax-Tmin)
            end
            Ssn_i = max(u[2*i],0.0)
            Tsn_i = u[2*i-1]
            Qmelt_i = p[ind_hyd.thetaG1]/p[ind_hyd.Ut] *
                      SmoothIncrease(Tmean-p[ind_hyd.Tsmth],dx=p[ind_hyd.DeltaTsm]) *
                      SmoothHeaviside(Tsn_i,dx=p[ind_hyd.DeltaTsm]) *
                      (1.0-exp(-Ssn_i/p[ind_hyd.Ssnth]))
            du[2*i-1] = - log(p[ind_hyd.thetaG2])/p[ind_hyd.Ut]*(Tmean-Tsn_i)
            du[2*i]   = fsnow*Ptot - Qmelt_i
            P = P + fA[i] * ( (1.0-fsnow)*Ptot + Qmelt_i )
        end

        if ismissing(Epot_fun)
            local E = Epot_Hamon(0.5*(Tmin_fun(t)+Tmax_fun(t))+Tshift,Lday_fun(t)) * p[ind_hyd.ce]
        else
            local E = Epot_fun(t) * p[ind_hyd.ce]
        end

        if P > E
            local Pn = P - E
            local En = 0.0
        else
            local Pn = 0.0
            local En = E - P
        end

        # evaluate right hand side of ODE system (parameters are known from enclosing function):

        local SS    = max(u[2*n_bands+1],0.0)
        local Ps    = Pn * ( 1.0 - (SS/p[ind_hyd.x1])^p[ind_hyd.alpha] )
        # local Es    = En * ( 2.0*SS/p[ind_hyd.x1] - (SS/p[ind_hyd.x1])^p[ind_hyd.alpha] )
        local Es    = En * ( 1.0 - (1.0-SS/p[ind_hyd.x1])^p[ind_hyd.alpha] )  # modified to remain in [0,1] for alpha != 2
        local Qperc = p[ind_hyd.x1]^(1.0-p[ind_hyd.beta]) / ( (p[ind_hyd.beta]-1.0)*p[ind_hyd.Ut] ) * p[ind_hyd.nu]^(p[ind_hyd.beta]-1.0) * SS^p[ind_hyd.beta]
    
        du[2*n_bands+1] = Ps - Es - Qperc

        local Qin = Qperc + (Pn-Ps)
        for i in 1:nres
            Sr_i              = max(u[2*n_bands+1+i],0.0)
            Qout              = p[ind_hyd.nres]/p[ind_hyd.x4] * Sr_i
            du[2*n_bands+1+i] = Qin - Qout
            Qin               = Qout
        end

        local SR = max(u[2*n_bands+nres+2],0.0)
        local Q9 = p[ind_hyd.Phi]*Qin   # inflow from last Nash cascade reservoir into routing store
        local QF = p[ind_hyd.x2]/p[ind_hyd.x3]^p[ind_hyd.omega] * SR^p[ind_hyd.omega]
        local QR = p[ind_hyd.x3]^(1.0-p[ind_hyd.gamma]) / ( (p[ind_hyd.gamma]-1.0)*p[ind_hyd.Ut] ) * SR^p[ind_hyd.gamma]

        du[2*n_bands+nres+2] = Q9 + QF - QR

        # check output:
        
        if eltype(du) == Float64   # avoid output during taking of derivatives
            if ! all(isfinite.(du))
                println("*** infinite derivatives on exit from rhs_GR4neige!")
                Hyperflex_Print_States_Rhs_Par(t,u,du,p,ind_hyd)
            end
        end

    end

    # define HBV-based model:
    # -----------------------

    parnames_required_HBV = ["Tsfth","Tsmth","csf","cmelt","DeltaTsm","Ssnth","Sswth","cfr","cwh",
                             "Sfc","beta","cperc","Suzth","Suzdiv","k0","k1","k2","kr","kdg","nres",
                             "Ssmth","ce"]
                                     
    function rhs_HBV!(du,u,p,t)

        # state variables: Ssn_1, Ssw_1, Ssm_1, ... , Ssn_nbands, Ssw_nbands, Ssm_nbands, 
        #                  Suz, Slz, Sr_1, ... , Sr_nres
        # (note: due to refreezing not set of sequential equations)

        # check input:
        
        if eltype(u) == Float64   # avoid output during taking of derivatives
            if ! all(isfinite.(u))
                println("*** infinite states on entry to rhs_HBV!")
                Hyperflex_Print_States(t,u)
            end
        end

        for i in 1:length(u)
            if abs(u[i]) > 1.0e6; println("i = ",i," t = ",t," u[i] = ",u[i]); end
        end

        # evaluate input at time t:

        Pmult  = 1.0; if ind_sens.Pmult>0;  Pmult  = p[ind_sens.Pmult];  end
        Tshift = 0.0; if ind_sens.Tshift>0; Tshift = p[ind_sens.Tshift]; end

        local Q_rech = 0.0
        for i in 1:n_bands
            if n_bands == 1
                Tmin = Tmin_fun(t) + Tshift
                Tmax = Tmax_fun(t) + Tshift
                Ptot = Prec_fun(t) * Pmult
            else
                Tmin  = Tmin_fun_band[i](t) + Tshift
                Tmax  = Tmax_fun_band[i](t) + Tshift
                Ptot  = Prec_fun_band[i](t) * Pmult
            end
            Tmean = 0.5*(Tmin+Tmax)
            if Tmin >= p[ind_hyd.Tsfth]
                fsnow = 0.0
            elseif Tmax <= p[ind_hyd.Tsfth]
                fsnow = 1.0
            else
                fsnow = (p[ind_hyd.Tsfth]-Tmin)/(Tmax-Tmin)
            end
            Ssn_i = max(u[3*i-2],0.0)    # snow
            Ssw_i = max(u[3*i-1],0.0)    # snow water
            Ssm_i = max(u[3*i],0.0)      # soil moisture
            Qmelt_i = p[ind_hyd.cmelt] *
                      SmoothIncrease(Tmean-p[ind_hyd.Tsmth],dx=p[ind_hyd.DeltaTsm]) *
                      (1.0-exp(-Ssn_i/p[ind_hyd.Ssnth]))
            Qrefr_i = p[ind_hyd.cfr] * p[ind_hyd.cmelt] *
                      SmoothIncrease(p[ind_hyd.Tsmth]-Tmean,dx=p[ind_hyd.DeltaTsm]) *
                      (1.0-exp(-Ssw_i/p[ind_hyd.Sswth]))
            Qsw_i = 0.0;
            if Ssw_i < p[ind_hyd.cwh]*Ssn_i
                Qsw_i = (Qmelt_i+(1-fsnow)*Ptot)*
                          (1.0-exp(-Ssn_i/p[ind_hyd.Ssnth]))*
                          (1.0-exp(-(p[ind_hyd.cwh]*Ssn_i-Ssw_i)/p[ind_hyd.Sswth]))
            end
            Qrel_i = 0.0
            if Ssn_i > 0
                Qrel_i = Ssw_i/(p[ind_hyd.cwh]*Ssn_i) * Qmelt_i
            end
            Qsn_i = Qmelt_i + (1.0-fsnow)*Ptot - Qsw_i + Qrel_i

            if ismissing(Epot_fun)
                local E = Epot_Hamon(Tmean+Tshift,Lday_fun(t)) * p[ind_hyd.ce]
            else
                local E = Epot_fun(t) * p[ind_hyd.ce]
            end
            du[3*i-2] = p[ind_hyd.csf]*fsnow*Ptot - Qmelt_i + Qrefr_i
            du[3*i-1] = Qsw_i - Qrefr_i - Qrel_i
            du[3*i]   = Qsn_i*( 1.0 - (Ssm_i/p[ind_hyd.Sfc])^p[ind_hyd.beta] ) - 
                        E * ( 1.0 - exp(-Ssm_i/p[ind_hyd.Ssmth]) ) * exp(-Ssn_i/p[ind_hyd.Ssnth])
            Q_rech = Q_rech + fA[i] * Qsn_i * (Ssm_i/p[ind_hyd.Sfc])^p[ind_hyd.beta]
        end

        Suz = max(u[3*n_bands+1],0.0)
        Slz = max(u[3*n_bands+2],0.0)
        Q_perc = p[ind_hyd.cperc] * ( 1.0 - exp(-Suz/p[ind_hyd.Suzth]) )
        Q_0    = p[ind_hyd.k0]*SmoothIncrease(Suz - p[ind_hyd.Suzdiv];dx=p[ind_hyd.Suzth])
        Q_1    = p[ind_hyd.k1]*Suz
        Q_2    = p[ind_hyd.k2]*Slz
        Q_dg   = p[ind_hyd.kdg]*Slz
        du[3*n_bands+1] = Q_rech - Q_perc - Q_0 - Q_1
        du[3*n_bands+2] = Q_perc - Q_2 - Q_dg
        
        if nres > 0
            local Qin = Q_0 + Q_1 + Q_2
            for i in 1:nres
                Sh_i              = max(u[3*n_bands+2+i],0.0)
                Qout              = p[ind_hyd.nres]*p[ind_hyd.kr] * Sh_i
                du[3*n_bands+2+i] = Qin - Qout
                Qin               = Qout
            end
        end

        # check output:
        
        if eltype(du) == Float64   # avoid output during taking of derivatives
            if ! all(isfinite.(du))
                println("*** infinite derivatives on exit from rhs_HBV!")
                Hyperflex_Print_States_Rhs_Par(t,u,du,p,ind_hyd)
            end
        end

    end

    # define Hyperflex model (names of required parameters and rhs function of odes):
    # -------------------------------------------------------------------------------
    
    parnames_required_Hyperflex = ["ce","Suth",
                                   "Sumax","Sgmax","alphau","alphap","alphag","alphab",
                                   "ki","kp","kf","kr","kg","kdg",
                                   "fi","Dm","Dr",
                                   "nres",
                                   "cmelt","amelt","DeltaTsm","Ssnth","Tsfth","Tsmth"]
                                                                    
    function rhs_Hyperflex!(du,u,p,t)

        # state variables: Ss_1, ... , Ss_nbands, Su, Si, Sf, Sg, Sr_1, ... , Sr_nres

        # check input:
        
        if eltype(u) == Float64   # avoid output during taking of derivatives
            if ! all(isfinite.(u))
                println("*** infinite states on entry to rhs_Hyperflex!")
                Hyperflex_Print_States(t,u)
            end
        end

        # evaluate input at time t:

        Pmult  = 1.0; if ind_sens.Pmult>0;  Pmult  = p[ind_sens.Pmult];  end
        Tshift = 0.0; if ind_sens.Tshift>0; Tshift = p[ind_sens.Tshift]; end

        local P = 0.0
        for i in 1:n_bands
            if n_bands == 1
                Tmin = Tmin_fun(t) + Tshift
                Tmax = Tmax_fun(t) + Tshift
                Ptot = Prec_fun(t) * Pmult
            else
                Tmin  = Tmin_fun_band[i](t) + Tshift
                Tmax  = Tmax_fun_band[i](t) + Tshift
                Ptot  = Prec_fun_band[i](t) * Pmult
            end
            Tmean = 0.5*(Tmin+Tmax)
            if Tmin >= p[ind_hyd.Tsfth]
                fsnow = 0.0
            elseif Tmax <= p[ind_hyd.Tsfth]
                fsnow = 1.0
            else
                fsnow = (p[ind_hyd.Tsfth]-Tmin)/(Tmax-Tmin)
            end
            Ssn_i = max(u[i],0.0)
            Qmelt_i = p[ind_hyd.cmelt] *
                      (1.0-p[ind_hyd.amelt]*cos(2.0*pi*t/365.25)) * 
                      SmoothIncrease(Tmean-p[ind_hyd.Tsmth],dx=p[ind_hyd.DeltaTsm]) *
                      (1.0-exp(-Ssn_i/p[ind_hyd.Ssnth]))
            du[i] = fsnow*Ptot - Qmelt_i
            P = P + fA[i] * ( (1.0-fsnow)*Ptot + Qmelt_i )
        end

        if ismissing(Epot_fun)
            local E = Epot_Hamon(0.5*(Tmin_fun(t)+Tmax_fun(t))+Tshift,Lday_fun(t)) * p[ind_hyd.ce]
        else
            local E = Epot_fun(t) * p[ind_hyd.ce]
        end

        # if P > E
        #     local Pn = P - E
        #     local En = 0.0
        # else
        #     local Pn = 0.0
        #     local En = E - P
        # end
        Pn = P
        En = E

        # evaluate right hand side of ODE system (parameters are known from enclosing function):

        local Su = min(max(u[n_bands+1],0.0),p[ind_hyd.Sumax])
        local Si = max(u[n_bands+2],0.0)
        local Sf = max(u[n_bands+3],0.0)
        local Sg = min(max(u[n_bands+4],0.0),p[ind_hyd.Sgmax])
        local Qi = p[ind_hyd.fi] * Pn
        local Pu = (1.0-p[ind_hyd.fi]) * Pn * ( 1.0 - (Su/p[ind_hyd.Sumax])^p[ind_hyd.alphau] )
        local Eu = En * ( 1.0 - exp(-Su/p[ind_hyd.Suth]) )
        local Qp = p[ind_hyd.kp] * p[ind_hyd.Sumax]^(1.0-p[ind_hyd.alphap])*Su^p[ind_hyd.alphap] * ( 1.0 - (Sg/p[ind_hyd.Sgmax])^p[ind_hyd.alphag] )
        local Qu = (1.0-p[ind_hyd.fi]) * Pn - Pu
        local Qm = p[ind_hyd.Dm] * Qu * ( 1.0 - (Sg/p[ind_hyd.Sgmax])^p[ind_hyd.alphag] )
        local Qf = (1.0-p[ind_hyd.Dr]) * (Qu - Qm)
        local Qr = p[ind_hyd.Dr] * (Qu - Qm)
    
        du[n_bands+1] = Pu - Eu - Qp

        du[n_bands+2] = Qi - p[ind_hyd.ki]*Si

        du[n_bands+3] = Qf - p[ind_hyd.kf]*Sf

        du[n_bands+4] = Qp + Qm - (p[ind_hyd.kg]+p[ind_hyd.kdg])*p[ind_hyd.Sgmax]^(1.0-p[ind_hyd.alphab])*Sg^p[ind_hyd.alphab]

        local Qin = Qr
        for i in 1:nres
            Sr_i            = max(u[n_bands+4+i],0.0)
            Qout            = p[ind_hyd.nres]*p[ind_hyd.kr] * Sr_i
            du[n_bands+4+i] = Qin - Qout
            Qin             = Qout
        end

        # check output:
        
        if eltype(du) == Float64   # avoid output during taking of derivatives
            if ! all(isfinite.(du))
                println("*** infinite derivatives on exit from rhs_Hyperflex!")
                Hyperflex_Print_States_Rhs_Par(t,u,du,p,ind_hyd)
            end
        end

    end

    # check input:
    # ------------

    if length(par) != length(parnames)
        error(string("*** Hyperflex_RunModel: length of vector par (",length(par)," ",par,
                     ") not equal to length of vector parnames (",length(parnames),
                     " ",parnames,")"))
    end

    if verbose 
        sigdigits = 6
        eltype_string = string(eltype(par))
        if eltype_string == "Float64"
            println(string("  ",join(string(parnames[j],"=",round(par[j];sigdigits=sigdigits)," ") for j in 1:length(par))))
        elseif occursin("ForwardDiff",eltype_string)
            println(string("  ",join(string(parnames[j],"=",round(ForwardDiff.value(par[j]);sigdigits=sigdigits)," ") for j in 1:length(par))))
        else
            println(string("  ",join(string(parnames[j],"=",par[j]," ") for j in 1:length(par))))
        end
    end

    if length(times) == 1
        error("*** Hyperflex_RunModel: provide more than one output time point")
    else
        for i in 2:length(times)
            if ! ( times[i] > times[i-1] )
                error("*** Hyperflex_RunModel: time points must be increasing")
            end
        end
    end

    if length(par) > 0
        if ! all(isfinite(par))
            println("*** Hyperflex_RunModel: some parameters are infinite on input; no solution calculated")
            println(string("    ",join(string(parnames[i],"=",par[i]," ") for i in 1:length(par))))
            return missing
        end
    end

    # create interpolators for precipitation and evapotranspiration:
    # --------------------------------------------------------------

    # Prec_fun = Interpolations.LinearInterpolation(Prec[:,1],Prec[:,2],extrapolation_bc=Interpolations.Flat())
    # Prec_fun = Interpolations.interpolate(Prec[:,1],Prec[:,2],Interpolations.SteffenMonotonicInterpolation())
    Prec_fun = IntPolFun(Prec[:,1],Prec[:,2])
    # Epot_fun = Interpolations.interpolate(Epot[:,1],Epot[:,2],Interpolations.SteffenMonotonicInterpolation())
    # Epot_fun = Interpolations.LinearInterpolation(Epot[:,1],Epot[:,2],extrapolation_bc=Interpolations.Flat())
    # Epot_fun = Interpolations.interpolate(Epot[:,1],Epot[:,2],Interpolations.LinearMonotonicInterpolation())
    # Epot_fun = BasicInterpolators.LinearInterpolator(Epot[:,1],Epot[:,2])
    if ! ismissing(Epot)
        # Epot_fun = Interpolations.interpolate(Epot[:,1],Epot[:,2],Interpolations.SteffenMonotonicInterpolation())
        Epot_fun = IntPolFun(Epot[:,1],Epot[:,2])
    else
        Epot_fun = missing
    end
    if ! ismissing(Tmin)
        # Tmin_fun = Interpolations.interpolate(Tmin[:,1],Tmin[:,2],Interpolations.SteffenMonotonicInterpolation())
        Tmin_fun = IntPolFun(Tmin[:,1],Tmin[:,2])
    else
        Tmin_fun = missing
    end
    if ! ismissing(Tmax)
        # Tmax_fun = Interpolations.interpolate(Tmax[:,1],Tmax[:,2],Interpolations.SteffenMonotonicInterpolation())
        Tmax_fun = IntPolFun(Tmax[:,1],Tmax[:,2])
    else
        Tmax_fun = missing
    end
    if ! ismissing(Lday)
        # Lday_fun = Interpolations.interpolate(Lday[:,1],Lday[:,2],Interpolations.SteffenMonotonicInterpolation())
        Lday_fun = IntPolFun(Lday[:,1],Lday[:,2])
    else
        Lday_fun = missing
    end
    if ismissing(Epot)
        if ( ismissing(Tmin) | ismissing(Tmax) | ismissing(Lday) )
           error("*** Hyperflex_RunModel: either Epot or Tmin and Tmax and Lday must be provided")
        end
    end

    # define indices and define ODEProblem:

    errmsg = String[]

    ind_sens,errmsg = Hyperflex_GetIndices(parnames,["Pmult","Tshift"])
    Pmult  = 1.0; if ind_sens.Pmult>0;  Pmult  = par[ind_sens.Pmult];  end
    Tshift = 0.0; if ind_sens.Tshift>0; Tshift = par[ind_sens.Tshift]; end

    if model == "1box"

        ind_hyd,errmsg = Hyperflex_GetIndices(parnames,parnames_required_1box)
        if length(errmsg) > 0
            println("*** Hyperflex_RunModel: index/indices not found:")
            println(join(errmsg,"\n"))
            return missing
        end
        ode_prob = DifferentialEquations.ODEProblem(rhs_1box!,[par[ind_hyd.S0]],(minimum(times),maximum(times)),par)
        states_bounds = [[0.0,Float64(Inf)] for i in 1:1]

    elseif model == "2box"

        ind_hyd,errmsg = Hyperflex_GetIndices(parnames,parnames_required_2box)
        if length(errmsg) > 0
            println("*** Hyperflex_RunModel: index/indices not found:")
            println(join(errmsg,"\n"))
            return missing
        end
        ode_prob = DifferentialEquations.ODEProblem(rhs_2box!,par[[ind_hyd.S10,ind_hyd.S20]],(minimum(times),maximum(times)),par)
        states_bounds = [[0.0,Float64(Inf)] for i in 1:2]

    elseif model == "GR4"

        ind_hyd,errmsg = Hyperflex_GetIndices(parnames,parnames_required_GR4)
        if length(errmsg) > 0
            println("*** Hyperflex_RunModel: index/indices not found:")
            println(join(errmsg,"\n"))
            return missing
        end
        nres = round(Int,par[ind_hyd.nres])
        ode_prob = DifferentialEquations.ODEProblem(rhs_GR4!,zeros(nres+2).+0.01,(minimum(times),maximum(times)),par)
        states_bounds = [[0.0,Float64(Inf)] for i in 1:(nres+2)]

    elseif model == "GR4neige"

        ind_hyd,errmsg = Hyperflex_GetIndices(parnames,parnames_required_GR4neige)
        if length(errmsg) > 0
            println("*** Hyperflex_RunModel: index/indices not found:")
            println(join(errmsg,"\n"))
            return missing
        end
        if ismissing(Felevband)
            n_bands = 1
            fA = [1.0]
        else
            n_bands = length(Felevband)
            fA = []
            # Prec_fun_band = Array{Any,1}(undef,n_bands)
            # Tmin_fun_band = Array{Any,1}(undef,n_bands)
            # Tmax_fun_band = Array{Any,1}(undef,n_bands)
            Prec_fun_band = []
            Tmin_fun_band = []
            Tmax_fun_band = []
            for i in 1:n_bands
                fA = vcat(fA,Felevband[i].fA)
                # P_fun_band[i] = LinearInterpolation(Felevband[i].P[:,1],Felevband[i].P[:,2],extrapolation_bc=Interpolations.Flat())
                # Tmin_fun_band[i] = LinearInterpolation(Felevband[i].Tmin[:,1],Felevband[i].Tmin[:,2],extrapolation_bc=Interpolations.Flat())
                # Tmax_fun_band[i] = LinearInterpolation(Felevband[i].Tmax[:,1],Felevband[i].Tmax[:,2],extrapolation_bc=Interpolations.Flat())
                # Prec_fun_band[i] = Interpolations.interpolate(Felevband[i].Prec[:,1],Felevband[i].Prec[:,2],Interpolations.SteffenMonotonicInterpolation())
                # Tmin_fun_band[i] = Interpolations.interpolate(Felevband[i].Tmin[:,1],Felevband[i].Tmin[:,2],Interpolations.SteffenMonotonicInterpolation())
                # Tmax_fun_band[i] = Interpolations.interpolate(Felevband[i].Tmax[:,1],Felevband[i].Tmax[:,2],Interpolations.SteffenMonotonicInterpolation())
                # Prec_fun_band[i] = IntPolFun(Felevband[i].Prec[:,1],Felevband[i].Prec[:,2])
                # Tmin_fun_band[i] = IntPolFun(Felevband[i].Tmin[:,1],Felevband[i].Tmin[:,2])
                # Tmax_fun_band[i] = IntPolFun(Felevband[i].Tmax[:,1],Felevband[i].Tmax[:,2])
                Prec_fun_band = vcat(Prec_fun_band,IntPolFun(Felevband[i].Prec[:,1],Felevband[i].Prec[:,2]))
                Tmin_fun_band = vcat(Tmin_fun_band,IntPolFun(Felevband[i].Tmin[:,1],Felevband[i].Tmin[:,2]))
                Tmax_fun_band = vcat(Tmax_fun_band,IntPolFun(Felevband[i].Tmax[:,1],Felevband[i].Tmax[:,2]))
            end

        end
        nres = round(Int,par[ind_hyd.nres])
        ode_prob = DifferentialEquations.ODEProblem(rhs_GR4neige!,zeros(2*n_bands+nres+2).+0.01,(minimum(times),maximum(times)),par)
        states_bounds = [[-Inf Inf],[0.0 Inf]]
        if n_bands > 1
            for i in 2:n_bands; states_bounds=vcat(states_bounds,[[-Inf Inf],[0.0 Inf]]); end
        end
        states_bounds = vcat(states_bounds,[[0.0,Inf] for i in 1:(nres+2)])
        
    elseif model == "HBV"

        ind_hyd,errmsg = Hyperflex_GetIndices(parnames,parnames_required_HBV)
        if length(errmsg) > 0
            println("*** Hyperflex_RunModel: index/indices not found:")
            println(join(errmsg,"\n"))
            return missing
        end
        if ismissing(Felevband)
            n_bands = 1
            fA = [1.0]
        else
            n_bands = length(Felevband)
            fA = []
            # Prec_fun_band = Array{Any,1}(undef,n_bands)
            # Tmin_fun_band = Array{Any,1}(undef,n_bands)
            # Tmax_fun_band = Array{Any,1}(undef,n_bands)
            Prec_fun_band = []
            Tmin_fun_band = []
            Tmax_fun_band = []
            for i in 1:n_bands
                fA = vcat(fA,Felevband[i].fA)
                # P_fun_band[i] = LinearInterpolation(Felevband[i].P[:,1],Felevband[i].P[:,2],extrapolation_bc=Interpolations.Flat())
                # Tmin_fun_band[i] = LinearInterpolation(Felevband[i].Tmin[:,1],Felevband[i].Tmin[:,2],extrapolation_bc=Interpolations.Flat())
                # Tmax_fun_band[i] = LinearInterpolation(Felevband[i].Tmax[:,1],Felevband[i].Tmax[:,2],extrapolation_bc=Interpolations.Flat())
                # Prec_fun_band[i] = Interpolations.interpolate(Felevband[i].Prec[:,1],Felevband[i].Prec[:,2],Interpolations.SteffenMonotonicInterpolation())
                # Tmin_fun_band[i] = Interpolations.interpolate(Felevband[i].Tmin[:,1],Felevband[i].Tmin[:,2],Interpolations.SteffenMonotonicInterpolation())
                # Tmax_fun_band[i] = Interpolations.interpolate(Felevband[i].Tmax[:,1],Felevband[i].Tmax[:,2],Interpolations.SteffenMonotonicInterpolation())
                # Prec_fun_band[i] = IntPolFun(Felevband[i].Prec[:,1],Felevband[i].Prec[:,2])
                # Tmin_fun_band[i] = IntPolFun(Felevband[i].Tmin[:,1],Felevband[i].Tmin[:,2])
                # Tmax_fun_band[i] = IntPolFun(Felevband[i].Tmax[:,1],Felevband[i].Tmax[:,2])
                Prec_fun_band = vcat(Prec_fun_band,IntPolFun(Felevband[i].Prec[:,1],Felevband[i].Prec[:,2]))
                Tmin_fun_band = vcat(Tmin_fun_band,IntPolFun(Felevband[i].Tmin[:,1],Felevband[i].Tmin[:,2]))
                Tmax_fun_band = vcat(Tmax_fun_band,IntPolFun(Felevband[i].Tmax[:,1],Felevband[i].Tmax[:,2]))
            end
        end
        nres = round(Int,par[ind_hyd.nres])
        ode_prob = DifferentialEquations.ODEProblem(rhs_HBV!,zeros(3*n_bands+2+nres).+0.01,(minimum(times),maximum(times)),par)
        states_bounds = [[0.0,Float64(Inf)] for i in 1:(3*n_bands+2+nres)]

    elseif model == "Hyperflex"

        ind_hyd,errmsg = Hyperflex_GetIndices(parnames,parnames_required_Hyperflex)
        if length(errmsg) > 0
            println("*** Hyperflex_RunModel: index/indices not found:")
            println(join(errmsg,"\n"))
            return missing
        end
        if ismissing(Felevband)
            n_bands = 1
            fA = [1.0]
        else
            n_bands = length(Felevband)
            fA = []
            # Prec_fun_band = Array{Any,1}(undef,n_bands)
            # Tmin_fun_band = Array{Any,1}(undef,n_bands)
            # Tmax_fun_band = Array{Any,1}(undef,n_bands)
            Prec_fun_band = []
            Tmin_fun_band = []
            Tmax_fun_band = []
            for i in 1:n_bands
                fA = vcat(fA,Felevband[i].fA)
                # P_fun_band[i] = LinearInterpolation(Felevband[i].P[:,1],Felevband[i].P[:,2],extrapolation_bc=Interpolations.Flat())
                # Tmin_fun_band[i] = LinearInterpolation(Felevband[i].Tmin[:,1],Felevband[i].Tmin[:,2],extrapolation_bc=Interpolations.Flat())
                # Tmax_fun_band[i] = LinearInterpolation(Felevband[i].Tmax[:,1],Felevband[i].Tmax[:,2],extrapolation_bc=Interpolations.Flat())
                # Prec_fun_band[i] = Interpolations.interpolate(Felevband[i].Prec[:,1],Felevband[i].Prec[:,2],Interpolations.SteffenMonotonicInterpolation())
                # Tmin_fun_band[i] = Interpolations.interpolate(Felevband[i].Tmin[:,1],Felevband[i].Tmin[:,2],Interpolations.SteffenMonotonicInterpolation())
                # Tmax_fun_band[i] = Interpolations.interpolate(Felevband[i].Tmax[:,1],Felevband[i].Tmax[:,2],Interpolations.SteffenMonotonicInterpolation())
                # Prec_fun_band[i] = IntPolFun(Felevband[i].Prec[:,1],Felevband[i].Prec[:,2])
                # Tmin_fun_band[i] = IntPolFun(Felevband[i].Tmin[:,1],Felevband[i].Tmin[:,2])
                # Tmax_fun_band[i] = IntPolFun(Felevband[i].Tmax[:,1],Felevband[i].Tmax[:,2])
                Prec_fun_band = vcat(Prec_fun_band,IntPolFun(Felevband[i].Prec[:,1],Felevband[i].Prec[:,2]))
                Tmin_fun_band = vcat(Tmin_fun_band,IntPolFun(Felevband[i].Tmin[:,1],Felevband[i].Tmin[:,2]))
                Tmax_fun_band = vcat(Tmax_fun_band,IntPolFun(Felevband[i].Tmax[:,1],Felevband[i].Tmax[:,2]))
            end
        end
        nres = round(Int,par[ind_hyd.nres])
        ode_prob = DifferentialEquations.ODEProblem(rhs_Hyperflex!,zeros(n_bands+nres+4).+0.01,(minimum(times),maximum(times)),par)
        states_bounds = [[0.0,Float64(Inf)] for i in 1:(n_bands+nres+4)]

    else

        error(string("Hyperflex_RunModel: unknown model: ",model))

    end

    # solve ODE problem:
    # ------------------

    if isa(algorithm,Vector); alg = algorithm; else alg = [algorithm]; end

    res = missing
    for j in 1:length(alg)

        if alg[j] == "Seq1dODESolver"

            T = eltype(par)
            res = Seq1dODESolver(ode_prob.f,
                                 ode_prob.u0,
                                 2.0 .* ones(T,length(ode_prob.u0)),
                                 times,
                                 dt,
                                 ode_prob.p,
                                 #algorithm  = "ITP",
                                 bounds     = states_bounds,
                                 method     = "ImplicitEuler",
                                 #method     = "ExplicitEuler",
                                 useNLsolve = true,
                                 reltol     = reltol,
                                 abstol     = abstol,
                                 verbose    = verbose)
            if ismissing(res)
                println("*** Seq1dODESolver failed")
                flush(stdout)
            else
                res = transpose(res)
                break
            end

        else

            res = DifferentialEquations.solve(ode_prob,
                                              alg[j],
                                              saveat   = times,
                                              # sensalg  = DiffEqSensitivity.SensitivityADPassThrough(),
                                              sensealg = DiffEqSensitivity.QuadratureAdjoint(),
                                              # sensalg  = DiffEqSensitivity.ForwardSensitivity(),
                                              # sensalg  = DiffEqSensitivity.ForwardDiffSensitivity(convert_tspan=false),
                                              # sensalg  = DiffEqSensitivity.BacksolveAdjoint(),
                                              dt       = dt,
                                              dtmax    = dtmax,
                                              reltol   = reltol,
                                              abstol   = abstol,
                                              maxiters = 1000000)
            if res.retcode != :Success
                println(string("*** DifferentialEquations.solve failed with algorithm ",
                               split(split(split(String(Symbol(alg[j])),".")[min(2,end)],"(")[1],"{")[1],
                               "; return code: ",String(res.retcode)))
                println(string("    ",join(string(parnames[i],"=",par[i]," ") for i in 1:length(par))))
                flush(stdout)
            else
                if j > 1.0
                    println(string("*** DifferentialEquations.solve succeeded with algorithm ",
                                   split(split(split(String(Symbol(alg[j])),".")[min(2,end)],"(")[1],"{")[1]))
                    flush(stdout)
                end
                break
            end

        end

    end

    if ismissing(res); return missing; end
    if res.retcode != :Success; return missing; end

    # combine solution with additional variables to a matrix:
    # -------------------------------------------------------

    # note: the first and last column must be time and discharge at the outlet of the catchment

    Tmin_val = repeat([NaN],length(times))
    if ! ismissing(Tmin_fun)
        Tmin_val = Tmin_fun.(times) .+ Tshift  # Tmin
    end
    Tmax_val = repeat([NaN],length(times))
    if ! ismissing(Tmax_fun)
        Tmax_val = Tmax_fun.(times) .+ Tshift  # Tmax
    end
    Epot_val = repeat([NaN],length(times))
    if ismissing(Epot_fun)                     # Epot
        Epot_val = Epot_Hamon.(0.5.*(Tmin_fun.(times).+Tmax_fun.(times)).+Tshift,Lday_fun.(times)) .* par[ind_hyd.ce]
    else
        Epot_val = Epot_fun.(times) .* par[ind_hyd.ce]
    end
    Prec_val = Prec_fun.(times) .* Pmult       # Prec

    if model == "1box"

        S = max.(res[1,:],0.0)
        Q = par[ind_hyd.k]/par[ind_hyd.Styp]^(par[ind_hyd.alpha]-1.0).*S.^par[ind_hyd.alpha]

        results = hcat(times,        # t
                       Tmin_val,     # Tmin
                       Tmax_val,     # Tmax
                       Prec_val,     # Prec
                       Epot_val,     # Epot
                       S,            # S
                       Q)            # Q
        colnames = ["t","Tmin","Tmax","Prec","Epot","S","Q"]

    elseif model == "2box"

        S1  = max.(res[1,:],0.0)
        S2  = max.(res[2,:],0.0)
        Q1  = par[ind_hyd.k1]/par[ind_hyd.Styp]^(par[ind_hyd.alpha1]-1.0).*S1.^par[ind_hyd.alpha1]
        Q2  = par[ind_hyd.k2]/par[ind_hyd.Styp]^(par[ind_hyd.alpha2]-1.0).*S2.^par[ind_hyd.alpha2]
        Q12 = par[ind_hyd.k12]/par[ind_hyd.Styp]^(par[ind_hyd.alpha12]-1.0).*S1.^par[ind_hyd.alpha12]

        results = hcat(times,        # t
                       Tmin_val,     # Tmin
                       Tmax_val,     # Tmax
                       Prec_val,     # Prec
                       Epot_val,     # Epot
                       S1,           # S1
                       S2,           # S2
                       Q1,           # Q1
                       Q2,           # Q2
                       Q12,          # Q12
                       Q1 .+ Q2)     # Q
        colnames = ["t","Tmin","Tmax","Prec","Epot","S1","S2","Q1","Q2","Q12","Q"]

    elseif model == "GR4"

        SS  = max.(res[1,:],0.0)
        SR  = max.(res[nres+2,:],0.0)
        Quh = par[ind_hyd.nres]/par[ind_hyd.x4] .* max.(res[nres+1,:],0.0)
        QF  = par[ind_hyd.x2]/par[ind_hyd.x3]^par[ind_hyd.omega] .* SR.^par[ind_hyd.omega]
        QR  = par[ind_hyd.x3]^(1.0-par[ind_hyd.gamma]) / ( (par[ind_hyd.gamma]-1.0)*par[ind_hyd.Ut] ) .* SR.^par[ind_hyd.gamma]
        Qd  = max.( 0.0 , (1.0-par[ind_hyd.Phi]).*Quh .+ QF )
        Q   = QR .+ Qd

        results = hcat(times,                    # t
                       Tmin_val,                 # Tmin
                       Tmax_val,                 # Tmax
                       Prec_val,                 # Prec
                       Epot_val,                 # Epot
                       SS)                       # SS
        for j in 1:nres
            results = hcat(results,res[1+j,:])   # Sr[j]
        end
        results = hcat(results,
                       SR,                       # SR
                       Q)                        # Q
        colnames = vcat(["t","Tmin","Tmax","Prec","Epot","SS"],string.("Sr",lpad.(1:nres,2,"0")),["SR","Q"])

    elseif model == "GR4neige"

        SS  = max.(res[2*n_bands+1,:],0.0)
        SR  = max.(res[2*n_bands+nres+2,:],0.0)
        Quh = par[ind_hyd.nres]/par[ind_hyd.x4] .* max.(res[2*n_bands+nres+1,:],0.0)
        QF  = par[ind_hyd.x2]/par[ind_hyd.x3]^par[ind_hyd.omega] .* SR.^par[ind_hyd.omega]
        QR  = par[ind_hyd.x3]^(1.0-par[ind_hyd.gamma]) / ( (par[ind_hyd.gamma]-1.0)*par[ind_hyd.Ut] ) .* SR.^par[ind_hyd.gamma]
        Qd  = max.( 0.0 , (1.0-par[ind_hyd.Phi]).*Quh .+ QF )
        Q   = QR .+ Qd

        results = hcat(times,                    # t
                       Tmin_val,                 # Tmin
                       Tmax_val,                 # Tmax
                       Prec_val,                 # Prec
                       Epot_val)                 # Epot
        for j in 1:n_bands
            results = hcat(results,
                           max.(res[2*j,:],0.0), # Ssn[j]
                           res[2*j-1,:])         # Tsn[j]
        end
        results = hcat(results,SS)               # SS
        for j in 1:nres
            results = hcat(results,max.(res[2*n_bands+1+j,:],0.0))  # Sr[j]
        end
        results = hcat(results,
                       SR,                       # SR
                       Q)                        # Q
        colnames = vcat(["t","Tmin","Tmax","Prec","Epot"],
                        reduce(vcat,[string.(["Ssn","Tsn"],lpad(j,2,"0")) for j in 1:n_bands]),
                        "SS",string.("Sr",lpad.(1:nres,2,"0")),["SR","Q"])

    elseif model == "HBV"

        Suz     = max.(res[3*n_bands+1,:],0.0)
        Slz     = max.(res[3*n_bands+2,:],0.0)
        Q_0 = par[ind_hyd.k0].*SmoothIncrease.(Suz .- par[ind_hyd.Suzdiv];dx=par[ind_hyd.Suzth])
        Q_1 = par[ind_hyd.k1].*Suz
        Q_2 = par[ind_hyd.k2].*Slz
        if nres == 0
            Q   = Q_0 .+ Q_1 .+ Q_2
        else
            Sr_nres = max.(res[3*n_bands+2+nres,:],0.0)
            Q       = (par[ind_hyd.nres]*par[ind_hyd.kr]).*Sr_nres
        end

        results = hcat(times,                                       # t
                       Tmin_val,                                    # Tmin
                       Tmax_val,                                    # Tmax
                       Prec_val,                                    # Prec
                       Epot_val)                                    # Epot
        for j in 1:n_bands
            results = hcat(results,
                           max.(res[3*j-2,:],0.0),                  # Ssn[j]
                           max.(res[3*j-1,:],0.0),                  # Ssw[j]
                           max.(res[3*j,:],0.0))                    # Ssm[j]
        end
        results = hcat(results,Suz)                                 # Suz
        results = hcat(results,Slz)                                 # Slz
        if nres > 0
            for j in 1:nres
                results = hcat(results,max.(res[3*n_bands+2+j,:],0.0))  # Sr[j]
            end
        end
        results = hcat(results,Q)                                   # Q
        colnames = vcat(["t","Tmin","Tmax","Prec","Epot"],
                        reduce(vcat,[string.(["Ssn","Ssw","Ssm"],lpad(j,2,"0")) for j in 1:n_bands]),
                        "Suz","Slz")
        if nres > 0; colnames = vcat(colnames,string.("Sr",lpad.(1:nres,2,"0"))); end
        colnames = vcat(colnames,"Q")

    elseif model == "Hyperflex"

        Su  = max.(res[n_bands+1,:],0.0)
        Si  = max.(res[n_bands+2,:],0.0)
        Sf  = max.(res[n_bands+3,:],0.0)
        Sg  = max.(res[n_bands+4,:],0.0)
        Q0  = par[ind_hyd.ki].*Si
        Q1  = par[ind_hyd.kf].*Sf
        Q2  = par[ind_hyd.nres]*par[ind_hyd.kr] .* max.(res[n_bands+nres+4,:],0.0)
        Q3  = par[ind_hyd.kg]*par[ind_hyd.Sgmax]^(1.0-par[ind_hyd.alphab]).*Sg.^par[ind_hyd.alphab]
        Q   = Q0 .+ Q1 .+ Q2 .+ Q3

        results = hcat(times,                                   # t
                       Tmin_val,                                # Tmin
                       Tmax_val,                                # Tmax
                       Prec_val,                                # Prec
                       Epot_val)                                # Epot
        for j in 1:n_bands
            results = hcat(results,max.(res[j,:],0.0))          # Ssn[j]
        end
        results = hcat(results,
                       Su,                                      # Su
                       Si,                                      # Si
                       Sf,                                      # Sf
                       Sg)                                      # Sg
        for j in 1:nres
           results = hcat(results,max.(res[n_bands+4+j,:],0.0)) # Sr[j]
        end
        results = hcat(results,Q)                               # Q
        colnames = vcat(["t","Tmin","Tmax","Prec","Epot"],
                        string.("Ssn",lpad.(1:n_bands,2,"0")),
                        ["Su","Si","Sf","Sg"],string.("Sr",lpad.(1:nres,2,"0")),["Q"])

    end
    
    return (results=results, colnames=colnames)

end

# Get a function to run the model with predefined settings except
# output time points and parameter values (and their names):
# [for inference and for showing results using the same settings]

function Hyperflex_Get_RunModel(
            model::String,                             # name of model
            par_hyd::AbstractVector,                   # parameters
            parnames_hyd::Vector{String},              # parameter names
            Prec::Matrix{Float64},                     # 2 col matrix time, precip
            Epot::Union{Missing,Matrix{Float64}},      # 2 col matrix time, pot. evap
            Tmin::Union{Missing,Matrix{Float64}},      # 2 col matrix time, Tmin
            Tmax::Union{Missing,Matrix{Float64}},      # 2 col matrix time, Tmax
            Lday::Union{Missing,Matrix{Float64}},      # 2 col matrix time, daylight length Lday
            Felevband::Union{Missing,Array{Fband,1}};  # elevation band forcing (A) 
            algorithm = DifferentialEquations.Euler(), # integration algorithm
            dt = 0.1*(maximum(P[:,1])-minimum(P[:,1]))/size(P)[1],
                                      # time step for fixed time step integrators
            dtmax = (maximum(P[:,1])-minimum(P[:,1]))/size(P)[1], 
                                      # maximum time step for variable time step int.
                                      # (adapt to not miss input peaks!)
            reltol = 1e-4,                             # relative tolerance of integrator
            abstol = 1e-5)                             # absolute tolerance of integrator

    function runmodel(times,par,parnames;verbose=false)
    
        return Hyperflex_RunModel(model,vcat(par,par_hyd),vcat(parnames,parnames_hyd),
                                  Prec,Epot,Tmin,Tmax,Lday,Felevband,times;
                                  algorithm=algorithm,
                                  dt=dt,dtmax=dtmax,reltol=reltol,abstol=abstol,
                                  verbose=verbose)

    end
    
    return runmodel
end


# Define log likelihood function:
# -------------------------------

# Observation or lumped error model:

function Hyperflex_Logpdfobs_IndepHomoscedasticNormal(
            # Qsim::Array{Any,1},
            Qsim::AbstractVector,
            Qobs::Vector{Float64};
            par::AbstractArray = [], 
            parnames::Vector{String} = String[])

    ind_lik,errmsg = Hyperflex_GetIndices(parnames,["sigmaQ"])
    if length(errmsg) > 0; return errmsg; end
    # ind_sigmaQ = findfirst(isequal("sigmaQ"),parnames)
    # if isnothing(ind_sigmaQ); return "parameter sigmaQ not found"; end
 
    return sum(DistributionsAD.logpdf.(DistributionsAD.Normal.(Qsim,par[ind_lik.sigmaQ]),Qobs))

end

# Get log likelihood function with predefined settings just as a function
# of selected parameter values:

function Hyperflex_Get_Loglikeli(
            runmodel,                       # function to run the model in the form:
                                            # runmodel(times;par=par,parnames=parnames)
            logpdfobs,                      # log observation likelihood
            par_lik,
            parnames_lik,
            Q::Matrix{Float64},             # 2 col matrix time, observed discharge
            parnames::Vector{String};       # names of parameters to be expected as argument
            starttime::Float64 = NaN,       # time to start the simulation
            verbose = false)

    ind = findall(x->isfinite(x),Q[:,2])  # indices with observed discharge
    times = Q[ind,1]
    Qobs  = Q[ind,2]

    if length(times) > 1
        for i in 2:length(times)
            if ! ( times[i] > times[i-1] )
                error("Hyperflex_Get_Loglikeli: time points of observed discharge must be increasing")
            end
        end
    end

    times_sim = times
    if isfinite(starttime)
        if ! ( starttime < minimum(times) )
            error("Hyperflex_Get_Loglikeli: startime must either be NaN or smaller than any of the times")
        end
        pushfirst!(times_sim,starttime)
    end

    function loglikeli(par)

        if length(par) != length(parnames)
            error(string("*** loglikeli: length of vector par (",length(par)," ",par,
                         ") not equal to length of vector parnames (",length(parnames),
                         " ",parnames,")"))
        end
        res = runmodel(times_sim,par,parnames;verbose=verbose)
        if ismissing(res)
            return Float64(-Inf)
            # return - 1.0e6
        end

        Qsim = res.results[:,end]
        if isfinite(starttime); Qsim = Qsim[2:end]; end

        ll = logpdfobs(Qsim,Qobs;
                       par=vcat(par,par_lik),parnames=vcat(parnames,parnames_lik))

        return ll
    end

    return loglikeli
 
end


# Get log prior function with predefined defaults just as a function of
# selected parameter values (and their names):

function Hyperflex_Get_LogPrior(logprior,parnames_est,par,parnames)

    function lp(par_est)

        if length(par_est) != length(parnames_est) 
            error("Hyperflex_LogPrior: length of par is not equal to lenth of parnames")
        end

        return logprior(par_est,parnames_est,par,parnames)
    end

    return lp
end

function Hyperflex_Get_LogPosterior(logprior,loglikeli;verbose=false)

    function logposterior(par)

        lpri = logprior(par)
        llik = NaN; if isfinite(lpri); llik = loglikeli(par); end
        lpost = lpri + llik

        if verbose
            Zygote.ignore() do
            sigdigits = 6
            type_string = string(typeof(lpost))
            if type_string == "Float64"
                println("    logpost = ",round(lpost,sigdigits=sigdigits),
                        " logpri = ",round(lpri,sigdigits=sigdigits),
                        " loglikeli = ",round(llik,sigdigits=sigdigits))
            elseif occursin("ForwardDiff",type_string)
                println("    logpost = ",round(ForwardDiff.value(lpost),sigdigits=sigdigits),
                        " logpri = ",round(ForwardDiff.value(lpri),sigdigits=sigdigits),
                        " loglikeli = ",round(ForwardDiff.value(llik),sigdigits=sigdigits))
            else
                println("    logpost = ",lpost," logpri = ",lpri," loglikeli = ",llik)
            end   
            end 
        end

        return lpost

    end

    return logposterior
end


function Hyperflex_InferParameters(
            logposterior::Function,         # function to return log posterior as a function of par 
            par::AbstractArray,             # initial values of parameters to be estimated
            parnames::Array{String,1};      # names of parameters par
            Trans = missing,
            method::String = "AdvancedHMC", # inference method:
                                            # "AdvancedHMC": Hamiltonian Monte Carlo
                                            # "DynamicHMC":  alternative impl. of Hamiltonian Monte Carlo
                                            # "VI":          Variational Inference (mean-field)
            AutoDiff = "ForwardDiff",       # library for automatic differentiation
            n_samples::Int = 1000,          # length of the Markov chain to be sampled
            n_adapt::Int = n_samples÷2,     # number of iterations used for adaptation
            logfile = missing,              # optional file name for logging progress
            verbose = false)                # report inference progress (currently gradient evaluation)

    # transform parameters and log posterior:
    # ---------------------------------------

    if ismissing(Trans)
        Trans = Array{TransPar}(undef,length(par))
        for i in 1:length(par)
            Trans[i] = TransParUnbounded()
        end
    end

    par_trans = TransformForward(Trans,par)
	
    function logposterior_trans(par_trans)
        if ! all(isfinite(par_trans)); return Float64(-Inf); end
        par = TransformBackward(Trans,par_trans)
        return logposterior(par) + CorrectLogPdf(Trans,par_trans)
    end

    function neg_logposterior_trans(par_trans)
        return - logposterior_trans(par_trans)
    end    

    function grad_logposterior_trans(par_trans)

        # calculate gradient:
        
        if AutoDiff == "ForwardDiff"

            grad = ForwardDiff.gradient(logposterior_trans,par_trans)

        else

            grad = Zygote.gradient(logposterior_trans,par_trans)[1]

        end
*
        return grad
    end

    function grad_neg_logposterior_trans(par_trans)
        return - grad_logposterior_trans(par_trans)
    end

#=
    function grad_logposterior_trans(par_trans)

        # calculate gradient with ForwardDiff:
        
        grad = ForwardDiff.gradient(logposterior_trans,par_trans)
        norm_grad = sqrt(sum(grad.^2))
        if verbose
            println(string("  norm of gradient of transformed logposterior = "),norm_grad)
            println(string("  ",join(string("dlpt/d",parnames[i],"=",grad[i]," ") for i in 1:length(par_trans))))
            flush(stdout)
        end

        # replace component for snow model parameters with finite difference approximation:

        if correct_gradient
            dp = 0.001
            lpt = missing
            for j in 1:length(grad)
                if (parnames[j] == "cmelt") | (parnames[j] == "amelt") | (parnames[j] == "thetaG1") | (parnames[j] == "thetaG2")
                    p_trans = copy(par_trans)
                    p_trans[j] = par_trans[j] + log(1.0-dp)
                    if ismissing(lpt); lpt = logposterior_trans(par_trans); end 
                    grad[j] = (logposterior_trans(p_trans)-lpt)/log(1.0-dp)
                end
            end
            if verbose & (! ismissing(lpt))
                norm_grad = sqrt(sum(grad.^2))
                println(string("  norm of corrected gradient of transformed logposterior = "),norm_grad)
                println(string("  ",join(string("dlpt/d",parnames[i],"=",grad[i]," ") for i in 1:length(par_trans))))
                flush(stdout)
            end
        end

        return grad
    end

    function logpost_and_grad_trans(par_trans)
        return logposterior_trans(par_trans), grad_logposterior_trans(par_trans)
    end

    function grad_neg_logposterior_trans!(storage,par_trans)
        storage .= - grad_logposterior_trans(par_trans)
    end
    =#

    if method == "AdvancedHMC"

        # set up sampler:
        # ---------------

        metric      = AdvancedHMC.DiagEuclideanMetric(length(par_trans))
        # hamiltonian = AdvancedHMC.Hamiltonian(metric, logposterior_trans, ForwardDiff)
        # hamiltonian = AdvancedHMC.Hamiltonian(metric, logposterior_trans, logpost_and_grad_trans)
        hamiltonian = AdvancedHMC.Hamiltonian(metric, logposterior_trans, 
                                              if AutoDiff=="Zygote"; Zygote; else ForwardDiff; end)
        initial_eps = AdvancedHMC.find_good_stepsize(hamiltonian,par_trans)
        integrator  = AdvancedHMC.Leapfrog(initial_eps)
        #proposal    = AdvancedHMC.NUTS{MultinomialTS, ClassicNoUTurn}(integrator,max_depth=10,Δ_max=1000.0)
        #proposal    = AdvancedHMC.StaticTrajectory(integrator,20)
        proposal    = AdvancedHMC.NUTS{AdvancedHMC.MultinomialTS, AdvancedHMC.GeneralisedNoUTurn}(integrator)
        adaptor     = AdvancedHMC.StanHMCAdaptor(AdvancedHMC.MassMatrixAdaptor(metric), AdvancedHMC.StepSizeAdaptor(0.8, integrator))

        # sample:
        # -------

        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: Starting AdvancedHMC sampling (autodiff ",AutoDiff,")"))
        flush(stdout)
        samples_trans, stats_trans = AdvancedHMC.sample(hamiltonian, proposal, par_trans, n_samples, 
                                                        adaptor, n_adapt; progress=true)
        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: AdvancedHMC sampling completed (autodiff ",AutoDiff,")"))
        flush(stdout)

        # tranform back to original scale:
        # --------------------------------

        samples = [collect(TransformBackward(Trans,s)) for s in samples_trans]

    elseif method == "DynamicHMC"

        trans_identity = TransformVariables.as(Array, TransformVariables.asℝ, length(par_trans))
        # trans_identity = TransformVariables.as((par=TransformVariables.as(Array,length(par_trans)),))

        logposttrans = LogDensityProblems.TransformedLogDensity(
                           trans_identity,DynamicHMC_InferenceProblem(logposterior_trans))

        logposttrans_withgrad = LogDensityProblems.ADgradient(
                                    if AutoDiff == "ForwardDiff"; :ForwardDiff else :Zygote; end,
                                    logposttrans)

        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: Starting inference with DynamicHMC (autodiff ",AutoDiff,")"))
        flush(stdout)
        results = DynamicHMC.mcmc_with_warmup(Random.GLOBAL_RNG,logposttrans_withgrad,n_samples;
                                              initialization = (q=par_trans,),
                                              reporter=DynamicHMC.LogProgressReport())
        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: Inference with DynamicHMC completed (autodiff ",AutoDiff,")"))
        flush(stdout)

        results = TransformVariables.transform.(trans_identity,results.chain)
        samples = vcat((hcat(s...) for s in results) ...)

    elseif method == "VI"

        advi = AdvancedVI.ADVI(10, 10000)
        d = length(par)
        getq(theta) = DistributionsAD.TuringDiagMvNormal(theta[1:d],exp.(theta[(d+1):(2*d)]))
        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: Starting variational inference with AdvancedVI"))
        flush(stdout)
        res_vi = AdvancedVI.vi(logposterior_trans,
                               advi,
                               getq,
                               vcat(par_trans,par_trans.-2))  # sd half of mean (if log scale)
        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: Variational inference with AdvancedVI completed"))
        flush(stdout)
    
        par_trans_dist = DistributionsAD.TuringDiagMvNormal(res_vi.m,res_vi.σ)
    
        samples = transpose(rand(par_trans_dist,n_samples))
        for j in 1:size(samples)[1]
            samples[j,:] = TransformBackward(Trans,samples[j,:])
        end

    elseif method == "Optimization"

        colnames = copy(parnames); push!(colnames,"logpost")

        steps = Array{Float64,1}
        if ! ismissing(logfile)
            lp = logposterior(par)
            steps = copy(par); push!(steps,lp)
            steps = transpose(steps)
            CSV.write(logfile,Tables.table(steps;header=colnames),delim='\t');
        end

        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: Starting optimization with Optim.optimize"))
        flush(stdout)
        res = Optim.optimize(neg_logposterior_trans,
                             # grad_neg_logposterior_trans!,
                             par_trans,
                             Optim.LBFGS(linesearch = Optim.BackTracking(order=2)),
                             # see https://discourse.julialang.org/t/optim-with-occasionally-non-finite-values/10174
                             # for the linesearch option
                             autodiff=:forward)   # Zygote is not supported
        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: optimization with Optim.optimize completed"))
        flush(stdout)

        samples = transpose(TransformBackward(Trans,Optim.minimizer(res)))
        if ! all(isfinite.(samples)); samples = transpose(par); end

        if ! ismissing(logfile)
            par_opt = TransformBackward(Trans,Optim.minimizer(res))
            lp = logposterior(par_opt)
            step = copy(par_opt); push!(step,lp)
            steps = vcat(steps,transpose(step))
            CSV.write(logfile,Tables.table(steps;header=colnames),delim='\t');
        end

    elseif method == "GradFreeOpt"

        colnames = copy(parnames); push!(colnames,"logpost")

        steps = Array{Float64,1}
        if ! ismissing(logfile)
            lp = logposterior(par)
            steps = copy(par); push!(steps,lp)
            steps = transpose(steps)
            CSV.write(logfile,Tables.table(steps;header=colnames),delim='\t');
        end

        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: Starting gradient-free optimization with Optim.optimize"))
        flush(stdout)
        res = Optim.optimize(neg_logposterior_trans,
                             # grad_neg_logposterior_trans!,
                             par_trans,
                             Optim.NelderMead())
        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: gradient-free optimization with Optim.optimize completed"))
        flush(stdout)

        samples = transpose(TransformBackward(Trans,Optim.minimizer(res)))
        if ! all(isfinite.(samples)); samples = transpose(par); end

        if ! ismissing(logfile)
            par_opt = TransformBackward(Trans,Optim.minimizer(res))
            lp = logposterior(par_opt)
            step = copy(par_opt); push!(step,lp)
            steps = vcat(steps,transpose(step))
            CSV.write(logfile,Tables.table(steps;header=colnames),delim='\t');
        end

    elseif method == "StochasticOpt"

        par_trans_opt = copy(par_trans)

        colnames = copy(parnames); push!(colnames,"logpost")

        steps = Array{Float64,1}
        if ! ismissing(logfile)
            lp = logposterior(par)
            steps = copy(par); push!(steps,lp)
            steps = transpose(steps)
            CSV.write(logfile,Tables.table(steps;header=colnames),delim='\t');
        end
        
        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: Starting stochastic optimization (autodiff ",AutoDiff,")"))
        flush(stdout)
        for i in 1:n_samples
            Flux.Optimise.update!(Flux.Optimise.ADADelta(0.9),par_trans_opt,grad_neg_logposterior_trans(par_trans_opt))
            #Flux.Optimise.update!(Flux.Optimize.AMSGrad(),par_trans_opt,grad_neg_logposterior_trans(par_trans_opt))

            if mod(i,10) == 0
                if ! ismissing(logfile)
                    par_opt = TransformBackward(Trans,par_trans_opt)
                    lp = logposterior(par_opt)
                    step = copy(par_opt); push!(step,lp)
                    steps = vcat(steps,transpose(step))
                    CSV.write(logfile,Tables.table(steps;header=colnames),delim='\t');
                end
            end
        end
        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: ",n_samples," stochastic optimization steps completed (autodiff ",AutoDiff,")"))
        flush(stdout)

        samples = transpose(TransformBackward(Trans,par_trans_opt))
        if ! all(isfinite.(samples)); samples = transpose(par); end
    
    elseif method == "GalacticOptim"

        # XXX to be checked!

        colnames = copy(parnames); push!(colnames,"logpost")

        steps = Array{Float64,1}
        if ! ismissing(logfile)
            lp = logposterior(par)
            steps = copy(par); push!(steps,lp)
            steps = transpose(steps)
            CSV.write(logfile,Tables.table(steps;header=colnames),delim='\t');
        end

        fopt(x,p)      = neg_logposterior_trans(x)
        grad_fopt(x,p) = if AutoDiff == "ForwardDiff"
                             ForwardDiff.gradient(neg_logposterior_trans,x)
                         else
                            Zygote.gradient(neg_logposterior_trans,x)[1]
                         end
        hess_fopt(x,p) = if AutoDiff == "ForwardDiff"
                             ForwardDiff.hessian(neg_logposterior_trans,x)
                         else
                             Zygote.hessian(neg_logposterior_trans,x)
                         end

        #=
        println("value")
        println(fopt(par_trans,0.0))
        println("gradient")
        println(grad_fopt(par_trans,0.0))
        println("hessian")
        println(hess_fopt(par_trans,0.0))
        =#

        #local prob = OptimizationProblem(OptimizationFunction(fopt,GalacticOptim.AutoForwardDiff()),
        #                                 par_trans)
        prob = GalacticOptim.OptimizationProblem(
                   GalacticOptim.OptimizationFunction(fopt,grad=grad_fopt,hess=hess_fopt),
                   par_trans)
        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: Starting optimization with GalacticOptim (autodiff ",AutoDiff,")"))
        flush(stdout)
        res = GalacticOptim.solve(prob,
                                  Flux.Optimise.ADADelta(0.9),
                                  reltol   = 1.0e-4,
                                  abstol   = 1.0e-4,
                                  maxiters = n_samples)
        println(string(Dates.format(Dates.now(),"yyyy-mm-dd HH:MM:SS"),
                       " Hyperflex_InferParameters: optimization with GalacticOptim completed (autodiff ",AutoDiff,")"))
        flush(stdout)

        println(typeof(res))
        println(res)

        samples = transpose(TransformBackward(Trans,res))

        if ! ismissing(logfile)
            par_opt = TransformBackward(Trans,Optim.minimizer(res))
            lp = logposterior(par_opt)
            step = copy(par_opt); push!(step,lp)
            steps = vcat(steps,transpose(step))
            CSV.write(logfile,Tables.table(steps;header=colnames),delim='\t');
        end

    else

        error(string("Hyperflex_InferParameters: unknown inference method: ",method))

    end

    return MCMCChains.Chains(samples,parnames)

end

#=

function Hyperflex_Get_Discharge_Pmult(simulator,times,Pmult;par=[],parnames=[])

    function Discharge_Pmult(Pmult::Real)

        res = simulator(times,par=vcat(par,Pmult),parnames=vcat(parnames,"Pmult"))
        if ismissing(res); return missing; end
        return res.results[:,end]

    end

    return Discharge_Pmult

end


function Hyperflex_Get_Discharge_Tshift(simulator,times,Tshift;par=[],parnames=[])

    function Discharge_Tshift(Tshift::Real)

        res = simulator(times,par=vcat(par,Tshift),parnames=vcat(parnames,"Tshift"))
        if ismissing(res); return missing; end
        return res.results[:,end]

    end

    return Discharge_Tshift

end


function Hyperflex_CalcSens(simulator,times;par=[],parnames=[])

    CalcDischarge_Pmult = Hyperflex_Get_Discharge_Pmult(simulator,times,1.0,
                                                       par=par,parnames=parnames)
    CalcDischarge_Tshift = Hyperflex_Get_Discharge_Tshift(simulator,times,0.0,
                                                         par=par,parnames=parnames)

    # if ismissing(CalcDischarge_Pmult(1.0)); return missing; end # check whether simulation works

    sens = hcat(times,
                ForwardDiff.derivative(CalcDischarge_Pmult,1.0),
                ForwardDiff.derivative(CalcDischarge_Tshift,0.0))

    return (sensitivities=sens, colnames=["t","Sens_P","Sens_T"])

end

=#

function Hyperflex_NSE(Qobs::Vector{Float64},Qsim::Vector{Float64})

    if length(Qobs) != length(Qsim); return NaN; end

    ind = findall(isfinite.(Qobs) .& isfinite.(Qsim))
    if length(ind) == 0; return NaN; end

    meanQobs = Statistics.mean(Qobs[ind])
    
    return 1 - sum( (Qsim[ind] .- Qobs[ind]).^2 ) / sum( (Qobs[ind] .- meanQobs).^2 )

end

function Hyperflex_NSE(Qobs::Matrix{Float64},Qsim::Matrix{Float64})

    Qsim_IntPolFun = IntPolFun(Qsim[:,1],Qsim[:,2];extrapolation="none")

    Qsim_IntPol = Qsim_IntPolFun.(Qobs[:,1])

    return Hyperflex_NSE(Qobs[:,2],Qsim_IntPol)

end

function Epot_Hamon(T,Lday)  # T temp degC, Lday daylight length in days

    # E according to Hamon following the equations (56) and (57) in the SI 
    # of Jiang et al. 2020, https://doi.org/10.1029/2020GL088229 :
    VP   = 0.611 .* exp.(17.3 .* T ./ (T .+ 237.3))       # kPa
    Epot = 29.8 .* Lday .* 24.0 .* VP ./ (T .+ 273.2)     # mm/day

    return(Epot)

end


# simulation function:
# --------------------

function HyperflexSimulate(simulator,times,par,parnames;
                           Qobs_cal=missing,Qobs_val=missing,sens=false,
                           dir=".",filename=missing)

    sigdigits = 6
    println(string("Simulating ",filename))
    println(string("  ",join(string(parnames[j],"=",round(par[j],sigdigits=sigdigits)," ") for j in 1:length(par))))

    res = simulator(times,par,parnames)
    if ismissing(res)
        println("    *** CamelsSimulate: unable to complete simulation")
        return missing
    end
    CSV.write(string(dir,"/",filename,".dat"),
              Tables.table(res.results,header=res.colnames),
              delim='\t')

    NSE_cal = missing
    NSE_val = missing
    if ! ismissing(Qobs_cal)
        NSE_cal = Hyperflex_NSE(Qobs_cal,hcat(res.results[:,1],res.results[:,end]))
    end
    if ! ismissing(Qobs_val)
        NSE_val = Hyperflex_NSE(Qobs_val,hcat(res.results[:,1],res.results[:,end]))
    end
    if (! ismissing(Qobs_cal)) | (! ismissing(Qobs_val))
        CSV.write(string(dir_output,"/",filename,"_NSE.dat"),
                  Tables.table(transpose(vcat(par,[NSE_cal,NSE_val]));
                               header=vcat(parnames,["NSE_cal","NSE_val"]));
                  delim='\t')
    end
    println(string("    NSE_cal = ",round(NSE_cal,sigdigits=3),", NSE_val = ",round(NSE_val,sigdigits=3)))

    if sens

        par_P = copy(par)
        parnames_P = copy(parnames)
        ind = findfirst(isequal("Pmult"),parnames)
        if isnothing(ind)
            par_P = vcat(par_P,[1.0]) 
            parnames_P = vcat(parnames_P,["Pmult"])
            ind = length(par_P)
        end
        par_P[ind] = 1.1
        res_P = simulator(times,par_P,parnames_P)
        if ismissing(res_P)
            println("    *** CamelsSimulate: unable to complete simulation with increased precipitation")
        else
            CSV.write(string(dir,"/",filename,"_Pmult.dat"),
                      Tables.table(hcat(res_P.results[:,1],
                                        round.(res_P.results[:,2:end],sigdigits=sigdigits));
                                   header=vcat(res_P.colnames[1],"DateTime",res_P.colnames[2:end]));
                      delim='\t')
      end
 
        par_T = copy(par)
        parnames_T = copy(parnames)
        ind = findfirst(isequal("Tshift"),parnames)
        if isnothing(ind)
            par_T = vcat(par_T,[0.0]) 
            parnames_T = vcat(parnames_T,["Tshift"])
            ind = length(par_T)
        end
        par_T[ind] = 1.0
        res_T = simulator(times,par_T,parnames_T)
        if ismissing(res_T)
            println("    *** CamelsSimulate: unable to complete simulation with increased temperature")
        else
            CSV.write(string(dir,"/",filename,"_Tshift.dat"),
                      Tables.table(hcat(res_T.results[:,1],
                                        round.(res_T.results[:,2:end],sigdigits=sigdigits));
                                   header=vcat(res_T.colnames[1],"DateTime",res_T.colnames[2:end]));
                      delim='\t')
        end
        
    end

    return merge(res,(NSE_cal=NSE_cal,NSE_val=NSE_val))

end

