## =======================================================
##
## File: Auxiliary.jl
##
## Auxiliary functions
##
## creation:          February 03, 2022 -- Peter Reichert
## last modification: February 03, 2022 -- Peter Reichert
## 
## contact:           peter.reichert@eawag.ch
##
## =======================================================


function SmoothHeaviside(x;dx=1.0,type="logistic")

    if type == "logistic"

        return 1.0 / ( 1.0 + exp(-4.0*x/dx) )

    elseif type == "quadratic"

        if x <= -dx
            return 0
        elseif x >= dx
            return 1.0
        elseif x < 0.0
            return 0.5*((x+dx)/dx)^2
        else
            return 1.0 - 0.5*((dx-x)/dx)^2
        end

    elseif type == "nonsmooth"

        if x == 0.0 
            return 0.5
        elseif x < 0.0
            return 0.0
        else
            return 1.0
        end

    else

        error("*** SmoothHeaviside: unknown type: ",type)

    end

end

function SmoothIncrease(x;dx=1.0,type="quadratic")

    if type == "quadratic"

        if x <= -dx
            return 0.0
        elseif x >= dx
            return x
        else
            return (x+dx)^2/(4*dx)
        end

    else

        error("*** SmoothIncrease: unknown type: ",type)

    end

end


#=

import ForwardDiff
import Plots

x  = collect(-5-0:0.1:5.0)

y1 = SmoothHeaviside.(x;type="quadratic")
y2 = SmoothHeaviside.(x;type="logistic")
y3 = SmoothHeaviside.(x;type="nonsmooth")

SmoothHeaviside1(x) = SmoothHeaviside(x;type="quadratic")
SmoothHeaviside2(x) = SmoothHeaviside(x;type="logistic")

dy1dx = ForwardDiff.derivative.(SmoothHeaviside1,x)
dy2dx = ForwardDiff.derivative.(SmoothHeaviside2,x)

y4 = SmoothIncrease.(x;type="quadratic")

dy4dx = ForwardDiff.derivative.(SmoothIncrease,x)

Plots.plot(x,y1,linewidth=1,linecolor="red",label="quadratic",xlabel="x",ylabel="f_HS")
Plots.plot!(x,y2,linewidth=1,linecolor="blue",label="logistic")
Plots.plot!(x,y3,linewidth=1,linecolor="green",label="nonsmooth")

Plots.plot(x,dy1dx,linewidth=1,linecolor="red",label="quadratic",xlabel="x",ylabel="df_HS/dx")
Plots.plot!(x,dy2dx,linewidth=1,linecolor="blue",label="logistic")

Plots.plot(x,y4,linewidth=1,linecolor="red",label="quadratic",xlabel="x",ylabel="f_HS")

Plots.plot(x,dy4dx,linewidth=1,linecolor="red",label="quadratic",xlabel="x",ylabel="df_HS/dx")

=#