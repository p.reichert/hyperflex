## -------------------------------------------------------
##
## File: Camels.jl
##
## Functions to read and compile camels data
##
## Based on data of the CAMELS data set USA:
## https://ral.ucar.edu/solutions/products/camels
##
## A. J. Newman, M. P. Clark, K. Sampson, A. Wood, 
## L. E. Hay, A. Bock, R. J. Viger, D. Blodgett, L. Brekke, 
## J. R. Arnold, T. Hopson, and Q. Duan, 2015
## Development of a large-sample watershed-scale 
## hydrometeorological dataset for the contiguous USA: 
## dataset characteristics and assessment of regional 
## variability in hydrologic model performance. 
## Hydrol. Earth Syst. Sci., 19, 209-223, 2015
## https://doi.org/10.5194/hess-19-209-2015
##
## Addor, N., Newman, A. J., Mizukami, N. and Clark, M. P.
## The CAMELS data set: catchment attributes and 
## meteorology for large-sample studies, 
## Hydrol. Earth Syst. Sci., 21, 5293–5313, 2017
## https://doi.org/10.5194/hess-21-5293-2017.
##
## creation:          January  01, 2021 -- Peter Reichert
## last modification: January  31, 2022 -- Peter Reichert
## 
## contact:           peter.reichert@eawag.ch
##
## -------------------------------------------------------


using  DataFrames
import CSV
import DelimitedFiles
import Dates
import Statistics

import Interpolations

# define data structures:
# -----------------------

struct CamelsForcingElevBand
    n_bands::Int
    elev_lower::Array{Real,1}
    elev_upper::Array{Real,1}
    area::Array{Real,1}
    area_fractions::Array{Real,1}
    datetime::DataFrame
    forcing::Array{DataFrame,1}
end

struct CamelsData
    id::String
    huc::String
    latitude::Real
    longitude::Real
    area::Real
    forcing_mean::DataFrame
    forcing_elevband_original::Union{CamelsForcingElevBand,Missing}
    forcing_elevband_aggregated::Union{CamelsForcingElevBand,Missing}
    attributes::DataFrameRow
    streamflow::DataFrame
    streamflow_compare::Union{Missing,DataFrame}
    streamflow_compare_seed::Union{Missing,String}
    missing_input::String
end

struct CamelsPlotFrame
    title1::String
    xlim1::Union{Missing,Array{Real,1}}
    ylim1::Union{Missing,Array{Real,1}}
    title2::String
    xlim2::Union{Missing,Array{Real,1}}
    ylim2::Union{Missing,Array{Real,1}}
end

# global offset date:
# -------------------

CamelsOffsetDate = Dates.DateTime(1980,1,1,0)


# function to get aggregation of elevation bands:
# -----------------------------------------------

function GetAggregation(areas,n_max)

    n = length(areas)
    aggregation = Array{Array{Int,1},1}(undef,max(n,n_max))
    for i in 1:n
        aggregation[i] = [i]
    end

    areas_mod = copy(areas)
    if n > n_max
        while n > n_max
           ind_min = findmin(areas_mod)[2]
           if ( ind_min > 1 ) & ( ind_min < n )
               if areas_mod[ind_min-1] < areas_mod[ind_min+1]
                   ind_merge = ind_min - 1
               else
                   ind_merge = ind_min + 1
               end
            elseif ind_min == 1
                ind_merge = 2
            else
                ind_merge = n - 1
            end
            append!(aggregation[ind_merge],aggregation[ind_min])
            deleteat!(aggregation,ind_min)
            areas_mod[ind_merge] = areas_mod[ind_merge] + areas_mod[ind_min]
            deleteat!(areas_mod,ind_min)
            n = length(aggregation)
        end
        for i in 1:n 
            aggregation[i] = sort(aggregation[i])
        end
    end

    return aggregation
end


# read data of selected basins:
# -----------------------------

function CamelsRead(basins,
                    dir_camels_timeseries,
                    dir_camels_attributes;
                    dir_camels_modres = missing,
                    n_bands_max       = 5)

    # read metadata of all gauges:

    gauge_meta = CSV.read(string(dir_camels_timeseries,"/basin_metadata/gauge_information.txt"), 
                          DataFrame;
                          delim='\t', skipto=2, header = false)
    rename!(gauge_meta,Symbol.(["HUC_02","GAGE_ID","GAGE_NAME","LAT","LONG","DRAINAGE AREA (KM^2)"]))
    gauge_meta[!,:GAGE_ID] = lpad.(gauge_meta[!,:GAGE_ID],8,"0")

    # read basin attributes:

    filenames = ["camels_name.txt","camels_topo.txt","camels_geol.txt","camels_soil.txt",
                 "camels_vege.txt","camels_clim.txt","camels_hydro.txt"]
    attributes = DataFrame()
    for i in 1:length(filenames)
        data = CSV.read(string(dir_camels_attributes,"/",filenames[i]),DataFrame;
                        delim=';', header = true)
        data[!,1] = lpad.(data[!,1],8,"0")
        if i == 1
            attributes = vcat(attributes,data)
        else
            sumerr = sum(attributes[!,1] .!= data[!,1])
            if sumerr > 0; error("CamelsRead: basin identifiers on attribute files not equal") end
            attributes = hcat(attributes,data[!,2:end])
        end
    end
    attributes[!,"runoff_ratio"] = tryparse.(Float64,attributes[!,"runoff_ratio"]) # was read as string

    # loop over selected basins:

    camelsdata = Array{CamelsData,1}(undef,length(basins))
    messages = String[]
    basins_missing_input = []
    for i in 1:length(basins)

        # collect information about missing input:

        missing_input = ""

        # identifier and hydrological unit code:

        id             = basins[i]
        row_meta_basin = findfirst(isequal(id),gauge_meta[!,:GAGE_ID])
        huc            = lpad(gauge_meta[row_meta_basin,:HUC_02],2,"0")
        latitude       = gauge_meta[row_meta_basin,:LAT]
        longitude      = gauge_meta[row_meta_basin,:LONG]

        # read mean forcing:

        forcing_mean_all = DelimitedFiles.readdlm(string(dir_camels_timeseries,"/basin_mean_forcing/daymet/",
                                                         huc,"/",id,"_lump_cida_forcing_leap.txt"))
        area = forcing_mean_all[3,1]                                  # area is in row 3; m^2
        forcing_mean = DataFrame(forcing_mean_all[5:end,:],:auto)     # change from row 5 to DataFrame
        rename!(forcing_mean,Symbol.(forcing_mean_all[4,:]))          # add header from row 4
        rename!(forcing_mean,:Mnth => :Month)                         # replace Mnth by Month
        forcing_mean[!,"dayl(s)"] = forcing_mean[!,"dayl(s)"]/3600/24 # change from s to day
        rename!(forcing_mean,"dayl(s)" => "daylight(day)")            # adapt unit from s to day
        forcing_mean[!,"daylight(day)"] = convert(Array{Float64,1},forcing_mean[!,"daylight(day)"])
        if ! all(isfinite.(forcing_mean[!,"daylight(day)"]))
            if length(missing_input) > 0; missing_input = string(missing_input," | "); end
            missing_input = string(missing_input,"some mean forcing daylight data are missing")
        end
        forcing_mean[!,"prcp(mm/day)"] = convert(Array{Float64,1},forcing_mean[!,"prcp(mm/day)"])
        if ! all(isfinite.(forcing_mean[!,"prcp(mm/day)"]))
            if length(missing_input) > 0; missing_input = string(missing_input," | "); end
            missing_input = string(missing_input,"some mean forcing prcp data are missing")
        end
        forcing_mean[!,"tmin(C)"] = convert(Array{Float64,1},forcing_mean[!,"tmin(C)"])
        if ! all(isfinite.(forcing_mean[!,"tmin(C)"]))
            if length(missing_input) > 0; missing_input = string(missing_input," | "); end
            missing_input = string(missing_input,"some mean forcing tmin data are missing")
        end
        forcing_mean[!,"tmax(C)"] = convert(Array{Float64,1},forcing_mean[!,"tmax(C)"])
        if ! all(isfinite.(forcing_mean[!,"tmax(C)"]))
            if length(missing_input) > 0; missing_input = string(missing_input," | "); end
            missing_input = string(missing_input,"some mean forcing tmax data are missing")
        end

        dates = Dates.DateTime.(forcing_mean[!,"Year"],forcing_mean[!,"Month"],
                                forcing_mean[!,"Day"],forcing_mean[!,"Hr"])
        time  = Dates.toms.(dates .- CamelsOffsetDate) / (1000.0*86400.0)
        insertcols!(forcing_mean,1,:Date => dates)
        insertcols!(forcing_mean,1,:Time => time)

        # read elevation band forcing:

        forcing_elevband_original = missing
        forcing_elevband_aggregated = missing
        if n_bands_max > 1 
            band_info = DelimitedFiles.readdlm(string(dir_camels_timeseries,"/elev_bands_forcing/daymet/",
                                                      huc,"/",id,".list"))
            n_bands = band_info[1,1]
            band_files = band_info[2:(n_bands+1),1]
            band_areas = band_info[2:(n_bands+1),2]
            perm       = sortperm(band_files)   # get ordering along altitude
            band_files = band_files[perm]       # reorder file names
            band_areas = band_areas[perm]       # reorder areas accordingly
            forcing    = Array{Any,1}(undef,n_bands)
            elev_lower = Array{Real,1}(undef,n_bands)
            elev_upper = Array{Real,1}(undef,n_bands)
            datetime   = Missing
            for j in 1:n_bands
                forcing_all = DelimitedFiles.readdlm(string(dir_camels_timeseries,"/elev_bands_forcing/daymet/",
                                                            huc,"/",band_files[j]))
                forcing[j] = DataFrame(forcing_all[5:end,5:end],:auto)
                rename!(forcing[j],Symbol.(forcing_all[4,1:7]))             # add header from row 4
                forcing[j][!,"dayl(s)"] = forcing[j][!,"dayl(s)"]/3600/24   # change from s to day
                rename!(forcing[j],"dayl(s)" => "daylight(day)")            # adapt unit from s to day
                forcing[j][!,"daylight(day)"] = convert(Array{Float64,1},forcing[j][!,"daylight(day)"])
                # we currently do not use elevation band daylight length:
                # if ! all(isfinite.(forcing[j][!,"daylight(day)"]))
                #     if length(missing_input) > 0; missing_input = string(missing_input," | "); end
                #     missing_input = string(missing_input,"some elevation band ",j," forcing daylight data are missing")
                # end
                forcing[j][!,"prcp(mm/day)"]  = convert(Array{Float64,1},forcing[j][!,"prcp(mm/day)"])
                if ! all(isfinite.(forcing[j][!,"prcp(mm/day)"]))
                    if length(missing_input) > 0; missing_input = string(missing_input," | "); end
                    missing_input = string(missing_input,"some elevation band ",j," forcing prcp data are missing")
                end
                forcing[j][!,"tmin(C)"]       = convert(Array{Float64,1},forcing[j][!,"tmin(C)"])
                if ! all(isfinite.(forcing[j][!,"tmin(C)"]))
                    if length(missing_input) > 0; missing_input = string(missing_input," | "); end
                    missing_input = string(missing_input,"some elevation band ",j," forcing tmin data are missing")
                end
                forcing[j][!,"tmax(C)"]       = convert(Array{Float64,1},forcing[j][!,"tmax(C)"])
                if ! all(isfinite.(forcing[j][!,"tmax(C)"]))
                    if length(missing_input) > 0; missing_input = string(missing_input," | "); end
                    missing_input = string(missing_input,"some elevation band ",j," forcing tmax data are missing")
                end
                if j == 1
                    datetime = DataFrame(forcing_all[5:end,1:4],:auto)
                    rename!(datetime,Symbol.(["Year","Month","Day","Hr"]))  # add header
                    datetime[!,"Hr"] .= 12
                    dates = Dates.DateTime.(datetime[!,"Year"],datetime[!,"Month"],
                                            datetime[!,"Day"],datetime[!,"Hr"])
                    time  = Dates.toms.(dates .- CamelsOffsetDate) / (1000.0*86400.0)
                    insertcols!(datetime,1,:Date => dates)
                    insertcols!(datetime,1,:Time => time)
                else
                    # check if values are identical to datetime
                end
                elev_lower[j] = forcing_all[2,1]
                elev_upper[j] = elev_lower[j] + 100.0
            end
            forcing_elevband_original = CamelsForcingElevBand(n_bands,elev_lower,elev_upper,band_areas,
                                                              band_areas./sum(band_areas),datetime,forcing)

            if n_bands > n_bands_max
                aggregation_inds = GetAggregation(band_areas,n_bands_max)

                n_bands = n_bands_max
                elev_lo = Array{Real,1}(undef,n_bands_max)
                elev_up = Array{Real,1}(undef,n_bands_max)
                band_ar = Array{Real,1}(undef,n_bands_max)
                forc    = Array{Any,1}(undef,n_bands_max)
                for j in 1:n_bands_max
                    elev_lo[j] = minimum(elev_lower[aggregation_inds[j]])  # minimum of aggregated levels
                    elev_up[j] = maximum(elev_upper[aggregation_inds[j]])  # maximum of aggregated levels
                    band_ar[j] = sum(band_areas[aggregation_inds[j]])      # sum of aggregated levels
                    forc[j]    = deepcopy(forcing[aggregation_inds[j][1]])
                    for k in 1:ncol(forc[j])  # area-weighted mean of aggregated levels
                        forc[j][!,k] .= 0
                        for l in aggregation_inds[j]
                            forc[j][!,k] .= forc[j][!,k] .+ band_areas[l].*forcing[l][!,k]
                        end
                        forc[j][!,k] .= forc[j][!,k] ./ sum(band_areas[aggregation_inds[j]])
                    end
                end
                forcing_elevband_aggregated = CamelsForcingElevBand(n_bands_max,elev_lo,elev_up,band_ar,
                                                                    band_ar./sum(band_ar),datetime,forc)
            else
                forcing_elevband_aggregated = forcing_elevband_original
            end
        end

        # read streamflow:

        streamflow = DataFrame(DelimitedFiles.readdlm(string(dir_camels_timeseries,"/usgs_streamflow/",
                                                             huc,"/",id,"_streamflow_qc.txt"))[:,2:end],
                               :auto)
        rename!(streamflow,["Year","Month","Day","Q","flag"])                # add header
        streamflow[findall(x -> x<-900,streamflow[!,"Q"]),"Q"] .= NaN        # replace -999 by NaN
        streamflow[!,"Q"] = streamflow[!,"Q"]*(0.3048^3)/area*1000.0*86400.0 # conversion feet^3 to mm/day
        rename!(streamflow,"Q" => "Q(mm/day)")                               # add unit to column name

        dates = Dates.DateTime.(streamflow[!,"Year"],streamflow[!,"Month"],
                                streamflow[!,"Day"],12)
        time  = Dates.toms.(dates .- CamelsOffsetDate) / (1000.0*86400.0)
        insertcols!(streamflow,1,:Date => dates)
        insertcols!(streamflow,1,:Time => time)

        # read SAC-SMA model results

        streamflow_compare = missing
        streamflow_compare_seed = missing
        if ! ismissing(dir_camels_modres)
            dir = string(dir_camels_modres,"/model_output_daymet/model_output/flow_timeseries/daymet/",huc)
            if isdir(dir)
                filenames = readdir(dir)
                if length(filenames) > 0
                    filenames_splitted = split.(filenames,"_")
                    ids = Array{String,1}(undef,length(filenames))
                    for j in 1:length(filenames); ids[j] = filenames_splitted[j][1]; end
                    inds = findall(isequal(id),ids)
                    if length(inds) > 0    # multiple (10) solutions for different seeds; choose the one with maximum NSE
                        seed_sel = filenames_splitted[inds[1]][2]
                        nse_sel  = Float64(-Inf)
                        for ind in inds
                            seed = filenames_splitted[ind][2]
                            streamflow_compare_raw = DelimitedFiles.readdlm(string(dir,"/",id,"_",seed,"_model_output.txt"))
                            streamflow_compare = DataFrame(streamflow_compare_raw[2:end,:],:auto)
                            rename!(streamflow_compare,String.(streamflow_compare_raw[1,:]))
                            streamflow_compare[!,"MOD_RUN"] = convert(Array{Float64,1},streamflow_compare[!,"MOD_RUN"])
                            streamflow_compare[!,"OBS_RUN"] = convert(Array{Float64,1},streamflow_compare[!,"OBS_RUN"])
                            nse = Camels_NSE(streamflow_compare[!,"OBS_RUN"],streamflow_compare[!,"MOD_RUN"])
                            if nse > nse_sel; nse_sel = nse; seed_sel = seed; end
                        end
                        streamflow_compare_seed = seed_sel
                        streamflow_compare_raw = DelimitedFiles.readdlm(string(dir,"/",id,"_",seed_sel,"_model_output.txt"))
                        streamflow_compare = DataFrame(streamflow_compare_raw[2:end,:],:auto)
                        rename!(streamflow_compare,String.(streamflow_compare_raw[1,:]))
                        rename!(streamflow_compare,:YR => :Year)                       # replace YR by year
                        rename!(streamflow_compare,:MNTH => :Month)                    # replace Mnth by Month
                        rename!(streamflow_compare,:DY => :Day)                        # replace DY by Day
                        rename!(streamflow_compare,:HR => :Hr)                         # replace HR by Hr

                        dates = Dates.DateTime.(streamflow_compare[!,"Year"],streamflow_compare[!,"Month"],
                                                streamflow_compare[!,"Day"],streamflow_compare[!,"Hr"])
                        time  = Dates.toms.(dates .- CamelsOffsetDate) / (1000.0*86400.0)
                        insertcols!(streamflow_compare,1,:Date => dates)
                        insertcols!(streamflow_compare,1,:Time => time)
                        streamflow_compare[!,"MOD_RUN"] = convert(Array{Float64,1},streamflow_compare[!,"MOD_RUN"])
                        streamflow_compare[!,"OBS_RUN"] = convert(Array{Float64,1},streamflow_compare[!,"OBS_RUN"])

                    end
                end
            end
        end
        if ismissing(streamflow_compare)
            vcat(messages,string("            *** results from SAC-SMA model not found for basin ",id))
        else
            # check for differences in streamflow between converted USGS data and data provided with model output:
            ratio_min      = 1.0
            ratio_max      = 1.0
            n_missing_date = 0
            n_missing_usgs = 0
            n_missing_sac  = 0
            for j in 1:nrow(streamflow_compare)
                avail = true
                ind = findfirst(isequal(streamflow_compare[j,"Date"]),streamflow[!,"Date"])
                if isnothing(ind)
                    n_missing_date = n_missing_date + 1
                    avail = false
                else
                    if ! isfinite(streamflow[ind,"Q(mm/day)"])
                        n_missing_usgs = n_missing_usgs + 1
                        avail = false
                    end
                end
                if ! isfinite(streamflow_compare[j,"OBS_RUN"])
                    n_missing_sac = n_missing_sac + 1
                    avail = false
                end
                if avail
                    ratio = streamflow_compare[j,"OBS_RUN"]/streamflow[ind,"Q(mm/day)"]
                    ratio_max = max(ratio,ratio_max)
                    ratio_min = min(ratio,ratio_min)
                end
            end
            if (ratio_min < 0.99) | (ratio_max > 1.01)
                vcat(messages,string("            *** basin ",id,": unequal USGS and SAC-SMA discharges (ratios in [",
                                     ratio_min,",",ratio_max,"])"))
            end
            if n_missing_date > 0
                vcat(messages,string("            *** basin ",id,": ",n_missing_date," SAC-SMA dates not found in USGS discharge data"))
            end
            if n_missing_usgs > 0
                vcat(messages,string("            *** basin ",id,": ",n_missing_usgs," USGS discharges not available"))
            end
            if n_missing_sac > 0
                vcat(messages,string("            *** basin ",id,": ",n_missing_sac," SAC-SMA discharges not available"))
            end
        end
    
        # collect information in structure:

        camelsdata[i] = CamelsData(id,huc,latitude,longitude,area,forcing_mean,
                                   forcing_elevband_original,forcing_elevband_aggregated,
                                   attributes[findfirst(isequal(basins[i]),attributes[!,1]),:],
                                   streamflow,streamflow_compare,streamflow_compare_seed,
                                   missing_input)

        if length(missing_input) > 0; basins_missing_input = vcat(basins_missing_input,id); end
    end

    println("CamelsRead: time series and catchment attributes from ",length(basins),
            " CAMELS basin(s) read")

    if length(basins_missing_input) > 0
        println("            *** ",length(basins_missing_input)," basins with incomplete input data: ",join(basins_missing_input,", "))
    end

    if length(messages) > 0
        for i in 1:length(messages)
            println(messages[i])
        end
    end
    flush(stdout)

    return camelsdata
end


# simulation function:
# --------------------

function CamelsSimulate(simulator,times,par,parnames;
                        Qobs_cal=missing,Qobs_val=missing,sens=false,
                        dir=".",filename=missing)

    function CamelsGetDate(t)
        return CamelsOffsetDate + Dates.Millisecond(convert(Int,round(t*1000.0*86400.0)))
    end
    datetimes = CamelsGetDate.(times)

    sigdigits = 6
    println(string("Simulating ",filename))
    println(string("  ",join(string(parnames[j],"=",round(par[j],sigdigits=sigdigits)," ") for j in 1:length(par))))

    res = simulator(times,par,parnames)
    if ismissing(res)
        println("    *** CamelsSimulate: unable to complete simulation")
        return missing
    end
    CSV.write(string(dir,"/",filename,".dat"),
              Tables.table(hcat(res.results[:,1],
                                datetimes,
                                round.(res.results[:,2:end],sigdigits=sigdigits));
                           header=vcat(res.colnames[1],"DateTime",res.colnames[2:end]));
              delim='\t')

    NSE_cal = missing
    NSE_val = missing
    if ! ismissing(Qobs_cal)
        NSE_cal = Hyperflex_NSE(Qobs_cal,hcat(res.results[:,1],res.results[:,end]))
    end
    if ! ismissing(Qobs_val)
        NSE_val = Hyperflex_NSE(Qobs_val,hcat(res.results[:,1],res.results[:,end]))
    end
    if (! ismissing(Qobs_cal)) | (! ismissing(Qobs_val))
        CSV.write(string(dir_output,"/",filename,"_NSE.dat"),
                  Tables.table(transpose(vcat(par,[NSE_cal,NSE_val]));
                               header=vcat(parnames,["NSE_cal","NSE_val"]));
                  delim='\t')
    end
    println(string("    NSE_cal = ",round(NSE_cal,sigdigits=3),", NSE_val = ",round(NSE_val,sigdigits=3)))

    if sens

        par_P = copy(par)
        parnames_P = copy(parnames)
        ind = findfirst(isequal("Pmult"),parnames)
        if isnothing(ind)
            par_P = vcat(par_P,[1.0]) 
            parnames_P = vcat(parnames_P,["Pmult"])
            ind = length(par_P)
        end
        par_P[ind] = 1.1
        res_P = simulator(times,par_P,parnames_P)
        if ismissing(res_P)
            println("    *** CamelsSimulate: unable to complete simulation with increased precipitation")
        else
            CSV.write(string(dir,"/",filename,"_Pmult.dat"),
                      Tables.table(hcat(res_P.results[:,1],
                                        datetimes,
                                        round.(res_P.results[:,2:end],sigdigits=sigdigits));
                                   header=vcat(res_P.colnames[1],"DateTime",res_P.colnames[2:end]));
                      delim='\t')
      end
 
        par_T = copy(par)
        parnames_T = copy(parnames)
        ind = findfirst(isequal("Tshift"),parnames)
        if isnothing(ind)
            par_T = vcat(par_T,[0.0]) 
            parnames_T = vcat(parnames_T,["Tshift"])
            ind = length(par_T)
        end
        par_T[ind] = 1.0
        res_T = simulator(times,par_T,parnames_T)
        if ismissing(res_T)
            println("    *** CamelsSimulate: unable to complete simulation with increased temperature")
        else
            CSV.write(string(dir,"/",filename,"_Tshift.dat"),
                      Tables.table(hcat(res_T.results[:,1],
                                        datetimes,
                                        round.(res_T.results[:,2:end],sigdigits=sigdigits));
                                   header=vcat(res_T.colnames[1],"DateTime",res_T.colnames[2:end]));
                      delim='\t')
        end
        
    end

    return merge(res,(NSE_cal=NSE_cal,NSE_val=NSE_val))

end


# plot results:
# -------------

function CamelsPlot(simulator,timepoints,dt_sim,par,parnames,dir,filename,frame;
                    Q_obs        = missing,
                    Q_compare    = missing,
                    annotations1 = missing,
                    annotations2 = missing,
                    min_sens     = -5.0,
                    max_sens     = 5.0)

    function CamelsGetDate(t)
        return CamelsOffsetDate + Dates.Millisecond(convert(Int,round(t*1000.0*86400.0)))
    end

    # do simulation and sensitivity analysis and write results to file:
    # -----------------------------------------------------------------

    # define time points:
    times_sim = collect(timepoints.start_sim:dt_sim:timepoints.end_val)
    # datetimes_sim = CamelsOffsetDate.+Dates.Millisecond.(convert.(Int,round.(times_sim.*1000.0*86400.0)))
    datetimes_sim = CamelsGetDate.(times_sim)

    # calculate results:
    res_sim   = simulator(times_sim,par=par,parnames=parnames)

    if ismissing(res_sim)

        println("CamelsPlot: unable to perform simulation")
        return

    end

    # calculate sensitivities:
    res_sens  = Hyperflex_CalcSens(simulator,times_sim;par=par,parnames=parnames)

    # calculate results for 10% increase in precipitation and for 1deg increase in temperature:
    dp = 0.001
    dT = 0.001
    res_sim_Pmult_micinc  = simulator(times_sim,par=vcat(par,1.0+dp),parnames=vcat(parnames,"Pmult"))
    res_sim_Tshift_micinc = simulator(times_sim,par=vcat(par,0.0+dT),parnames=vcat(parnames,"Tshift"))
    res_sim_Pmult_macinc  = simulator(times_sim,par=vcat(par,1.1),parnames=vcat(parnames,"Pmult"))
    res_sim_Tshift_macinc = simulator(times_sim,par=vcat(par,1.0),parnames=vcat(parnames,"Tshift"))

    if ismissing(res_sens) | ismissing(res_sim_Pmult_micinc) | ismissing(res_sim_Tshift_micinc) | ismissing(res_sim_Pmult_macinc) | ismissing(res_sim_Tshift_macinc)

        CSV.write(string(dir,"/",filename,"_sim.dat"),
                  Tables.table(hcat(round.(res_sim.results,sigdigits=6),
                                    datetimes_sim);
                               header=vcat(res_sim.colnames,
                                           "DateTime"));
                  delim='\t')

    else

        CSV.write(string(dir,"/",filename,"_sim.dat"),
                  Tables.table(hcat(round.(res_sim.results,sigdigits=6),
                                    round.(res_sens.sensitivities[:,2:end],sigdigits=6),
                                    round.((res_sim_Pmult_micinc.results[:,end].-res_sim.results[:,end])/dp,sigdigits=6),
                                    round.((res_sim_Tshift_micinc.results[:,end].-res_sim.results[:,end])/dT,sigdigits=6),
                                    round.(res_sim_Pmult_macinc.results[:,end].-res_sim.results[:,end],sigdigits=6),
                                    round.(res_sim_Tshift_macinc.results[:,end].-res_sim.results[:,end],sigdigits=6),
                                    datetimes_sim);
                               header=vcat(res_sim.colnames,
                                           res_sens.colnames[2:end],
                                           "Sens_P_FD",
                                           "Sens_T_FD",
                                           "DeltaQ_Precplus10perc",
                                           "DeltaQ_Tplus1deg",
                                           "DateTime"));
                  delim='\t')

    end
    CSV.write(string(dir,"/",filename,"_simP.dat"),
              Tables.table(hcat(round.(res_sim_Pmult_macinc.results,sigdigits=6),
                                datetimes_sim);
                           header=vcat(res_sim.colnames,
                                       "DateTime"));
    delim='\t')
    CSV.write(string(dir,"/",filename,"_simT.dat"),
              Tables.table(hcat(round.(res_sim_Tshift_macinc.results,sigdigits=6),
                                datetimes_sim);
                           header=vcat(res_sim.colnames,
                                      "DateTime"));
    delim='\t')

    # get and write results at observation time points and calculate NSE:
    # -------------------------------------------------------------------

    NSE_cal         = missing
    NSE_val         = missing
    NSE_cal_compare = missing
    NSE_val_compare = missing

    if ! ismissing(Q_obs)

        ind_obs_cal = findall((Q_obs[:,1] .>= timepoints.start_cal) .& (Q_obs[:,1] .< timepoints.start_val) .& isfinite.(Q_obs[:,2]))
        t_obs_cal = Q_obs[ind_obs_cal,1]
        Q_obs_cal = Q_obs[ind_obs_cal,2]
        datetimes_cal = CamelsGetDate.(t_obs_cal)
        ind_obs_val = findall((Q_obs[:,1] .>= timepoints.start_val) .& (Q_obs[:,1] .< timepoints.end_val) .& isfinite.(Q_obs[:,2]))
        t_obs_val = Q_obs[ind_obs_val,1]
        Q_obs_val = Q_obs[ind_obs_val,2]
        datetimes_val = CamelsGetDate.(t_obs_val)
    
        Q_sim_fun = Interpolations.LinearInterpolation(res_sim.results[:,1],res_sim.results[:,end],extrapolation_bc=Interpolations.Flat())
        Q_sim_cal = Q_sim_fun.(t_obs_cal)
        Q_sim_val = Q_sim_fun.(t_obs_val)

        NSE_cal   = Camels_NSE(Q_obs_cal,Q_sim_cal)
        NSE_val   = Camels_NSE(Q_obs_val,Q_sim_val)

        if ismissing(Q_compare)

            CSV.write(string(dir,"/",filename,"_sim_obs.dat"),
                      Tables.table(hcat(vcat(t_obs_cal,t_obs_val),
                                        round.(vcat(Q_sim_cal,Q_sim_val),sigdigits=6),
                                        vcat(Q_obs_cal,Q_obs_val),
                                        vcat(datetimes_cal,datetimes_val));
                                   header=vcat(res_sim.colnames[1],res_sim.colnames[end],"Q_obs","DateTime"));
                      delim='\t')

        else

            Q_compare_fun   = Interpolations.LinearInterpolation(Q_compare[:,1],Q_compare[:,2],extrapolation_bc=Interpolations.Flat())
            Q_compare_cal   = Q_compare_fun.(t_obs_cal)
            Q_compare_val   = Q_compare_fun.(t_obs_val)
    
            NSE_cal_compare = Camels_NSE(Q_obs_cal,Q_compare_cal)
            NSE_val_compare = Camels_NSE(Q_obs_val,Q_compare_val)

            CSV.write(string(dir,"/",filename,"_sim_obs.dat"),
                      Tables.table(hcat(vcat(t_obs_cal,t_obs_val),
                                        round.(vcat(Q_sim_cal,Q_sim_val),sigdigits=6),
                                        vcat(Q_obs_cal,Q_obs_val),
                                        vcat(Q_compare_cal,Q_compare_val),
                                        vcat(datetimes_cal,datetimes_val));
                                   header=vcat(res_sim.colnames[1],
                                               res_sim.colnames[end],
                                               "Q_obs",
                                               "Q_SACSMA",
                                               "DateTime"));
                      delim='\t')

        end

    end
    
    CSV.write(string(dir,"/",filename,"_NSE.dat"),
    Tables.table([NSE_cal NSE_val NSE_cal_compare NSE_val_compare],
                 header=["NSE_cal","NSE_val","NSE_cal_SACSMA","NSE_val_SACSMA"]);
    delim='\t')

    # plot discharge time series:
    # ---------------------------

    frames = frame; if typeof(frames)==CamelsPlotFrame; frames=[frames]; end

    for i in 1:length(frames)
    
        local l1 = Plots.@layout [a{0.47h}; b{0.06h}; c{0.47h}]
        Plots.plot(layout=l1)

        # calibration plot:
        Plots.plot!(subplot=1,[],[],
                    xlabel="time [d]",ylabel="Q [mm/d]",
                    title=frames[i].title1,titlefont=font(10),
                    xlims=frames[i].xlim1,
                    ylims=frames[i].ylim1,
                    grid=false)
        if ! ismissing(Q_obs)
            ind = findall(x->(x>=frames[i].xlim1[1])&(x<=frames[i].xlim1[2]),Q_obs[:,1])
            Plots.plot!(subplot=1,Q_obs[ind,1],Q_obs[ind,2],label="obs",
                        linewidth=0,linecolor="white",
                        markershape=:circle,markersize=1.0,markercolor="black")
        end
        if ! ismissing(Q_compare)
            ind = findall(x->(x>=frames[i].xlim1[1])&(x<=frames[i].xlim1[2]),Q_compare[:,1])
            Plots.plot!(subplot=1,Q_compare[ind,1],Q_compare[ind,2],label="SAC",linecolor="blue")
        end
        textNSE = string("NSE cal = ")
        if ! ismissing(NSE_cal)
            textNSE = string(textNSE,round(NSE_cal,digits=2))
        end
        if ! ismissing(NSE_cal_compare)
            textNSE = string(textNSE," (SAC: ",round(NSE_cal_compare,digits=2),")")
        end
        ind = findall(x->(x>=frames[i].xlim1[1])&(x<=frames[i].xlim1[2]),res_sim.results[:,1])
        Plots.plot!(subplot=1,res_sim.results[ind,1],res_sim.results[ind,end],label="sim",linecolor="red")
        Plots.annotate!(subplot=1,
                        frames[i].xlim1[2]-0.2*(frames[i].xlim1[2]-frames[i].xlim1[1]),
                        frames[i].ylim1[1]+0.9*(frames[i].ylim1[2]-frames[i].ylim1[1]),
                        text(textNSE,font(8)))
        if ( ! ismissing(annotations1) )
            for j in 1:length(annotations1)
                Plots.annotate!(subplot=1,
                                frames[i].xlim1[1]+0.1*(frames[i].xlim1[2]-frames[i].xlim1[1]),
                                frames[i].ylim1[1]+(0.95-(j-1)*0.06)*(frames[i].ylim1[2]-frames[i].ylim1[1]),
                                text(annotations1[j],font(6)))
            end
        end

        # empty space to avoid overlap of second title with first label:
        Plots.plot!(subplot=2,legend=false,grid=false,foreground_color_subplot=:white)

        # validation plot:
        Plots.plot!(subplot=3,[],[],
                    xlabel="time [d]",ylabel="Q [mm/d]",
                    title=frames[i].title2,titlefont=font(10),
                    xlims=frames[i].xlim2,ylims=frames[i].ylim2,
                    grid=false)
        if ! ismissing(Q_obs)
            ind = findall(x->(x>=frames[i].xlim2[1])&(x<=frames[i].xlim2[2]),Q_obs[:,1])
            Plots.plot!(subplot=3,Q_obs[ind,1],Q_obs[ind,2],label="",
                        linewidth=0,linecolor="white",
                        markershape=:circle,markersize=1.0,markercolor="black")
        end
        if ! ismissing(Q_compare)
            ind = findall(x->(x>=frames[i].xlim2[1])&(x<=frames[i].xlim2[2]),Q_compare[:,1])
            Plots.plot!(subplot=3,Q_compare[ind,1],Q_compare[ind,2],label="",linecolor="blue")
        end
        textNSE = string("NSE val = ")
        if ! ismissing(NSE_val)
            textNSE = string(textNSE,round(NSE_val,digits=2))
        end
        if ! ismissing(NSE_val_compare)
            textNSE = string(textNSE," (SAC: ",round(NSE_val_compare,digits=2),")")
        end
        ind = findall(x->(x>=frames[i].xlim2[1])&(x<=frames[i].xlim2[2]),res_sim.results[:,1])
        Plots.plot!(subplot=3,res_sim.results[ind,1],res_sim.results[ind,end],label="",linecolor="red")
        Plots.annotate!(subplot=3,
                        frames[i].xlim2[2]-0.2*(frames[i].xlim2[2]-frames[i].xlim2[1]),
                        frames[i].ylim2[1][1]+0.9*(frames[i].ylim2[2]-frames[i].ylim2[1]),
                        text(textNSE,font(8)))
        if ( ! ismissing(annotations2) )
            for j in 1:length(annotations2)
                Plots.annotate!(subplot=3,
                                frames[i].xlim2[1]+0.1*(frames[i].xlim1[2]-frames[i].xlim1[1]),
                                frames[i].ylim2[1]+(0.95-(j-1)*0.06)*(frames[i].ylim2[2]-frames[i].ylim2[1]),
                                text(annotations2[j],font(6)))
            end
        end

        try
            Plots.savefig(string(dir,"/",filename,"_discharge_",i,".pdf"))
        catch ex
            println(string("unable to plot discharge ",i," for basin ",filename))
            println(ex)
            flush(stdout)
        end


        # plot snow layers time series:
        # -----------------------------

        if "Ssnow01" in res_sim.colnames

            n_layer = 1
            while string("Ssnow",lpad(n_layer+1,2,"0")) in res_sim.colnames; n_layer = n_layer + 1; end

            local l2 = Plots.@layout [a{0.47h}; b{0.06h}; c{0.47h}]
            Plots.plot(layout=l2)

            max_snow = maximum(res_sim.results[:,findfirst(isequal(string("Ssnow",lpad(n_layer,2,"0"))),res_sim.colnames)])
       
            # calibration plot:
            Plots.plot!(subplot=1,[0,1],[0,0],linewidth=0,linecolor="black",label="",
                        xlabel="time [d]",ylabel="Snow layers [mm]",
                        title=frames[i].title1,titlefont=font(10),
                        xlims=frames[i].xlim1,
                        ylims=(0,max_snow),
                        grid=false)
            ind = findall(x->(x>=frames[i].xlim1[1])&(x<=frames[i].xlim1[2]),res_sim.results[:,1])
            for j in 1:n_layer
                Plots.plot!(subplot=1,
                            res_sim.results[ind,1],
                            res_sim.results[ind,findfirst(isequal(string("Ssnow",lpad(j,2,"0"))),res_sim.colnames)],
                            label=string("Ssnow",lpad(j,2,"0")),linewidth=1,linecolor=j)
            end
            if ( ! ismissing(annotations1) )
                for j in 1:length(annotations1)
                    Plots.annotate!(subplot=1,
                                    frames[i].xlim1[1]+0.1*(frames[i].xlim1[2]-frames[i].xlim1[1]),
                                    (0.95-(j-1)*0.06)*max_snow,
                                    text(annotations1[j],font(6)))
                end
            end
        
            # empty space to avoid overlap of second title with first label:
            Plots.plot!(subplot=2,legend=false,grid=false,foreground_color_subplot=:white)
          
            # validation plot:
            Plots.plot!(subplot=3,[0,1],[0,0],linewidth=0,linecolor="black",label="",
                        xlabel="time [d]",ylabel="Snow layers [mm]",
                        title=frames[i].title1,titlefont=font(10),
                        xlims=frames[i].xlim2,
                        ylims=(0,max_snow),
                        grid=false)
            ind = findall(x->(x>=frames[i].xlim2[1])&(x<=frames[i].xlim2[2]),res_sim.results[:,1])
            for j in 1:n_layer
                Plots.plot!(subplot=3,
                            res_sim.results[ind,1],
                            res_sim.results[ind,findfirst(isequal(string("Ssnow",lpad(j,2,"0"))),res_sim.colnames)],
                            label="",linewidth=1,linecolor=j)
            end
            if ( ! ismissing(annotations2) )
                for j in 1:length(annotations2)
                    Plots.annotate!(subplot=3,
                                    frames[i].xlim2[1]+0.1*(frames[i].xlim2[2]-frames[i].xlim2[1]),
                                    (0.95-(j-1)*0.06)*max_snow,
                                    text(annotations2[j],font(6)))
                end
            end
           
            try
                Plots.savefig(string(dir,"/",filename,"_snow_",i,".pdf"))
            catch ex
                println(string("unable to plot snow layers ",i," for basin ",filename))
                println(ex)
                flush(stdout)
            end
        
        end

        # plot sensitivity time series:
        # -----------------------------

        if ! ( ismissing(res_sens) | ismissing(res_sim_Pmult_micinc) | ismissing(res_sim_Tshift_micinc) | ismissing(res_sim_Pmult_macinc) | ismissing(res_sim_Tshift_macinc) )

            local l3 = Plots.@layout [a{0.47h}; b{0.06h}; c{0.47h}]
            Plots.plot(layout=l3)

            # calibration plot:
            Plots.plot!(subplot=1,frames[i].xlim1,[0,0],linewidth=1,linecolor="black",label="",
                        xlabel="time [d]",ylabel="sens [mm/d/10%Prec, mm/d/degC]",
                        title=frames[i].title1,titlefont=font(10),
                        xlims=frames[i].xlim1,
                        ylims=(min_sens,max_sens),
                        grid=false)
            ind = findall(x->(x>=frames[i].xlim1[1])&(x<=frames[i].xlim1[2]),res_sens.sensitivities[:,1])
            Plots.plot!(subplot=1,
                        res_sens.sensitivities[ind,1],
                        0.1 .* res_sens.sensitivities[ind,2],
                        label=string(res_sens.colnames[2],": 10%"),linecolor="lightblue")
            if ! ismissing(res_sim_Pmult_micinc)
                Plots.plot!(subplot=1,
                            res_sim.results[ind,1],
                            0.1*(res_sim_Pmult_micinc.results[ind,end]-res_sim.results[ind,end])/dp,
                            label=string(res_sens.colnames[2],": 10% FD"),linecolor="blue",linestyle=:dot)
            end
            if ! ismissing(res_sim_Pmult_macinc)
                Plots.plot!(subplot=1,
                            res_sim.results[ind,1],
                            res_sim_Pmult_macinc.results[ind,end]-res_sim.results[ind,end],
                            label=string("Delta P: 10%"),linecolor="darkblue",linestyle=:dot)
            end
            Plots.plot!(subplot=1,
                        res_sens.sensitivities[ind,1],
                        res_sens.sensitivities[ind,3],
                        label=string(res_sens.colnames[3],": 1degC"),linecolor="orangered")
            if ! ismissing(res_sim_Tshift_micinc)
                Plots.plot!(subplot=1,
                            res_sim.results[ind,1],
                            (res_sim_Tshift_micinc.results[ind,end]-res_sim.results[ind,end])/dT,
                            label=string(res_sens.colnames[3],": 1degC FD"),linecolor="red",linestyle=:dot)
            end
            if ! ismissing(res_sim_Tshift_macinc)
                Plots.plot!(subplot=1,
                            res_sim.results[ind,1],
                            res_sim_Tshift_macinc.results[ind,end]-res_sim.results[ind,end],
                            label=string("Delta T: 1deg"),linecolor="darkred",linestyle=:dot)
            end
            if ( ! ismissing(annotations1) )
                for j in 1:length(annotations1)
                    Plots.annotate!(subplot=1,
                                    frames[i].xlim1[1]+0.1*(frames[i].xlim1[2]-frames[i].xlim1[1]),
                                    min_sens+(0.95-(j-1)*0.06)*(max_sens-min_sens),
                                    text(annotations1[j],font(6)))
                end
            end

            # empty space to avoid overlap of second title with first label:
            Plots.plot!(subplot=2,legend=false,grid=false,foreground_color_subplot=:white)

            # validation plot:
            Plots.plot!(subplot=3,frames[i].xlim2,[0,0],linewidth=1,linecolor="black",label="",
                        xlabel="time [d]",ylabel="sens [mm/d/10%Prec, mm/d/degC]",
                        title=frames[i].title2,titlefont=font(10),
                        xlims=frames[i].xlim2,
                        ylims=(min_sens,max_sens),
                        grid=false)
            ind = findall(x->(x>=frames[i].xlim2[1])&(x<=frames[i].xlim2[2]),res_sens.sensitivities[:,1])
            Plots.plot!(subplot=3,
                        res_sens.sensitivities[ind,1],
                        0.1 .* res_sens.sensitivities[ind,2],
                        label="",linecolor="lightblue")
            if ! ismissing(res_sim_Pmult_micinc)
                Plots.plot!(subplot=3,
                            res_sim.results[ind,1],
                            0.1*(res_sim_Pmult_micinc.results[ind,end]-res_sim.results[ind,end])/dp,
                            label="",linecolor="blue",linestyle=:dot)
            end
            if ! ismissing(res_sim_Pmult_macinc)
                Plots.plot!(subplot=3,
                            res_sim.results[ind,1],
                            res_sim_Pmult_macinc.results[ind,end]-res_sim.results[ind,end],
                            label="",linecolor="darkblue",linestyle=:dot)
            end
            if ! ismissing(res_sim_Pmult_macinc)
                Plots.plot!(subplot=3,
                            res_sim.results[ind,1],
                            res_sim_Pmult_macinc.results[ind,end]-res_sim.results[ind,end],
                            label="",linecolor="darkblue",linestyle=:dot)
            end
            Plots.plot!(subplot=3,
                        res_sens.sensitivities[ind,1],
                        res_sens.sensitivities[ind,3],
                        label="",linecolor="orangered")
            if ! ismissing(res_sim_Tshift_micinc)
                Plots.plot!(subplot=3,
                res_sim.results[ind,1],
                (res_sim_Tshift_micinc.results[ind,end]-res_sim.results[ind,end])/dT,
                label="",linecolor="red",linestyle=:dot)
            end
            if ! ismissing(res_sim_Tshift_macinc)
                Plots.plot!(subplot=3,
                res_sim.results[ind,1],
                res_sim_Tshift_macinc.results[ind,end]-res_sim.results[ind,end],
                label="",linecolor="darkred",linestyle=:dot)
            end
            if ! ismissing(res_sim_Tshift_macinc)
                Plots.plot!(subplot=3,
                res_sim.results[ind,1],
                res_sim_Tshift_macinc.results[ind,end]-res_sim.results[ind,end],
                label="",linecolor="darkred",linestyle=:dot)
            end
            if ( ! ismissing(annotations2) )
                for j in 1:length(annotations2)
                    Plots.annotate!(subplot=3,
                                    frames[i].xlim2[1]+0.1*(frames[i].xlim2[2]-frames[i].xlim2[1]),
                                    min_sens+(0.95-(j-1)*0.06)*(max_sens-min_sens),
                                    text(annotations2[j],font(6)))
                end
            end
                
            try
                Plots.savefig(string(dir,"/",filename,"_sensitivities_",i,".pdf"))
            catch ex
                println(string("unable to plot sensitivities ",i," for basin ",filename))
                println(ex)
                flush(stdout)
            end

        end

    end

end


function Camels_NSE(Qobs,Qsim)

    meanQobs = Statistics.mean(Qobs)
    return 1.0 - sum( (Qsim .- Qobs).^2 ) / sum( (Qobs .- meanQobs).^2 )

end