#!/bin/bash
julia Hyperflex_run_Camels.jl   1   5 &> ../Output_Camels/GR4neige_Optimization_Seq1dODESolver_x1x2x3x4thetaG1thetaG2_bands5_Hoege_01.log &
julia Hyperflex_run_Camels.jl   6  10 &> ../Output_Camels/GR4neige_Optimization_Seq1dODESolver_x1x2x3x4thetaG1thetaG2_bands5_Hoege_02.log &
julia Hyperflex_run_Camels.jl  11  15 &> ../Output_Camels/GR4neige_Optimization_Seq1dODESolver_x1x2x3x4thetaG1thetaG2_bands5_Hoege_03.log &
julia Hyperflex_run_Camels.jl  16  20 &> ../Output_Camels/GR4neige_Optimization_Seq1dODESolver_x1x2x3x4thetaG1thetaG2_bands5_Hoege_04.log &
julia Hyperflex_run_Camels.jl  20  23 &> ../Output_Camels/GR4neige_Optimization_Seq1dODESolver_x1x2x3x4thetaG1thetaG2_bands5_Hoege_05.log &
julia Hyperflex_run_Camels.jl  24  27 &> ../Output_Camels/GR4neige_Optimization_Seq1dODESolver_x1x2x3x4thetaG1thetaG2_bands5_Hoege_06.log &
