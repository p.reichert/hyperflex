## ========================================================
##
## File: Hyperflex_test_Maimai.jl
##
## Test driver for Hierarchical Hydrological Model
## (at this time not yet hierarchical)
##
## Based on data of the Maimai catchment in New Zealand:
## http://www.hydroshare.org/resource/a292cb65a5d24a31a60978b2ab390266
##
## creation:          January  01, 2021 -- Peter Reichert
## last modification: January  11, 2022 -- Peter Reichert
## 
## contact:           peter.reichert@eawag.ch
##
## ========================================================


# Activate, instatiate and load packages:
# =======================================

import Pkg
Pkg.activate(".")
Pkg.instantiate()

using  DataFrames
import CSV
import JLD2

import Random
import Statistics

import Plots
import StatsPlots
import Plotly
Plots.gr()

include("Hyperflex.jl")


# Initialize parameters and control variables:
# ============================================

# choose which tasks to perform:
# ------------------------------

simulate        = true
testgradients   = true
inferparameters = false
initialsim      = true
produceplots    = true;  if Sys.islinux(); global produceplots = false; end
readandplot     = false

# file version descriptor:
# ------------------------

version = "maimai_test"

# directories:
# ------------

dir_input  = "../Input_Maimai"
dir_output = "../Output_Maimai"

# choose model:
# -------------

# model = "1box"
# model = "2box"
model = "GR4"

# model parameter values:
# -----------------------

if model == "1box"

    parnames_hyd = ["Styp","ce","S0",  "k" ,"alpha","m"];
    par_hyd      = [ 10.0 , 1.0,20.0,0.0002,  4.0  ,0.5];

elseif model == "2box"

    parnames_hyd = ["Styp","ce","S10","S20","k1", "k2","k12","alpha1","alpha2","alpha12","m1","m2"];
    par_hyd      = [ 10.0 , 1.0, 15.0, 1.0 ,0.04,0.002, 0.08,   2.5  ,   1.5  ,   1.0   , 0.5, 0.5];

elseif model == "GR4"

    parnames_hyd = [ "x1","x2","x3","x4","alpha","beta","gamma","omega","Phi",  "nu" ,"Ut","nres","ce"];
    par_hyd      = [350.0, 0.0,90.0, 1.7,  2.0  ,  5.0 ,  5.0  ,  3.5  , 0.9 ,1.4/9.0, 1.0, 11.0 , 1.0];

else

    error(string("*** Unknown model: ",model))

end

parnames_lik  = ["sigmaQ"];
par_lik       = [   0.1   ];

parnames_sens = ["Pmult","Tshift"];
par_sens      = [  1.0  ,   0.0  ];

parnames      = vcat(parnames_hyd,parnames_lik,parnames_sens)
par           = vcat(par_hyd,par_lik,par_sens)

# choose model parameters to be estimated and initial values:
# -----------------------------------------------------------

if     model == "1box"

    # parnames_est = ["S0",  "k" , "alpha", "ce","sigmaQ"]
    # par_est      = [20.0,0.0002,   4.0  , 1.0 ,   0.1   ]
    parnames_est = ["S0",  "k" , "alpha","sigmaQ"]
    par_est      = [20.0,0.0002,   4.0  ,   0.1   ]

elseif model == "2box"

    # parnames_est = ["S10", "S20","k1", "k2","k12","alpha1","alpha2","ce","sigmaQ"]
    # par_est      = [ 15.0,  1.0 ,0.04,0.002, 0.08,   2.5  ,   1.5  , 1.0,   0.1   ]
    parnames_est = ["S10", "S20","k1", "k2","k12","alpha1","sigmaQ"]
    par_est      = [ 15.0,  1.0 ,0.04,0.002, 0.08,   2.5  ,   0.1   ]

elseif model == "GR4"

    parnames_est = [ "x1","x2","x3","x4"];
    par_est      = [350.0, 0.0,90.0, 1.7];

else
    error(string("*** Unknown model: ",model))
end

# choose integration algorithm:
# -----------------------------

# overview of options: https://diffeq.sciml.ai/stable/solvers/ode_solve/

# IntAlg               = DifferentialEquations.Euler()
# IntAlg               = DifferentialEquations.Tsit5()
# IntAlg               = DifferentialEquations.BS3()
# IntAlg               = DifferentialEquations.ImplicitEuler()
# IntAlg               = DifferentialEquations.ImplicitMidpoint()
# IntAlg               = DifferentialEquations.Trapezoid()
# IntAlg               = DifferentialEquations.Hairer4()
IntAlg               = DifferentialEquations.Rodas3()
# IntAlg               = DifferentialEquations.Rodas5()

# choose inference method:
# ------------------------

InferenceMethod      = "AdvancedHMC"
# InferenceMethod      = "Optimization"
# InferenceMethod      = "StochasticOpt"
# InferenceMethod      = "GalacticOptim"   # does not yet work
# InferenceMethod      = "DynamicHMC"
# InferenceMethod      = "VI"              # does not yet  work

# choose automatic differentiation algorithm:
# -------------------------------------------

# AutoDiff             = "ForwardDiff"
AutoDiff             = "Zygote"

# numerical parameters ode integration:
# -------------------------------------

dt            = 0.1
dtmax         = 0.5
reltol        = 1.0e-5
abstol        = 1.0e-5

verbose_model = false

# numerical parameters for sampling:
# ----------------------------------

fact_prior_bounds = 1000.0     # factor for bounds of positive parameters
                               # (to avoid numerical integration problems)
n_samples     =  200
n_adapt       =  100


# Expand file version descriptor by selected options:
# ===================================================

version = string(version,"_",
                 model,"_",
                 split(split(split(String(Symbol(IntAlg)),".")[2],"(")[1],"{")[1],"_",
                 InferenceMethod,"_",
                 AutoDiff)


# Read and select input data:
# ===========================

data = CSV.read(string(dir_input,"/","Maimai010h.dat"),  DataFrame, 
                header=false, skipto=8, delim=' ', ignorerepeated=true)
rename!(data,["year","month","day","hour","min","QC","P","E","Q"])
insertcols!(data,1,"time" => 0:(nrow(data)-1))
data[!,["time","P","E","Q"]] = convert.(Float64,data[!,["time","P","E","Q"]])

println("Number of data records read:     ",nrow(data))
# delete!(data,(365*24+1):nrow(data))    # select first year (1985)
delete!(data,(181*24+1):nrow(data))    # select Jan to June 1985
# delete!(data,(31*24+1):nrow(data))     # select Jan 1985
println("Number of data records selected: ",nrow(data))


# Define output time points and default model parameters:

times = collect(range(minimum(data[!,"time"]),stop=maximum(data[!,"time"]),step=0.2))


# Define model function with defaults for most arguments:
# =======================================================

# call: runmodel(times,par,parnames) , where par,parnames can be a subset of the model parameters

runmodel = Hyperflex_Get_RunModel(model,
                                  par,
                                  parnames,
                                  Matrix{Float64}(data[:,["time","P"]]),
                                  Matrix{Float64}(data[:,["time","E"]]),
                                  missing,      # Tmin
                                  missing,      # Tmax
                                  missing,      # Lday
                                  missing;      # elevation band forcing
                                  algorithm = IntAlg,
                                  dt        = dt,
                                  dtmax     = dtmax,
                                  reltol    = reltol,
                                  abstol    = abstol)
                                  
# Run simulation:
# ===============

if simulate

    # run model with complete model function:

    @time res = Hyperflex_RunModel(model,
                                   par,
                                   parnames,
                                   Matrix{Float64}(data[:,["time","P"]]),  # P
                                   Matrix{Float64}(data[:,["time","E"]]),  # E
                                   missing,                                # Tmin
                                   missing,                                # Tmax
                                   missing,                                # Lday
                                   missing,                                # elevation band forcing
                                   times;
                                   algorithm = IntAlg,
                                   dt        = dt,
                                   dtmax     = dtmax,
                                   reltol    = reltol,
                                   abstol    = abstol,
                                   verbose   = verbose_model)

    # run model with simplified call:

    @time res2 = runmodel(times,par,parnames)

    # write and plot results:

    CSV.write(string(dir_output,"/","res_",version,"_simulation.dat"),Tables.table(res.results;header=res.colnames);delim='\t')

    if produceplots

        p1 = Plots.plot(data[!,"time"],data[!,"Q"],label="",
                        lw=0,line="white",markershape=:circle,markersize=1.5,markercolor="black",
                        xlabel="time [h]",ylabel="Q [mm/h]",
                        title=string("Time series ",model))
        Plots.plot!(res.results[:,1],res.results[:,size(res.results,2)],label="",line="red")
        #display(p1)
        Plots.savefig(p1,string(dir_output,"/","res_",version,"_simulation.pdf"))

    end

    # test of log likelihood and its gradient: 

    if testgradients

        # get log likelihood for full set of model parameters:

        loglikeli_par = Hyperflex_Get_Loglikeli(
                            runmodel,
                            Hyperflex_Logpdfobs_IndepHomoscedasticNormal,
                            par,
                            parnames,
                            Matrix{Float64}(data[:,["time","Q"]]),
                            parnames;
                            starttime = NaN)

        # evaluate log likelihood and its gradient:
 
        sigdigits = 6
        println()
        # test log likelihood:
        @time println(string("Log likelihood:                      ",loglikeli_par(par)))
        println()
        # test of gradient of log likelihood:
        @time println(string("Log likelihood ForwardDiff.gradient: ",
                      string.(parnames,"=",round.(ForwardDiff.gradient(loglikeli_par,par),sigdigits=sigdigits))))
        println()
        @time println(string("Log likelihood Zygote.gradient:      ",
                      string.(parnames,"=",round.(Zygote.gradient(loglikeli_par,par)[1],sigdigits=sigdigits))))

    end

end

# Infer model parameters:
# =======================

if inferparameters | testgradients

    if initialsim

        # perform initial simulation with initial values of parameters to be estimated:

        res_start = runmodel(times,par_est,parnames_est)

        # write and plot results:

        CSV.write(string(dir_output,"/","res_",version,"_initialsim.dat"),Tables.table(res_start.results;header=res_start.colnames);delim='\t')

        if produceplots

            p1 = Plots.plot(data[!,"time"],data[!,"Q"],label="",
                            lw=0,line="white",markershape=:circle,markersize=1.5,markercolor="black",
                            xlabel="time [h]",ylabel="Q [mm/h]",
                            title=string("Time series ",model))
            Plots.plot!(res_start.results[:,1],res_start.results[:,size(res_start.results,2)],label="",line="red")
            #display(p1)
            Plots.savefig(p1,string(dir_output,"/","res_",version,"_initialsim.pdf"))

        end

    end

    # get likelihood for subset of model parameters (in preparation of parameter estimation):

    loglikeli_par_est = Hyperflex_Get_Loglikeli(
                            runmodel,
                            Hyperflex_Logpdfobs_IndepHomoscedasticNormal,
                            par,
                            parnames,
                            Matrix{Float64}(data[:,["time","Q"]]),
                            parnames_est;
                            starttime = NaN)
                
   # test of log likelihood and its gradient: 

   if testgradients

        # evaluate log likelihood and its gradient:
        
        sigdigits = 6
        println()
        # test log likelihood:
        @time println(string("Log likelihood:                      ",loglikeli_par_est(par_est)))
        println()
        # test of gradient of log likelihood:
        @time println(string("Log likelihood ForwardDiff.gradient: ",
                      string.(parnames_est,"=",round.(ForwardDiff.gradient(loglikeli_par_est,par_est),sigdigits=sigdigits))))
        println()
        @time println(string("Log likelihood Zygote.gradient:      ",
                      string.(parnames_est,"=",round.(Zygote.gradient(loglikeli_par_est,par_est)[1],sigdigits=sigdigits))))

    end

    # define prior:

    function logprior(par_est,parnames_est,par,parnames)

        lp = 0.0
        if length(par_est) == 0; return lp; end
        sdrel = 0.5  # all priors except for x2 are lognormal and have a relative 
                     # standard deviation of 0.5 (to be adapted if needed);
                     # the prior for x2 is normal with mean 0 and sd 1
        sdlog = sqrt(log(1+sdrel^2))
        for i in 1:length(parnames_est)
            j = findfirst(isequal(parnames_est[i]),parnames)
            if ! ismissing(j)
                if parnames_est[i] == "x2"
                    lp = lp + DistributionsAD.logpdf(DistributionsAD.Normal(0.0,1.0),par_est[i])
                else
                    if ( par_est[i] <= 0.0 ); return -Inf; end
                    meanlog = log(par[j]) - 0.5*sdlog^2
                    lp = lp + DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                end
            end
        end

        return lp

    end
    
    # get log prior as a function of estimated parameters only:

    logprior_par_est = Hyperflex_Get_LogPrior(logprior,parnames_est,par,parnames)
    
    if testgradients

        # evaluate log prior and its gradient:

        sigdigits = 6
        println()
        # test log prior:
        @time println(string("Log prior:                      ",logprior_par_est(par_est)))
        println()
        # test of gradient of log prior:
        @time println(string("Log prior ForwardDiff.gradient: ",
                      string.(parnames_est,"=",round.(ForwardDiff.gradient(logprior_par_est,par_est),sigdigits=sigdigits))))
        println()
        @time println(string("Log prior Zygote.gradient:      ",
                      string.(parnames_est,"=",round.(Zygote.gradient(logprior_par_est,par_est)[1],sigdigits=sigdigits))))

    end

    # get log posterior:

    logpost_par_est = Hyperflex_Get_LogPosterior(logprior_par_est,loglikeli_par_est)

    if testgradients

        # evaluate log poeterior and its gradient:

        sigdigits = 6
        println()
        # test log posterior:
        @time println(string("Log posterior:                      ",logpost_par_est(par_est)))
        println()
        # test of gradient of log posterior:
        @time println(string("Log posterior ForwardDiff.gradient: ",
                      string.(parnames_est,"=",round.(ForwardDiff.gradient(logpost_par_est,par_est),sigdigits=sigdigits))))
        println()
        @time println(string("Log posterior Zygote.gradient:      ",
                      string.(parnames_est,"=",round.(Zygote.gradient(logpost_par_est,par_est)[1],sigdigits=sigdigits))))

    end

    # Define parameter transformation to accelerate inference and keep bounds:
    # ------------------------------------------------------------------------

    if inferparameters

        transpar = Array{TransPar}(undef,length(par_est));
        for j = 1:length(par_est)
            if parnames_est[j] == "x2"
                transpar[j] = TransParUnbounded()
            else
                # transpar[j] = TransParLowerBound(0.0)
                transpar[j] = TransParInterval(par_est[j]/fact_prior_bounds,
                                               par_est[j]*fact_prior_bounds)
            end
        end


        # Estimate model parameters:
        # --------------------------

        res_inference = Hyperflex_InferParameters(
                            logpost_par_est,
                            par_est,
                            parnames_est;
                            Trans      = transpar,
                            method     = InferenceMethod,
                            AutoDiff   = AutoDiff,
                            n_samples  = n_samples,
                            n_adapt    = n_adapt)

        CSV.write(string(dir_output,"/","res_",version,"_sample.dat"),
                         Tables.table(Array(res_inference);header=parnames_est);delim='\t')

        JLD2.@save string(dir_output,"/","res_",version,"_sample.jld2") res_inference runmodel par parnames par_est parnames_est
        # JLD2.@load string(dir_output,"/","res_",version,"_sample.jld2") res_inference runmodel par parnames par_est parnames_est

        if (InferenceMethod == "AdvancedHMC") | (inferenceMethod == "DynamicHMC")
            mean_post      = Statistics.mean(res_inference[n_adapt:n_samples,:,:]);
        else
            mean_post      = Statistics.mean(res_inference);
        end
        par_post       = mean_post[:,2];
        parnames_post  = String.(mean_post[:,1]);

        res_post = runmodel(times,par_post,parnames_post)

        CSV.write(string(dir_output,"/","res_",version,"_postsim.dat"),Tables.table(res_post.results;header=res_post.colnames);delim='\t')

        if produceplots

            p2 = Plots.plot(res_inference)
            #display(p2)
            Plots.savefig(p2,string(dir_output,"/","res_",version,"_sample.pdf"))

            p3 = Plots.plot(data[!,"time"],data[!,"Q"],label="",
                            lw=0,line="white",markershape=:circle,markersize=1.5,markercolor="black",
                            xlabel="time [h]",ylabel="Q [mm/h]",
                            title=string("Time series ",model))
            Plots.plot!(res_post.results[:,1],res_post.results[:,size(res_post.results,2)],label="",line="red")
            #display(p3)
            Plots.savefig(p3,string(dir_output,"/","res_",version,"_postsim.pdf"))

        end

    end

end

# Read and plot results:
# ======================

if readandplot

    JLD2.@load string(dir_output,"/","res_",version,"_sample.jld2") res_inference runmodel par parnames par_est parnames_est

    mean_post      = Statistics.mean(res_inference[n_adapt:n_samples,:,:]);
    par_post       = mean_post[:,2];
    parnames_post  = String.(mean_post[:,1]);

    if produceplots

        p2 = Plots.plot(res_inference)
        #display(p2)
        Plots.savefig(p2,string(dir_output,"/","res_",version,"_sample.pdf"))

        res_post = runmodel(times,par_post,parnames_post)

        CSV.write(string(dir_output,"/","res_",version,"_postsim.dat"),Tables.table(res_post.results;header=res_post.colnames);delim='\t')

        p3 = Plots.plot(data[!,"time"],data[!,"Q"],label="",
                        lw=0,line="white",markershape=:circle,markersize=1.5,markercolor="black",
                        xlabel="time [h]",ylabel="Q [mm/h]",
                        title=string("Time series ",model))
        Plots.plot!(res_post.results[:,1],res_post.results[:,size(res_post.results,2)],label="",line="red")
        #display(p3)
        Plots.savefig(p3,string(dir_output,"/","res_",version,"_postsim.pdf"))

    end

end