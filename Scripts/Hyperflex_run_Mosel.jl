## =======================================================
##
## File: Hyperflex_run_Mosel.jl
##
## Test driver for Hierarchical Hydrological Model
##
## creation:          February 26, 2022 -- Peter Reichert
## last modification: March    03, 2022 -- Peter Reichert
## 
## contact:           peter.reichert@eawag.ch
##
## =======================================================

## Indices:
##
## i:    basins
## j:    parameters
## k,l:  different purposes


# Activate, instatiate and load packages:
# =======================================

import Pkg
Pkg.activate(".")
Pkg.instantiate()

using  DataFrames
import CSV
import DelimitedFiles
import JLD2

import Dates

import Statistics

import Plots
import StatsPlots
import Plotly
Plots.gr()

include("Hyperflex.jl")


# Initialize parameters and control variables:
# ============================================

# tasks:
# ------

simulate         = false
infer_parameters = true

testgradients    = false

verbose          = true

# directories:
# ------------

dir_input  = "../Input_Mosel"
dir_output = "../Output_Mosel"

# file version descriptor:
# ------------------------

version = "20220302"

# choose model:
# -------------

# model = "1box"
# model = "2box"
model = "GR4"
# model = "GR4neige"
# model = "HBV"
# model = "Hyperflex"

# model parameter values:
# -----------------------

# Default parameters for box models as used in:
#
# Reichert, P., Ammann, L. and Fenicia, F.
# Potential and Challenges of Investigating Intrinsic Uncertainty 
# of Hydrological Models with Stochastic, Time‐Dependent Parameters
# Water Resources Research, in press, 2021.
# https://doi.org/10.1029/2020WR028400

if model == "1box"

    parnames_hyd = ["Styp","ce","S0",  "k" ,"alpha","m"];
    par_hyd      = [ 10.0 , 1.0,20.0,0.0002,  4.0  ,0.5];

elseif model == "2box"

    parnames_hyd = ["Styp","ce","S10","S20","k1", "k2","k12","alpha1","alpha2","alpha12","m1","m2"];
    par_hyd      = [ 10.0 , 1.0, 15.0, 1.0 ,0.04,0.002, 0.08,   2.5  ,   1.5  ,   1.0   , 0.5, 0.5];

# Default parameters for GR4J and continoustime GR4 models as described in:
#
# Perrin, C., Michel, C. and Andreassian, V.
# Improvement of a parsimonious model for streamflow simulation
# Journal of Hydrology 279, 275-289, 2003
# https://doi.org/10.1016/S0022-1694(03)00225-7
#
# and
#
# Santos, L., Thirel, G. and Perrin, C.
# Continuous state-space representation of a bucket-type rainfall-runoff model:
# a case study with the GR4 model using state-space GR4 (version 1.0)
# Geoscientific Model Development 11, 1591-1605, 2018.
# https://doi.org/10.5194/gmd-11-1591-2018
#
# and for Cemaneige snow accumulation model described in:
#
# Audrey, V., Andreassian, V. and Perrin, C.
# `As simple as possible but not simpler': What is useful in a temperature-based
# snow-accounting routine? Part 2 - Sensitivity analysis of the Cemaneige snow
# accounting routine on 380 catchments
# Journal of Hydrology 517, 1176-1187, 2014.
# https://doi.org/10.1016/j.jhydrol.2014.04.058

elseif model == "GR4"

    parnames_hyd = [ "x1","x2","x3","x4","alpha","beta","gamma","omega","Phi",  "nu" ,"Ut","nres","ce"];
    par_hyd      = [350.0, 0.0,90.0, 1.7,  2.0  ,  5.0 ,  5.0  ,  3.5  , 0.9 ,1.4/9.0, 1.0, 11.0 , 1.0];

elseif model == "GR4neige"

    parnames_hyd = [ "x1","x2","x3","x4","alpha","beta","gamma","omega","Phi",  "nu" ,"Ut","nres","ce"];
    par_hyd      = [350.0, 0.0,90.0, 1.7,  2.0  ,  5.0 ,  5.0  ,  3.5  , 0.9 ,1.4/9.0, 1.0, 11.0 , 1.0];
    parnames_snow= ["thetaG1","thetaG2","Tsfth","Tsmth","DeltaTsm","Ssnth"];
    par_snow     = [   3.0   ,   0.5   ,  0.0  ,  0.0  ,    1.0   ,  1.0  ];
    parnames_hyd = vcat(parnames_hyd,parnames_snow)
    par_hyd      = vcat(par_hyd,par_snow)

elseif model == "HBV"

    parnames_alt = ["Tsfth","Tsmth","csf","cmelt","DeltaTsm","Ssnth","Sswth","cfr","cwh","Sfc","beta"];
    par_alt      = [  0.0  ,  0.0  , 1.0 ,  3.0  ,    1.0   ,  1.0  ,  0.5  , 0.1 , 0.1 ,100.0,  3.0 ];
    parnames_hyd = ["cperc","Suzth","Suzdiv","k0","k1","k2","kr","kdg","nres","Ssmth","ce"];
    par_hyd      = [  1.5  ,  1.0  ,  10.0  , 0.5, 0.1, 0.1, 2.0, 0.0 ,  5.0 ,  0.5  , 1.0];
    parnames_hyd = vcat(parnames_alt,parnames_hyd)
    par_hyd      = vcat(par_alt,par_hyd)

elseif model == "Hyperflex"

    parnames_hyd = ["Sumax","Sgmax","alphau","alphap","alphag","alphab",
                    "ki","kp","kf","kr","kg","kdg","fi","Dm","Dr","Suth","ce","nres"];
    par_hyd      = [ 200.0 , 400.0 ,   2.0  ,   2.0  ,   2.0  ,   2.0  ,
                     3.0, 0.3, 1.0, 1.0, 0.1, 0.0 ,0.01, 0.9, 0.9,  0.5 , 1.0,  5.0 ];
    parnames_snow= ["cmelt","amelt","DeltaTsm","Ssnth","Tsfth","Tsmth"];
    par_snow     = [  3.0  ,  0.01 ,    1.0   ,  1.0  ,  0.0  ,  0.0  ];
    parnames_hyd = vcat(parnames_hyd,parnames_snow)
    par_hyd      = vcat(par_hyd,par_snow)

else

    error(string("*** Unknown model: ",model))

end

parnames_lik  = ["sigmaQ"];
par_lik       = [   1.0   ];

parnames_sens = ["Pmult","Tshift"];
par_sens      = [  1.0  ,   0.0  ];

parnames      = vcat(parnames_hyd,parnames_lik,parnames_sens)
par           = vcat(par_hyd,par_lik,par_sens)

# choose model parameters to be estimated and initial values:
# -----------------------------------------------------------

if     model == "1box"

    # parnames_est  = ["S0",  "k" , "alpha", "ce","sigmaQ"]
    # par_est       = [20.0,0.0002,   4.0  , 1.0 ,   0.1   ]
    parnames_est  = ["S0",  "k" , "alpha","sigmaQ"]
    par_est       = [20.0,0.0002,   4.0  ,   0.1   ]
	startval_from = ""

elseif model == "2box"

    # parnames_est  = ["S10", "S20","k1", "k2","k12","alpha1","alpha2","ce","sigmaQ"]
    # par_est       = [ 15.0,  1.0 ,0.04,0.002, 0.08,   2.5  ,   1.5  , 1.0,   0.1   ]
    parnames_est  = ["S10", "S20","k1", "k2","k12","alpha1","sigmaQ"]
    par_est       = [ 15.0,  1.0 ,0.04,0.002, 0.08,   2.5  ,   0.1   ]
	startval_from = ""

elseif model == "GR4"

    parnames_est  = [ "x1","x2","x3","x4","sigmaQ"];
    par_est       = [350.0, 0.0,90.0, 1.7,   1.0   ];
	startval_from = "bestpar_GR4_x1x2x3x4sigmaQ.dat"

elseif model == "GR4neige"

    # parnames_est  = [ "x1","x2","x3","x4","thetaG1","thetaG2","sigmaQ"];
    # par_est       = [350.0, 0.0,90.0, 1.7,   3.0   ,   0.5   ,   1.0   ];
    parnames_est  = [ "x1","x2","x3","x4","thetaG1","thetaG2",
                     "Tsfth","Tsmth","alpha","beta","gamma","sigmaQ"];
    par_est       = [350.0, 0.0,90.0, 1.7,   3.0   ,   0.5   ,
                       0.0  ,  0.0  ,  2.0  ,  5.0 ,  5.0  ,   1.0  ];
	startval_from = "bestpar_GR4neige_x1x2x3x4thetaG1thetaG2sigmaQ.dat"

elseif model == "HBV"

    parnames_est = ["cmelt","Sfc","beta",
                    "cperc","Suzdiv","k0","k1","k2","kr","kdg",
                    "sigmaQ"];
    par_est      = [  3.0  ,100.0,  3.0 ,
                      1.5  ,  10.0  , 0.5, 0.1, 0.1, 2.0, 0.0 ,
                      1.0   ];
    # parnames_est = ["cmelt","Sfc","beta",
    #                 "cperc","Suzdiv","k0","k1","k2","kr",
    #                 "Tsfth","Tsmth",
    #                 "sigmaQ"];
    # par_est      = [  3.0  ,100.0,  3.0 ,
    #                   1.5  ,  10.0  , 0.5, 0.1, 0.1, 2.0,
    #                   0.0  ,  0.0  ,
    #                   1.0   ];
	startval_from = "bestpar_HBV_cmeltSfccpercSuzdivk0k1k2krsigmaQ.dat"

elseif model == "Hyperflex"

    # parnames_est = ["Sumax","Sgmax",
    #                 "ki","kp","kf","kr","kg","kdg","fi","Dm","Dr",
    #                 "cmelt",
    #                 "sigmaQ"];
    # par_est      = [ 200.0 , 400.0 ,
    #                  2.0, 0.2, 1.0, 1.0, 0.1, 0.0 ,0.01, 0.9, 0.9,
    #                   3.0  ,
    #                    1.0  ];
    parnames_est = ["Sumax","Sgmax",
                    "ki","kp","kf","kr","kg","kdg","fi","Dm","Dr",
                    "cmelt",
                    "Tsfth","Tsmth","amelt","alphau","alphap","alphag","alphab",
                    "sigmaQ"];
    par_est      = [ 200.0 , 400.0 ,
                     2.0, 0.2, 1.0, 1.0, 0.1, 0.0 ,0.01, 0.9, 0.9,
                     3.0,
                      0.0  ,  0.0  ,  0.01 ,   2.0  ,   2.0  ,   2.0  ,   2.0  ,
                       1.0  ];
    startval_from = "bestpar_Hyperflex_SumaxSgmaxkikpkfkrkgkdgfiDmDrrssigmaQ.dat"

else
    error(string("*** Unknown model: ",model))
end

# choose integration algorithm:
# -----------------------------

# overview of options: https://diffeq.sciml.ai/stable/solvers/ode_solve/

# IntAlg               = DifferentialEquations.Euler()
# IntAlg               = DifferentialEquations.Tsit5()
# IntAlg               = DifferentialEquations.BS3()

# IntAlg               = DifferentialEquations.ImplicitEuler()
# IntAlg               = DifferentialEquations.ImplicitMidpoint()
# IntAlg               = DifferentialEquations.Trapezoid()
# IntAlg               = DifferentialEquations.Hairer4()
# IntAlg               = DifferentialEquations.Rodas3()
# IntAlg               = DifferentialEquations.Rodas5()
# IntAlg               = DifferentialEquations.Rosenbrock23()
# IntAlg               = DifferentialEquations.TRBDF2()
# IntAlg               = "Seq1dODESolver"   # only compatible with AutoDiff = ForwardDiff

IntAlg = [DifferentialEquations.TRBDF2(),
          DifferentialEquations.Rodas3(),
          DifferentialEquations.Rosenbrock23()]
          
# choose inference method:
# ------------------------

# InferenceMethod      = "AdvancedHMC"
InferenceMethod      = "Optimization"
# InferenceMethod      = "GradFreeOpt"
# InferenceMethod      = "StochasticOpt"
# InferenceMethod      = "GalacticOptim"   # does not yet work
# InferenceMethod      = "DynamicHMC"
# InferenceMethod      = "VI"              # does not yet work

# choose automatic differentiation algorithm:
# -------------------------------------------

AutoDiff             = "ForwardDiff"
# AutoDiff             = "Zygote"

# numerical parameters for ODE integration:
# -----------------------------------------

years_warmup      = 3
# dt_ODE            = 0.2
dt_ODE            = 0.05
dt_ODE_max        = 1.0
reltol_ODE        = 1.0e-3
abstol_ODE        = 1.0e-5
dt_out            = 0.2

# numerical parameters for sampling:
# ----------------------------------

n_samples         = 1000
n_adapt           =  200
fact_prior_bounds =  100.0

# Read data:
# ==========

data   = DataFrame(DelimitedFiles.readdlm(string(dir_input,"/","DataFlex_tot.dat");skipstart=1),:auto)
header = replace.(DelimitedFiles.readdlm(string(dir_input,"/","DataFlex_tot.dat"),',')[1,:]," "=>"")
rename!(data,Symbol.(header))
data[!,"Time"] = Float64.(collect(0:(nrow(data)-1)))

times_out = data[:,"Time"]
    
basins = SubString.(header[findall(occursin.("P_",header))],3)
for basin in basins
    ind = findall(isequal(1),round.(Int,data[:,string("QC_Q_",basin)]))
    if ( length(ind) > 0 ); data[ind,string("Q_",basin)] .= NaN; end
end

# basins = [basins[1]]   # for testing only

n_basins = length(basins)


# expand file version descriptor:
# ===============================

if isa(IntAlg,Vector); IntAlg1 = IntAlg[1]; else IntAlg1 = IntAlg; end

version_inference = string(version,
                           "_",model,
                           "_",split(split(split(String(Symbol(IntAlg1)),".")[min(2,end)],"(")[1],"{")[1],
                           "_",InferenceMethod,
                           "_",AutoDiff,
                           "_",join(parnames_est))


# read parameter values from file if available:
# ---------------------------------------------

par_read_basins = Vector{Any}(undef,n_basins);
parnames_read_basins = String[]
par_est_basins = Vector{Any}(undef,n_basins);
parnames_est_basins = parnames_est
for i in 1:n_basins
    par_read_basins[i] = Float64[];
    par_est_basins[i]  = copy(par_est);
end
if isfile(string(dir_input,"/",startval_from))
    parread = CSV.read(string(dir_input,"/",startval_from),DataFrame;delim='\t', header = true)
    colnames = names(parread)
    if "basin" in colnames
        parread[!,"basin"] = lpad.(parread[!,"basin"],8,"0")
        for j in 1:length(colnames)
            if colnames[j] in parnames; push!(parnames_read_basins,colnames[j]); end
        end
        if length(parnames_read_basins) == 0
            println("*** no parameter columns found on parameter file")
        else
            for i in 1:n_basins
                par_read_basins[i] = Vector{Float64}(undef,length(parnames_read_basins))
                row_i = findfirst(isequal(basins[i]),parread[!,"basin"])
                if isnothing(row_i)
                    println(string("*** parameter values for basin ",basins[i]," not found"))
                    for j in 1:length(parnames_read_basins)
                        par_read_basins[i][j] = par[findfirst(isequal(parnames_read_basins[j]),parnames)]
                    end
                else
                    for j in 1:length(parnames_read_basins)
                        par_read_basins[i][j] = parread[row_i,findfirst(isequal(parnames_read_basins[j]),colnames)]
                    end
                    for j in 1:length(parnames_est_basins)
                        col_j = findfirst(isequal(parnames_est_basins[j]),colnames)
                        if isnothing(col_j)
                            par_est_basins[i][j] = par_est[j]
                        else
                            par_est_basins[i][j] = parread[row_i,col_j]
                        end
                    end
                end
            end
        end
    end
end


# Define model functions with defaults for most arguments:
# ========================================================

# call: simulator[i](times,par,parnames) , where par,parnames can be a subset of the model parameters

simulators_basins = Array{Any,1}(undef,n_basins);
for i in 1:n_basins

    t    = data[:,"Time"]                       # day
    Prec = data[:,string("P_",basins[i])];      # mm/day
    T    = data[:,string("T_",basins[i])];      # degC
    Epot = data[:,string("E_",basins[i])];      # mm/day

    prepend!(t,collect(1:(years_warmup*365)) .- years_warmup*365 .+ t[1] .- 1.0);
    for l in 1:years_warmup
        prepend!(Prec,Prec[1:365]);
        prepend!(T,T[1:365]);
        prepend!(Epot,Epot[1:365]);
    end

    simulators_basins[i] = Hyperflex_Get_RunModel(
                              model,
                              par,
                              parnames,
                              hcat(t,Prec),
                              hcat(t,Epot),
                              missing,       # Tmin is missing
                              missing,       # Tmax is missing
                              missing,       # Lday is missing
                              missing;       # Felevband is missing
                              algorithm      = IntAlg,
                              dt             = dt_ODE,
                              dtmax          = dt_ODE_max,
                              reltol         = reltol_ODE,
                              abstol         = abstol_ODE);

end


# Run simulation for all basins:
# ==============================

if simulate

    if length(parnames_read_basins) > 0 
        version_simulation = string(version,
                                    "_",model,
                                    "_",join(parnames_read_basins))
    else
        version_simulation = string(version,
                                    "_",model)
    end

    for i in 1:n_basins

        if length(parnames_read_basins) > 0
            @time res = HyperflexSimulate(simulators_basins[i],times_out,par_read_basins[i],parnames_read_basins;
                                          Qobs_cal=hcat(data[:,"Time"],data[:,string("Q_",basins[i])]),
                                          Qobs_val=missing,
                                          dir=dir_output,filename=string(basins[i],"_",version_simulation,"_simulation"))
            par_grad      = par_read_basins[i]
            parnames_grad = parnames_read_basins
        else
            @time res = HyperflexSimulate(simulators_basins[i],times_out,par,parnames;
                                          Qobs_cal=hcat(data[:,"Time"],data[:,string("Q_",basins[i])]),
                                          Qobs_val=missing,
                                          dir=dir_output,filename=string(basins[i],"_",version_simulation,"_simulation"))
            par_grad      = par
            parnames_grad = parnames
        end

        if testgradients

            # get log likelihood for full set of model parameters (with default values):

            loglikeli_par = Hyperflex_Get_Loglikeli(
                                simulators_basins[i],
                                Hyperflex_Logpdfobs_IndepHomoscedasticNormal,
                                par,
                                parnames,
                                Qobs[i][2],
                                parnames_grad;
                                starttime = - years_warmup * 365.0,
                                verbose   = verbose)

            # evaluate log likelihood and its gradient:
 
            sigdigits = 6
            println()
            # test log likelihood:
            @time println(string("Log likelihood basin ",basins[i],":                  ",round(loglikeli_par(par_grad),sigdigits=sigdigits)))
            println()
            # test of gradient of log likelihood:
            @time println(string("Log likelihood ForwardDiff.gradient (all pars): ",
                          string.(parnames_grad,"=",round.(ForwardDiff.gradient(loglikeli_par,par_grad),sigdigits=sigdigits))))
            println()
            @time println(string("Log likelihood Zygote.gradient (all pars):      ",
                          string.(parnames_grad,"=",round.(Zygote.gradient(loglikeli_par,par_grad)[1],sigdigits=sigdigits))))

        end
        
    end

end


# Define likelihoods, priors, posteriors for all basins and parameter transformations:
# ====================================================================================

loglikeli_basins = Array{Any,1}(undef,n_basins);
logpri_basins    = Array{Any,1}(undef,n_basins);
logpost_basins   = Array{Any,1}(undef,n_basins);

if infer_parameters | testgradients

    # Define parameter transformations:
    # ---------------------------------

    TransParBasin = Array{TransPar}(undef,length(parnames_est_basins));
    for j = 1:length(parnames_est_basins)
        if ( parnames_est_basins[j] == "x2" ) |
           ( parnames_est_basins[j] == "kdg")
            TransParBasin[j] = TransParUnbounded()
        elseif ( parnames_est_basins[j] == "alpha" ) | 
               ( parnames_est_basins[j] == "alphau" ) |
               ( parnames_est_basins[j] == "alphap" ) |
               ( parnames_est_basins[j] == "alphag" ) |
               ( parnames_est_basins[j] == "alphab" ) |
               ( parnames_est_basins[j] == "beta" ) |
               ( parnames_est_basins[j] == "gamma" ) |
               ( parnames_est_basins[j] == "omega" )
            TransParBasin[j] = TransParInterval(1.0,6.0);
        elseif ( parnames_est_basins[j] == "Phi" ) | 
               ( parnames_est_basins[j] == "Dm" ) | 
               ( parnames_est_basins[j] == "Dr" ) |
               ( parnames_est_basins[j] == "fi ") |
               ( parnames_est_basins[j] == "as ")
            TransParBasin[j] = TransParInterval(0.0,1.0);
        elseif parnames_est_basins[j] == "thetaG2"
            TransParBasin[j] = TransParInterval(0.01,0.99);
        elseif ( parnames_est_basins[j] == "Tsfth" ) |
               ( parnames_est_basins[j] == "Tsmth" )
            TransParBasin[j] = TransParInterval(-2.0,3.0);
        else
            TransParBasin[j] = TransParInterval(par_est[j]/fact_prior_bounds,
                                                par_est[j]*fact_prior_bounds)
        end
    end

    for i in 1:n_basins

        # adjust starting values to bounds:

        for j in 1:length(parnames_est_basins)
            bounds = TransParGetBounds(TransParBasin[j])
            if isfinite(bounds[1]) & isfinite(bounds[2])
                if par_est_basins[i][j] <= bounds[1]
                    par_est_basins[i][j] = bounds[1] + (bounds[2]-bounds[1])/fact_prior_bounds^2
                elseif par_est_basins[i][j] >= bounds[2]
                    par_est_basins[i][j] = bounds[2] - (bounds[2]-bounds[1])/fact_prior_bounds^2
                end
            elseif isfinite(bounds[1])
                if par_est_basins[i][j] <= bounds[1]
                    if iszero(bounds[1]) & iszero(par_est_basins[i][j])
                        par_est_basins[i][j] = 1.0  # scale unknown
                    else
                        par_est_basins[i][j] = bounds[1] + 0.5*(abs(bounds[1])+abs(par_est_basins[i][j]))/fact_prior_bounds
                    end
                end
            elseif isfinite(bounds[2])
                if par_est_basins[i][j] >= bounds[2]
                    if iszero(bounds[2]) & iszero(par_est_basins[i][j])
                        par_est_basins[i][j] = -1.0  # scale unknown
                    else
                        par_est_basins[i][j] = bounds[2] - 0.5*(abs(bounds[1])+abs(par_est_basins[i][j]))/fact_prior_bounds
                    end
                end
            end
        end

        # set up likelihood:
        # ------------------

        loglikeli_basins[i] = Hyperflex_Get_Loglikeli(
                                  simulators_basins[i],
                                  Hyperflex_Logpdfobs_IndepHomoscedasticNormal,
                                  par_lik,
                                  parnames_lik,
                                  hcat(data[:,"Time"],data[:,string("Q_",basins[i])]),
                                  parnames_est;
                                  starttime = - years_warmup * 365.0,
                                  verbose   = verbose)

        if testgradients

            # evaluate log likelihood and its gradient:
 
            sigdigits = 6
            println()
            # test log likelihood:
            @time println(string("Log likelihood basin ",basins[i],":       ",round(loglikeli_basins[i](par_est_basins[i]),sigdigits=sigdigits)))
            println()
            # test of gradient of log likelihood:
            @time println(string("Log likelihood ForwardDiff.gradient: ",
                          string.(parnames_est_basins,"=",round.(ForwardDiff.gradient(loglikeli_basins[i],par_est_basins[i]),sigdigits=sigdigits))))
            println()
            @time println(string("Log likelihood Zygote.gradient:      ",
                          string.(parnames_est_basins,"=",round.(Zygote.gradient(loglikeli_basins[i],par_est_basins[i])[1],sigdigits=sigdigits))))

        end

        # set up prior:
        # -------------

        function logprior(par_est,parnames_est,par,parnames)

            lp = 0.0
            if length(par_est) == 0; return lp; end

            # all parameters have their prior mean at their default parameter value

            # rule for shape and standard deviation (with a few exceptions):
            sdrel_wide = 0.5    # most priors for positive parameters are currently
                                # lognormal with a relative standard deviation of 0.5
            sdrel_narrow = 0.1  # some are lognormal with a narrower relative standard
                                # deviation of 0.1
            sd_bounded = 0.1    # most parameters bounded to the interval [0,1]
                                # are (truncated) normal with standard deviation 0.1

            sdlog_wide   = sqrt(log(1+sdrel_wide^2))
            sdlog_narrow = sqrt(log(1+sdrel_narrow^2))
 
            for i in 1:length(parnames_est)
                j = findfirst(isequal(parnames_est[i]),parnames)
                if ! ismissing(j)

                # unbounded parameters with wide prior:

                    if parnames_est[i] == "x2"
                        lp = lp + DistributionsAD.logpdf(DistributionsAD.Normal(0.0,1.0),par_est[i])
                    elseif parnames_est[i] == "kdg"
                        lp = lp + DistributionsAD.logpdf(DistributionsAD.Normal(0.0,1.0),par_est[i])

                # unbounded parameters with narrow prior:

                    elseif ( parnames_est[i] == "Tsfth" ) |
                           ( parnames_est[i] == "Tsmth" )
                        lp = lp + DistributionsAD.logpdf(DistributionsAD.Normal(0.0,0.1),par_est[i])

                # parameters bounded to the interval [0,1]:

                    elseif ( parnames_est[i] == "Dm" ) | 
                           ( parnames_est[i] == "Dr" ) |
                           ( parnames_est[i] == "fi" ) |
                           ( parnames_est[i] == "amelt" ) |
                           ( parnames_est[i] == "thetaG2" )
                        if ( par_est[i] <= 0.0 ) | ( par_est[i] >= 1.0 ); return -Inf; end
                        lp = lp + DistributionsAD.logpdf(DistributionsAD.Normal(par[findfirst(isequal(parnames_est[i]),parnames)],sd_bounded),par_est[i])

                # positive parameters with a narrow prior:

                    elseif ( parnames_est[i] == "cmelt" ) |
                           ( parnames_est[i] == "thetaG1" ) |
                           ( parnames_est[i] == "ce" )
                        if ( par_est[i] <= 0.0 ); return -Inf; end
                        meanlog = log(par[j]) - 0.5*sdlog_narrow^2
                        lp = lp + DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog_narrow),par_est[i])

                # other (positive) parameters have a wide prior:

                    else
                        if ( par_est[i] <= 0.0 ); return -Inf; end
                        meanlog = log(par[j]) - 0.5*sdlog_wide^2
                        lp = lp + DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog_wide),par_est[i])
                    end
                end
            end

            return lp

        end
    
        # get log prior as a function of estimated parameters only:

        logpri_basins[i] = Hyperflex_Get_LogPrior(logprior,parnames_est_basins,par,parnames)
    
        if testgradients

            # evaluate log prior and its gradient:
   
            sigdigits = 6
            println()
            # test log prior:
            @time println(string("Log prior basin ",basins[i],":       ",logpri_basins[i](par_est_basins[i])))
            println()
            # test of gradient of log prior:
            @time println(string("Log prior ForwardDiff.gradient: ",
                         string.(parnames_est_basins,"=",round.(ForwardDiff.gradient(logpri_basins[i],par_est_basins[i]),sigdigits=sigdigits))))
            println()
            @time println(string("Log prior Zygote.gradient:      ",
                          string.(parnames_est_basins,"=",round.(Zygote.gradient(logpri_basins[i],par_est_basins[i])[1],sigdigits=sigdigits))))

        end

        # get log posterior:
        # ------------------

        logpost_basins[i] = Hyperflex_Get_LogPosterior(logpri_basins[i],loglikeli_basins[i];
                                                       verbose=verbose)

        if testgradients

            # evaluate log posterior and its gradient:

            sigdigits = 6
            println()
            # test log posterior:
            @time println(string("Log posterior basin ",basins[i],":       ",logpost_basins[i](par_est_basins[i])))
            println()
            # test of gradient of log posterior:
            @time println(string("Log posterior ForwardDiff.gradient: ",
                          string.(parnames_est_basins,"=",round.(ForwardDiff.gradient(logpost_basins[i],par_est_basins[i]),sigdigits=sigdigits))))
            println()
            @time println(string("Log posterior Zygote.gradient:      ",
                          string.(parnames_est_basins,"=",round.(Zygote.gradient(logpost_basins[i],par_est_basins[i])[1],sigdigits=sigdigits))))

        end

    end

end


# Infer parameters by individual basins:
# ======================================

if infer_parameters

    for i in 1:n_basins

        # perform initial simulation:

        res = HyperflexSimulate(simulators_basins[i],times_out,par_est_basins[i],parnames_est_basins;
                                Qobs_cal=Qobs_cal=hcat(data[:,"Time"],data[:,string("Q_",basins[i])]),
                                Qobs_val=missing,
                                dir=dir_output,filename=string(basins[i],"_",version_inference,"_initialsim"))

        # do inference:

        res_inference = Hyperflex_InferParameters(
                            logpost_basins[i],
                            par_est_basins[i],
                            parnames_est_basins;
                            Trans      = TransParBasin,
                            method     = InferenceMethod,
                            AutoDiff   = AutoDiff,
                            n_samples  = n_samples,
                            n_adapt    = n_adapt)

        # write and save parameter sample/optimal parameter values:
                    
        CSV.write(string(dir_output,"/",basins[i],"_",version_inference,"_sample.dat"),
                  Tables.table(Array(res_inference);header=parnames_est);delim='\t')

        JLD2.@save string(dir_output,"/",basins[i],"_",version_inference,"_sample.jld2") res_inference par parnames par_est parnames_est
        # JLD2.@load string(dir_output,"/",basins[i],"_",version_inference,"_sample.jld2") res_inference par parnames par_est parnames_est

        if (InferenceMethod == "AdvancedHMC") | (InferenceMethod == "DynamicHMC")
            mean_post  = Statistics.mean(res_inference[n_adapt:n_samples,:,:]);
        else
            mean_post  = Statistics.mean(res_inference);
        end
        par_post       = mean_post[:,2];
        parnames_post  = String.(mean_post[:,1]);

        # perform posterior simulation:

        res = HyperflexSimulate(simulators_basins[i],times_out,par_post,parnames_post;
                                Qobs_cal=Qobs_cal=hcat(data[:,"Time"],data[:,string("Q_",basins[i])]),
                                Qobs_val=missing,
                                dir=dir_output,filename=string(basins[i],"_",version_inference,"_finalsim"))
           
    end
end

