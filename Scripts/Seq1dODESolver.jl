## =======================================================
##
## File: Seq1dODESolver.jl
##
## Robust implicit Euler solver for sequential ODEs
##
## creation:          May        , 2021 -- Peter Reichert
## last modification: January  18, 2022 -- Peter Reichert
## 
## contact:           peter.reichert@eawag.ch
##
## =======================================================


using  Interpolations
using  NLsolve
using  ForwardDiff

include("TransPar.jl")


_debug_odesolver_   = false


function Seq1dODESolver(rhs!,start,inc,times,dt,par; 
                        bounds     = missing,
                        method     = "ExplicitEuler",
                        algorithm  = "bisection",   # alternative: ITP
                        useNLsolve = true,
                        reltol     = 1.0e-4,
                        abstol     = 1.0e-4,
                        verbose    = false)

    tstart        = minimum(times)
    tend          = maximum(times)
    times_int     = collect(tstart:dt:(tend+dt))
   
    n_int         = length(times_int)
    n_out         = length(times)
    n_states      = length(start)
    n_iter        = 20

    n_eval_init   = 0
    n_eval_search = 0
    n_eval_conv   = 0
    n_eval_total  = 0

    if ismissing(bounds)
        bounds = [[Float64(-Inf),Float64(Inf)] for i in 1:length(start)]
    end
    # TransStates = Array{TransPar}(undef,length(bounds));
    # for j = 1:length(bounds)
    #     TransStates[j] = TransParUnbounded();
    #     if isfinite(bounds[j][1])
    #         if isfinite(bounds[j][2])
    #             TransStates[j] = TransParInterval(bounds[j][1],bounds[j][2]);
    #         else
    #             TransStates[j] = TransParLowerBound(bounds[j][1]);
    #         end
    #     elseif isfinite(bounds[j][2])
    #         TransStates[j] = TransParUpperBound(bounds[j][2]);
    #     end
    # end

    if length(bounds) != n_states
        println(string("*** length of bounds (",length(bounds),") not equal to number of states (",n_states,")"))
        flush(stdout)
        return missing
    end

    # define function to be made to zeros

    function f!(delta,u0,u1_transORnot,t0,t1;trans=true)

        # if trans
        #     local u1 = TransformBackward(TransStates,u1_transORnot)
        #     rhs!(du,u1,par,t1)
        #     for j in 1:length(u0)
        #         delta[j] = u1[j] - u0[j] - (t1-t0)*du[j]
        #     end
        # else
            if n_states > 1
                rhs!(du,u1_transORnot,par,t1)
            else
                du[1] = rhs!(u1_transORnot[1],par,t1)
            end
            for j in 1:length(u0)
                delta[j] = u1_transORnot[j] - u0[j] - (t1-t0)*du[j]
            end
        # end
	
        if eltype(delta) == Float64   # avoid output during taking of derivatives
            if ! all(isfinite.(delta))
                n_notfinite = n_notfinite + 1
            end
            if n_notfinite > 100   # nlsolve can only be stopped this way:
                ErrorException("*** Implicit Euler solver: too many calls lead to non-finite response")
            end
        end
        n_eval_total = n_eval_total + 1

    end

    # allocate arrays (without initialization):

    T = eltype(par)
    du    = Array{Real,1}(undef,n_states)
    delta = Array{Real,1}(undef,n_states)
    u1    = Array{T,1}(undef,n_states)
    u2    = Array{T,1}(undef,n_states)
    u3    = Array{T,1}(undef,n_states)

    # loop over integration time steps:

    res_int = Array{Real,2}(undef,n_int,n_states)
    res_int[1,:] .= start
    for i in 2:n_int

        if method == "ExplicitEuler"
            
            if n_states > 1
                rhs!(du,res_int[i-1,:],par,times_int[i-1])
            else
                du[1] = rhs!(res_int[i-1,1],par,times_int[i-1])
            end
            n_eval_init  = n_eval_init + 1
            n_eval_total = n_eval_total + 1
            res_int[i,:] = res_int[i-1,:] + dt.*du

        elseif method == "ImplicitEuler"  # CAUTION: sequential ODEs assumed!!

            if n_states > 1   # get du for explicit Euler prediction
                rhs!(du,res_int[i-1,:],par,times_int[i-1])
            else
                du[1] = rhs!(res_int[i-1,1],par,times_int[i-1])
            end
            n_eval_init  = n_eval_init + 1
            n_eval_total = n_eval_total + 1
            if _debug_odesolver_
                if eltype(res_int) == Float64   # avoid output during taking of derivatives
                    println(string("time = ",times_int[i]))
                    println(string("  old state = ",res_int[i-1,:]))
                    flush(stdout)
                end
            end
            for j in 1:n_states     # initialize solution interval end points

                # solution 1: explicit Euler prediction:
                u1[j] = res_int[i-1,j] + dt*du[j]
                if u1[j] <= bounds[j][1]; u1[j] = 0.5*(res_int[i-1,j]+bounds[j][1]); end
                if u1[j] >= bounds[j][2]; u1[j] = 0.5*(res_int[i-1,j]+bounds[j][2]); end

                # solution 2: same as previous step:
                u2[j] = res_int[i-1,j]

                # initialize u3 to u2:
                u3[j] = u1[j]

            end

            # try multi-dimensional solver starting at explicit Euler solution first

            success = false

            if useNLsolve

                f_i!(delta,u) = f!(delta,res_int[i-1,:],u,times_int[i-1],times_int[i])

                # u1_trans = TransformForward(TransStates,u1)
                res_nlsolve = missing
                n_notfinite = 0
                try
                    res_nlsolve = nlsolve(f_i!,
                                          u1;   # u1_trans
                                          autodiff   = :forward,
                                          #method     = :trust_region,
                                          method     = :newton,
                                          #method     = :anderson,
                                          ftol       = abstol,
                                          xtol       = reltol*sqrt(sum(u1.*u1)),
                                          iterations = 100)
                    if ! ismissing(res_nlsolve); success = converged(res_nlsolve); end
                catch ex
                    println(ex)
                    flush(stdout)
                end
            end
            if success
                # res_int[i,:] = TransformBackward(TransStates,res_nlsolve.zero)
                res_int[i,:] = res_nlsolve.zero
            else
                if useNLsolve & _debug_odesolver_
                    if eltype(res_int) == Float64   # avoid output during taking of derivatives
                        println("  first nlsolve search starting at explict Euler solution failed")
                        flush(stdout)
                    end
                end

            # try multi-dimensional solver starting at old solution next

                if useNLsolve
                    # u2_trans = TransformForward(TransStates,u2)
                    res_nlsolve = missing
                    n_notfinite = 0
                    try
                        res_nlsolve = nlsolve(f_i!,
                                              u2;   # u2_trans
                                              autodiff=:forward,
                                              method = :newton,
                                              ftol=1.0e-6)
                        if ! ismissing(res_nlsolve); success = converged(res_nlsolve); end
                    catch ex
                        println(ex)
                        flush(stdout)
                    end
                end
                if success
                    # res_int[i,:] = TransformBackward(TransStates,res_nlsolve.zero)
                    res_int[i,:] = res_nlsolve.zero
                else
                    if useNLsolve & _debug_odesolver_
                        if eltype(res_int) == Float64   # avoid output during taking of derivatives
                            println("  second nlsolve search starting at old solution failed")
                            flush(stdout)
                        end
                    end
    
            # do sequential interval splitting if multi-dimensional solver failed twice

                    # loop over state variables:

                    for j in 1:n_states

                        # calculate delta for u1:

                        n_notfinite = 0
                        f!(delta,res_int[i-1,:],u1,times_int[i-1],times_int[i];trans=false)
                        n_eval_init = n_eval_init + 1
                        d1 = delta[j]
                        if _debug_odesolver_
                            if eltype(u1) == Float64   # avoid output during taking of derivatives
                                println(string("  starting component ",j," of ",n_states))
                                println(string("    u1[",j,"] = ",u1[j]," d1 = ",d1))
                                flush(stdout)
                            end
                        end

                        if abs(d1) < abstol + abs(u1[j])*reltol   # solution found

                            u3[j] = u1[j]
                            if _debug_odesolver_
                                if eltype(u1) == Float64   # avoid output during taking of derivatives
                                    println(string("  u1[",j,"] accepted"))
                                    flush(stdout)
                                end
                            end

                        else
                    
                            # calculate delta for u2:

                            n_notfinite = 0
                            f!(delta,res_int[i-1,:],u2,times_int[i-1],times_int[i];trans=false)
                            n_eval_init = n_eval_init + 1
                            d2 = delta[j]
                            if _debug_odesolver_
                                if eltype(u2) == Float64   # avoid output during taking of derivatives
                                    println(string("    u2[",j,"] = ",u2[j]," d2 = ",d2))
                                    flush(stdout)
                                end
                            end

                            if abs(d2) < abstol + abs(u2[j])*reltol   # solution found

                                u3[j] = u2[j]
                                if _debug_odesolver_
                                    if eltype(u2) == Float64   # avoid output during taking of derivatives
                                        println(string("  u2[",j,"] accepted"))
                                        flush(stdout)
                                    end
                                end

                            else

                                # check for different signs of delta:

                                if ( (d1>0.0) & (d2>0.0) ) | ( (d1<0.0) & (d2<0.0) )
 
                                    # search for interval boundary 2 with different sign of delta:
 
                                    if _debug_odesolver_
                                        if eltype(u2) == Float64   # avoid output during taking of derivatives
                                            println("  searching for u2 with different sign of delta")
                                            flush(stdout)
                                        end
                                    end
                                    for k = 1:25
 
                                        w = 0.5^k
                                        if     u1[j] + (-1.0)^k * k*k * inc[j] <= bounds[j][1]
                                            u2[j] = w*u1[j]+(1.0-w)*bounds[j][1]
                                        elseif u1[j] + (-1.0)^k * k*k * inc[j] >= bounds[j][2]
                                            u2[j] = w*u1[j]+(1.0-w)*bounds[j][2]
                                        else
                                            u2[j] = u1[j] + (-1.0)^k * k*k * inc[j]
                                        end
                                        n_notfinite = 0
                                        f!(delta,res_int[i-1,:],u2,times_int[i-1],times_int[i];trans=false)
                                        n_eval_search = n_eval_search + 1
                                        d2 = delta[j]
                                        if _debug_odesolver_
                                            if eltype(u2) == Float64   # avoid output during taking of derivatives
                                                println(string("    k = ",k))
                                                println(string("    u0[",j,"] = ",res_int[i-1,j]))
                                                println(string("    u1[",j,"] = ",u1[j]," d1 = ",d1))
                                                println(string("    u2[",j,"] = ",u2[j]," d2 = ",d2))
                                                flush(stdout)
                                            end
                                        end
                       
                                        if ( (d1>0.0) & (d2<0.0) ) | ( (d1<0.0) & (d2>0.0) ); break; end

                                    end

                                    if ( (d1>0.0) & (d2>0.0) ) | ( (d1<0.0) & (d2<0.0) )
                                        if _debug_odesolver_
                                            if eltype(res_int) == Float64   # avoid output during taking of derivatives
                                                println(string("*** Seq1dODESolver: unable to bracket solution:"))
                                                println(string("    t = [",times_int[i-1],",",times_int[i],"]"))
                                                println(string("    number of states = ",n_states))
                                                println(string("    u0[",j,"] = ",res_int[i-1,j]))
                                                println(string("    u1[",j,"] = ",u1[j]," d1 = ",d1))
                                                println(string("    u2[",j,"] = ",u2[j]," d2 = ",d2))
                                                flush(stdout)
                                            end
                                        end
                                        return missing
                                    end

                                end

                                # loop over interval splitting iterations:
   
                                if _debug_odesolver_
                                    if eltype(u1) == Float64   # avoid output during taking of derivatives
                                        println("  starting bisection")
                                        flush(stdout)
                                    end
                                end
                                for k in 1:n_iter

                                    # set u3 to midpoint of u1 and u2 and calculate delta:

                                    u3[j] = 0.5 * ( u1[j] + u2[j] )

                                    if algorithm == "ITP"

                                        nmax   = log2(0.5*abs(u2[j]-u1[j])/abstol)
                                        kappa1 = 0.1
                                        kappa2 = 2.0

                                        uf = (u1[j]*d2-u2[j]*d1)/(d2-d1)
                                        s  = sign(u3[j]-uf)
                                        d  = kappa1*abs(u2[j]-u1[j])^kappa2
                                        if d <= abs(u3[j]-uf)
                                            ut = uf + s*d
                                        else
                                            ut = u3[j]
                                        end
                                        r = abstol * 2.0^(nmax-k+1) - 0.5*abs(u2[j]-u1[j])
                                        if abs(ut-u3[j]) <= r
                                            u3[j] = ut
                                        else
                                            u3[j] = u3[j] - s*r
                                        end                                    
                                        
                                    end

                                    n_notfinite = 0
                                    f!(delta,res_int[i-1,:],u3,times_int[i-1],times_int[i];trans=false)
                                    n_eval_conv = n_eval_conv + 1
                                    d3 = delta[j]
                                    if _debug_odesolver_
                                        if eltype(u3) == Float64   # avoid output during taking of derivatives
                                            println(string("    u3[",j,"] = ",u3[j]," d3 = ",d3))
                                            flush(stdout)
                                        end
                                    end

                                    if abs(d3) < abstol + abs(u3[j])*reltol
                                        break
                                    end

                                    # set the correct boundary to u3:

                                    if ( (d1>0.0) & (d3<0.0) ) | ( (d1<0.0) & (d3>0.0) )
                                        u2[j] = u3[j]
                                    else
                                        u1[j] = u3[j]
                                    end

                                end
                                if _debug_odesolver_
                                    if eltype(u3) == Float64   # avoid output during taking of derivatives
                                        println(string("  u3[",j,"] accepted"))
                                        flush(stdout)
                                    end
                                end

                            end

                        end

                        # solution found

                        u1[j]        = u3[j]
                        u2[j]        = u3[j]
                        res_int[i,j] = u3[j]

                    end

                end

            end

            if _debug_odesolver_
                if eltype(res_int) == Float64   # avoid output during taking of derivatives
                    println(string("  new state = ",res_int[i,:]))
                    flush(stdout)
                end
            end

        else

            error(string("*** unknown integration method: ",method))

        end

    end

    if verbose
        println(string("number of evaluations for initialization:  ",n_eval_init))
        println(string("number of evaluations in nlsolve:          ",n_eval_total-n_eval_init-n_eval_search-n_eval_conv))
        println(string("number of evaluations for interval search: ",n_eval_search))
        println(string("number of evaluations for convergence:     ",n_eval_conv))
        flush(stdout)
    end

    # interpolate to output time points:

    res_out = Array{Real,2}(undef,n_out,n_states)
    for j in 1:n_states
        Interpolate_State_j = LinearInterpolation(times_int,res_int[:,j],extrapolation_bc=Interpolations.Flat())
        res_out[:,j] = Interpolate_State_j.(times)
    end

    return res_out
end


# for testing:

#=

start  = [1.0,1.0]
bounds = [[0.0,Inf],[0.0,5.0]]
inc    = [1.0,1.0]
dt     = 0.1
par    = [1.0,1.0]
tstart = 0.0
tend   = 10.0
#tend   = 1.0

function rhs!(du,u,p,t)
    du[1] = -p[1]*u[1]
    du[2] = p[1]*u[1] - p[2]*u[2]
end

reltol = 1.0e-6
abstol = 1.0e-6

# @time res_exp = Seq1dODESolver(rhs!,start,inc,collect(tstart:tend),dt,par;
#                                method="ExplicitEuler",reltol=reltol,abstol=abstol)
# println(res_exp)

@time res_imp = Seq1dODESolver(rhs!,start,inc,collect(tstart:tend),dt,par;bounds=bounds,
                               method     = "ImplicitEuler",
                               #algorithm = "ITP",
                               useNLsolve = true,
                               reltol     = reltol,
                               abstol     = abstol)
println(res_imp)

# calcres(par) = Seq1dODESolver(rhs!,start,inc,collect(tstart:tend),dt,par;
#                                method="ImplicitEuler",reltol=reltol,abstol=abstol)

# calcres([2.0,2.0])

# function lossfun(par)
#     return sum(calcres(par)[:,2])
# end

# println("loss")
# println(lossfun([2.0,2.0]))
# println(lossfun([2.1,2.0]))
# println(lossfun([2.0,2.1]))

# println("gradient of loss")
# println(ForwardDiff.gradient(lossfun,[2.0,2.0]))

=#
