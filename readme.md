# Hyperflex

Julia framework for conceptual hydrological modelling with the goal to implement a hierarchical regionalization approach to hydrological modelling.

## Use

File to be executed: **`Hyperflex_run_Camels.jl`**

The tasks to be performed have to be set to true at the beginning of the script and the following settings may have to be adapted (in particular also the camels directories explained below).

## Hydrological data set CAMELS

The CAMELS hydrology dataset files (see references below) have to be downloaded and placed in the directories specified by the variables

* **`dir_camels_timeseries`** (original directory name: **`basin_timeseries_v1p2_metForcing_obsFlow`**)

* **`dir_camels_attributes`** (original directory name: **`camels_attributes_v2.0`**)

* **`dir_camels_modres`** (original directory name: **`basin_timeseries_v1p2_modelOutput_daymet`**)

Also the variables **`dir_input`** and **`dir_output`** have to be adapted.

CAMELS data set USA: https://ral.ucar.edu/solutions/products/camels

A. J. Newman, M. P. Clark, K. Sampson, A. Wood, L. E. Hay, A. Bock, R. J. Viger, D. Blodgett, L. Brekke, J. R. Arnold, T. Hopson, and Q. Duan, 2015
Development of a large-sample watershed-scale hydrometeorological dataset for the contiguous USA: dataset characteristics and assessment of regional variability in hydrologic model performance. 
Hydrol. Earth Syst. Sci., 19, 209-223, 2015
https://doi.org/10.5194/hess-19-209-2015

Addor, N., Newman, A. J., Mizukami, N. and Clark, M. P.
The CAMELS data set: catchment attributes and meteorology for large-sample studies, 
Hydrol. Earth Syst. Sci., 21, 5293–5313, 2017
https://doi.org/10.5194/hess-21-5293-2017.


